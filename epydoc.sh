#!/bin/sh

epydoc -o ~/tmp/py4bot -u http://www.py4bot.org -n Py4bot -v \
       --no-imports --show-frames --graph all --introspect-only \
       py4bot/actuators py4bot/common py4bot/core py4bot/drivers py4bot/gaits py4bot/hardware py4bot/ik py4bot/inputs py4bot/services

