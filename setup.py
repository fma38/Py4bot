# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Packaging

Implements
==========

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

from py4bot.common import config


setup(name=config.APP_NAME,
      version="1.0.0b3",

      description="Py4bot framework",
      long_description=open('README').read(),
      url="http://www.py4bot.org",

      author="Frederic Mantegazza",
      author_email="fma38@gbiloba.org",

      license="GPL",

      maintainer="Frederic Mantegazza",
      maintainer_email="fma38@gbiloba.org",

      download_url="https://framagit.org/fma38/Py4bot.git",

      packages=["py4bot",
                "py4bot.actuators",
                "py4bot.common",
                "py4bot.core",
                "py4bot.drivers",
                "py4bot.gaits",
                "py4bot.hardware",
                "py4bot.ik",
                "py4bot.inputs",
                "py4bot.inputs.curves",
                "py4bot.inputs.frontends",
                "py4bot.inputs.mappers",
                "py4bot.services",
                ],

      scripts=["py4bot/scripts/py4bot-evdev.py",
               "py4bot/scripts/py4bot-gui-evcal.py",
               "py4bot/scripts/py4bot-gui-servocal.py",
               "py4bot/scripts/py4bot-gui-lx16acal.py",
               "py4bot/scripts/py4bot-joycal.py",
               "py4bot/scripts/py4bot-pca9685.py"],

      install_requires=["evdev"],
)
