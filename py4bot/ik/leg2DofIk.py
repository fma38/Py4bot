
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Leg inverse kinematic computations

Implements
==========

 - B{Leg2DofIk}

Documentation
=============

 y

 |           x'
 |         /
 |        /
 |       /
 |      /
 |     /
 |    /
 |   /
 |  /
 | / \ gamma
 |/   |
 +-------------------------- x


 z

 |
 |
 |
 |
 |
 |------+
 |   \   \
 |    ----\
 |  alpha  \
 |          \
 |           \
 |            \
 |             \
 +-------------------------- x'

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

from py4bot.ik.legIk import LegIk


class Leg2DofIk(LegIk):
    """
    """
    def __init__(self, geometry):
        """ Init Leg2DofIk object
        """
        super(Leg2DofIk, self).__init__(geometry)

    @property
    def coxa(self):
        return self._geometry['coxa']

    @property
    def femur(self):
        return self._geometry['femur']

    def _gamma(self, P):
        """
        """
        x, y = P[0], P[1]

        return math.atan2(y, x)

    def solve(self, P):
        x, y, z = P[0], P[1], -P[2]

        L = self.femur

        alpha = math.pi / 2. + math.acos(z / L)

        gamma = math.atan2(y, x)

        return {'alpha': math.degrees(alpha), 'gamma': math.degrees(gamma)}
