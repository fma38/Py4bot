
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Leg inverse kinematic computations

Implements
==========

 - B{Leg4DofIk}

Documentation
=============

 y

 |           x'
 |         /
 |        /
 |       /
 |      /
 |     /
 |    /
 |   /
 |  /
 | / \ gamma
 |/   |
 +-------------------------- x


 z

 |           +
 |          / \
 |         /   \
 |        / `-' \
 |       / beta  \
 |------+ \       \
 |     \  /      / \
 |      --    kappa +
 |      alpha     \ |
 |                  |
 |                  |
 +-------------------------- x'

In the current implementation, the tars is always vertical.

Usage
=====

TODO
====

 - check tars usage
 - optimize (compute L only once)

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

from py4bot.ik.leg3DofIk import Leg3DofIk


class Leg4DofIk(Leg3DofIk):
    """
    """
    def __init__(self, geometry):
        """ Init Leg4DofIk object
        """
        super(Leg4DofIk, self).__init__(geometry)

    @property
    def tars(self):
        return self._geometry['tars']

    def solve(self, P):
        x, y, z = P[0], P[1], -(P[2] + self.tars)

        L = math.sqrt((math.sqrt(x ** 2 + y ** 2) - self.coxa) ** 2 + z ** 2)

        alpha = math.pi / 2. + math.acos(z / L) + \
                math.acos((self.tibia ** 2 - self.femur ** 2 - L ** 2) / (-2 * L * self.femur))

        beta = math.acos((L ** 2 - self.tibia ** 2 - self.femur ** 2) / (-2 * self.tibia * self.femur))

        gamma = math.atan2(y, x)

        kappa = math.radians(450) - alpha - beta

        return {'alpha': math.degrees(alpha), 'beta': math.degrees(beta), 'gamma': math.degrees(gamma), 'kappa': math.degrees(kappa)}
