# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Logging service

Implements
==========

- B{DefaultFormatter}
- B{ColorFormatter}
- B{SpaceFormatter}
- B{SpaceColorFormatter}

@author: Frédéric Mantegazza
@copyright: (C) 2013-2021 Frédéric Mantegazza
@license: GPL
"""

import logging
import time
import sys


class DefaultFormatter(logging.Formatter):
    """ Default formatter
    """
    def format(self, record):
        msg = logging.Formatter.format(self, record)
        msg = msg.replace('\n', '\n\r')
        return msg + '\r'


class ColorFormatter(DefaultFormatter):
    """ Colors for console.
    """
    colors = {'trace': "\033[0;36;40;22m",     # cyan/noir, normal
              'debug': "\033[0;36;40;1m",      # cyan/noir, gras
              'info': "\033[0;37;40;1m",       # blanc/noir, gras
              'warning': "\033[0;33;40;1m",    # jaune/noir, gras
              'error': "\033[0;31;40;1m",      # rouge/noir, gras
              'exception': "\033[0;35;40;1m",  # magenta/noir, gras
              'critical': "\033[0;37;41;1m",   # blanc/rouge, gras
              'default': "\033[0m",            # defaut
              }

    def _toColor(self, msg, levelname):
        """ Colorize.
        """
        if levelname == 'TRACE':
            color = ColorFormatter.colors['trace']
        elif levelname == 'DEBUG':
            color = ColorFormatter.colors['debug']
        elif  levelname in 'INFO':
            color = ColorFormatter.colors['info']
        elif levelname == 'COMMENT':
            color = ColorFormatter.colors['comment']
        elif levelname == 'PROMPT':
            color = ColorFormatter.colors['prompt']
        elif levelname == 'WARNING':
            color = ColorFormatter.colors['warning']
        elif levelname == 'ERROR':
            color = ColorFormatter.colors['error']
        elif levelname == 'EXCEPTION':
            color = ColorFormatter.colors['exception']
        elif levelname == 'CRITICAL':
            color = ColorFormatter.colors['critical']
        else:
            color = ColorFormatter.colors['default']

        return color + msg + ColorFormatter.colors['default']

    def format(self, record):
        msg = DefaultFormatter.format(self, record)
        return self._toColor(msg, record.levelname)


class SpaceFormatter(DefaultFormatter):
    """ Format with newlines
    """
    def __init__(self, *args, **kwargs):
        super(SpaceFormatter, self).__init__(*args, **kwargs)

        self._lastLogTime = time.time()

    def _addSpace(self, msg):
        """ Add newlines

        Compute number of newlines according to delay from last log.

        @todo: does not work if multiple handlers use this formatter
        """
        if time.time() - self._lastLogTime > 3600:
            space = "\n\n\n"
        elif time.time() - self._lastLogTime > 60:
            space = "\n\n"
        elif time.time() - self._lastLogTime > 3:
            space = "\n"
        else:
            space = ""
        self._lastLogTime = time.time()

        return space + msg

    def format(self, record):
        msg = DefaultFormatter.format(self, record)
        return self._addSpace(msg)


class SpaceColorFormatter(SpaceFormatter, ColorFormatter):
    """ Formatter with colors and newlines
    """
    def format(self, record):
        msg = SpaceFormatter.format(self, record)
        return self._toColor(msg, record.levelname)
