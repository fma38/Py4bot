# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller analog management

Implements
==========

 - B{Axis}

Documentation
=============

Axis component purpose is to handle an analog value from frontends, like a gamepad axis, or a simple potentiometer.
Can also be use to get the analog value of buttons of some gamepads, like the Sony DualShock2. In this cas, it is
better to add the matching button key as modifier.

Value from frontend must be a float in range [-1.:1.]

Axis takes all AnalogComponent params, and have additionnal:

 - key              frontend key name (str) - Mandatory
 - modifier         frontend key modifier name (str) - Optional
 - curve            curve type of the transmitted value (str) - Optional, default to linear curve
 - mapper           function mapper (callable) - Optional

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.common.exception import Py4botError
from py4bot.services.logger import Logger
from py4bot.inputs.component import AnalogComponent
from py4bot.inputs.curves.curve import CurveStd
from py4bot.inputs.mappers.mapper import MapperStraight


class Axis(AnalogComponent):
    """
    """
    def __init__(self, command, key, threshold=0., modifier=None, curve=CurveStd(), mapper=MapperStraight()):
        """
        """
        super(Axis, self).__init__(command, mapper, threshold)

        self._key = key
        self._modifier = modifier
        self._curve = curve

    def _update(self):

        # Check if no modifier other than self._modifier is pressed
        # Modifiers are only keys starting with 'digital_'
        for key, value in self._frontendState.items():
            if value and key.startswith('digital_'):
                if self._modifier is None  or \
                   self._modifier is not None and key != self._modifier:
                    return

        if self._modifier is None or self._frontendState.get(self._modifier, False):
            if self._frontendStateChanged(self._key):

                # Read value from frontend
                value = self._frontendState[self._key]

                try:

                    # Apply curve
                    #Logger().debug("Axis._update(): call %s curve" % self._curve)
                    value = self._curve(value)
                    #Logger().debug("Axis._update(): curve returned %s" % (repr(value)))

                    # Apply mapper
                    #Logger().debug("Axis._update(): call %s mapper" % self._mapper)
                    args, kwargs = self._mapper(value)
                    #Logger().debug("Axis._update(): mapper returned %s, %s" % (repr(args), repr(kwargs)))

                    # Call command
                    #Logger().debug("Axis._update(): call %s command" % self._command)
                    self._command(*args, **kwargs)

                except Py4botError:
                    Logger().exception("Axis._update()")
