# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller Curves

Implements
==========

 - B{Curve}
 - B{CurvePower}

Documentation
=============

Usage
=====

Curves are used to modify the analog response of Components.

They should retain the input range [-1:+1], but they can change the moving sens (not recommended, however).

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

from py4bot.common import config
from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger


class Curve(object):
    """ Base class for Curves
    """
    def __init__(self):
        """ Init Curve object
        """
        super(Curve, self).__init__()

    def __str__(self):
        return self.__class__.__name__

    def __call__(self, value):
        raise NotImplementedError


class CurveVerySlow(Curve):
    """ Very slow curve
    """
    def __call__(self, value):
        return math.pow(value, 3)


class CurveSlow(Curve):
    """ Slow curve
    """
    def __call__(self, value):
        if value >= 0:
            return math.pow(value, 2)
        else:
            return -math.pow(value, 2)


class CurveStd(Curve):
    """ Standard curve (linear)
    """
    def __call__(self, value):
        return value


class CurveFast(Curve):
    """ Fast curve
    """
    def __call__(self, value):
        if value >= 0:
            return math.sqrt(value)
        else:
            return -math.sqrt(-value)


class CurveVeryFast(Curve):
    """ Very fast curve
    """
    def __call__(self, value):
        if value >= 0:
            return math.pow(value, 1/3)
        else:
            return -math.pow(-value, 1/3)
