
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Smart Remote Control 2 joystick implementation.

Implements
==========

 - B{JsonUDP}

Documentation
=============

This frontend reads joystick and buttons positions from the Smart Remote Control 2.

See hardware/smartRemoteCtrl2/smartRemoteCtrl2.ino file.

Usage
=====

TODO
====

Implement Failsafe mode


@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import socket
import json

from py4bot.common import config
from py4bot.services.logger import Logger
from py4bot.common.intervalMapper import IntervalMapper
from py4bot.inputs.frontends.frontend import Frontend

# Test for robot status
import time
import psutil
from collections import deque
q = [deque(maxlen=10), deque(maxlen=10), deque(maxlen=10), deque(maxlen=10)]
cpu = [0, 0, 0, 0]
import numpy
from py4bot.gaits.gaitManager import GaitManager
from py4bot.gaits.gaitSequencer import GaitSequencer
def movingAverage(x, w): 
    return numpy.convolve(x, numpy.ones(w), 'valid') / w

# Axis calibration (use to have a dead spot at center, to avoid noise)
DEFAULT_AXIS_CALIBRATION = {
    'JOY_X': (0x00, 0x70, 0x90, 0xff),  # X (left)
    'JOY_Y': (0x00, 0x70, 0x90, 0xff),  # Y (left)
    'JOY_Z': (0x00, 0x70, 0x90, 0xff),  # Z (left)
    'JOY_U': (0x00, 0x70, 0x90, 0xff),  # U (right)
    'JOY_V': (0x00, 0x70, 0x90, 0xff),  # V (right)
    'JOY_W': (0x00, 0x70, 0x90, 0xff),  # W (right)
    'POT_A': (0x00, 0x70, 0x90, 0xff),  # A
    'POT_B': (0x00, 0x70, 0x90, 0xff),  # B
    'MPU_ORIENT_X': (-45., -5., 5., 45.),  # MPU
    'MPU_ORIENT_Y': (-45., -5., 5., 45.),  # MPU
}


class JsonUDP(Frontend):
    """ Smart Remote Control 2 implementation
    """
    def __init__(self, ip="192.168.4.100", port=1234, calibration=DEFAULT_AXIS_CALIBRATION):
        """ Init JsonUDP object
        """
        super(JsonUDP, self).__init__("JsonUDP")

        # Create a datagram socket
        self._udpServerSocket = socket.socket(family=socket.AF_INET, type=socket.SOCK_DGRAM)
        self._udpServerSocket.setblocking(0)

        # Bind to address and port
        self._udpServerSocket.bind((ip, port))
        Logger().info("UDP server up and listening")

        self._intervalMapper = {}
        for axis in calibration.keys():
            self._intervalMapper[axis] = IntervalMapper(calibration[axis], (-1., 0., 0., +1))

    def _decode(self, jsonMessage):
        """ Decode JSON data
        """
        #Logger().trace("JsonUDP._decode()")

        data = json.loads(jsonMessage)
        #Logger().debug("JsonUDP._decode(): data=%s" % data)

        for axis, val in data['analog'].items():
            #Logger().debug("JsonUDP._decode(): %s=0x%x)" % (axis, val))
            value = self._intervalMapper[axis](val)
            value = round(value, 1)
            self._state["analog_%s" % axis] = value

        for button, val in data['digital'].items():
            #Logger().debug("JsonUDP._decode(): %s=0x%x)" % (button, val))
            self._state["digital_%s" % button] = val

    def refresh(self):

        # Listen for incoming datagrams
        try:
            bytesAddressPair = self._udpServerSocket.recvfrom(1024)
            jsonMessage = bytesAddressPair[0]
            address = bytesAddressPair[1]
            self._decode(jsonMessage.decode())

            for i in range(4):
                q[i].append(psutil.cpu_percent(percpu=True)[i])
                cpu[i] = movingAverage(q[i], len(q[i]))
            robotStatus = [
                "gait  %s" % GaitManager().gait.name,
                "state %s" % GaitSequencer().state,
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "mem   %04.1f%% %04.1f%%" % (psutil.virtual_memory()[2], psutil.swap_memory()[3]),
                "cpu   %02d %02d %02d %02d" % tuple(cpu),
                "time  %s" % time.strftime("%H:%M:%S", time.localtime())
           ]

            bytesToSend = json.dumps(robotStatus).encode()
#            bytesToSend = str.encode(msgFromServer)
            self._udpServerSocket.sendto(bytesToSend, address)

        except socket.error:
            pass
