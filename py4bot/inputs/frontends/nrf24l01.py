# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

NRF24L01-based input frontends. Mainly for using:

U{DIY Arduino RC transmitter <https://howtomechatronics.com/projects/diy-arduino-rc-transmitter/>} input frontends

Implements
==========

 - B{NRF24L01}

Documentation
=============

For more info of how to communicate through NRF24L01, see:

U{https://howtomechatronics.com/tutorials/arduino/arduino-wireless-communication-nrf24l01-tutorial}

Usage
=====

TODO
====

NOTE
====

Only works on Raspberry Pi

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""
import time

import spidev
import RPi.GPIO as GPIO
from lib_nrf24 import NRF24

from py4bot.services.logger import Logger
from py4bot.inputs.frontends.frontend import Frontend


class NRF24L01(Frontend):
    """ Class for NRF24L01-based input frontend
    """
    def __init__(self, name, address="42424"):
        """ Init NRF24L01 object
        """
        super(NRF24L01, self).__init__(name)

        self._address = address

        GPIO.setmode(GPIO.BCM)

        # Open radio
        self._radio = NRF24(GPIO, spidev.SpiDev())
        self._radio.begin(0, 17)

        self._radio.setRetries(15, 15)

        self._radio.setPayloadSize(32)
        self._radio.setChannel(0x60)
        self._radio.setDataRate(NRF24.BR_250KPS)
        self._radio.setPALevel(NRF24.PA_MIN)

        self._radio.setAutoAck(True)
        self._radio.enableDynamicPayloads()
        self._radio.enableAckPayload()

        self._radio.openReadingPipe(0, address)

        #self._radio.startListening()
        #self._radio.stopListening()

        self._radio.printDetails()

        self._radio.startListening()


    def _keyToDigital(self, eventCode):
        """ Mapping event code -> button num
        """
        raise NotImplementedError

    def _absToAnalog(self, eventCode, eventValue):
        """ Mapping event code/value -> analog num/value
        """
        raise NotImplementedError

    def _decode(self, payload):
        """ Decode NRF24L01 payload from Aruino RC Transmitter

            struct Data_Package {
                byte leftPotX;
                byte leftPotY;
                byte rightPotX;
                byte rightPotY;
                byte leftPot;
                byte rightPot;
                byte angleX;
                byte angleY;

                byte leftButton;
                byte rightButton;
                byte leftSwitch;
                byte rightSwitch;
                byte leftExtButton;
                byte leftIntButton;
                byte rightIntButton;
                byte rightExtButton;
            };
        """
        #Logger().trace("NRF24L01._decode()")

        try:
            index = 0
            for analog in range(8):
                self._state["analog_%02d" % analog] = payload[index]
                index += 1
            for digital in range(8):
                self._state["digital_%03d" % digital] = payload[index]
                index += 1

        except IndexError:
            Logger().debug("NRF24L01._decode(): invalid payload (%r)" % payload)
            Logger().exception("NRF24L01._decode()")

    def refresh(self):
        if self._radio.available(0):
            payload = []
            self._radio.read(payload, self._radio.getDynamicPayloadSize())
            Logger().debug("NRF24L01.refresh(): payload=%r" % payload)

            ackPL = [1]
            self._radio.writeAckPayload(1, ackPL, len(ackPL))

            self._decode(payload)
