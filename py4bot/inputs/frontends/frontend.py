# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Frontend base class

Implements
==========

 - B{Frontend}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.services.logger import Logger


class Frontend(object):
    """ Base class for input frontends
    """
    def __init__(self, name):
        """ Init Frontend object
        """
        super(Frontend, self).__init__()

        self._name = name

        self._state = {}

    @property
    def name(self):
        return self._name

    @property
    def state(self):
        """ Build state
        """
        return self._state

    def refresh(self):
        """ Check for new USB events
        """
        raise NotImplementedError
