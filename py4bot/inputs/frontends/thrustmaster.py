# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

'Thrustworld Thrustmaster dual analog 3' controller driver

Implements
==========

 - B{Thrustmaster}

Documentation
=============

{('EV_ABS', 3L): [(('ABS_X', 0L), AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                  (('ABS_Y', 1L), AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                  (('ABS_RZ', 5L), AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                  (('ABS_THROTTLE', 6L), AbsInfo(value=128, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_HAT0X', 16L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0)),
                  (('ABS_HAT0Y', 17L), AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))],
 ('EV_KEY', 1L): [(['BTN_A', 'BTN_GAMEPAD', 'BTN_SOUTH'], 304L),
                  (['BTN_B', 'BTN_EAST'], 305L),
                  ('BTN_C', 306L),
                  (['BTN_NORTH', 'BTN_X'], 307L),
                  (['BTN_WEST', 'BTN_Y'], 308L),
                  ('BTN_Z', 309L),
                  ('BTN_TL', 310L),
                  ('BTN_TR', 311L),
                  ('BTN_TL2', 312L),
                  ('BTN_TR2', 313L),
                  ('BTN_SELECT', 314L),
                  ('BTN_START', 315L)],
 ('EV_MSC', 4L): [('MSC_SCAN', 4L)],
 ('EV_SYN', 0L): [('SYN_REPORT', 0L),
                  ('SYN_CONFIG', 1L),
                  ('SYN_DROPPED', 3L),
                  ('?', 4L)]}

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.services.logger import Logger
from py4bot.common.intervalMapper import IntervalMapper
from py4bot.inputs.frontends.frontendUsb import FrontendUsb

# Button mapping
BUTTON_MAPPING = {
    0x130:  0,
    0x131:  1,
    0x132:  2,
    0x133:  3,
    0x134:  4,
    0x135:  5,
    0x136:  6 ,
    0x137:  7,
    0x138:  8,
    0x139:  9,
    0x13a: 10,
    0x13b: 11,
}

# Axis mapping
AXIS_MAPPING = {
    0x00: 0,
    0x01: 1,
    0x05: 2,
    0x06: 3,
    0x10: 4,
    0x11: 5
}

# Axis calibration
DEFAULT_AXIS_CALIBRATION = {
    0: (-128, +127),  #  X left  (ABS_X)
    1: (+127, -128),  #  Y left  (ABS_Y)
    2: (-128,  127),  #  X right (ABS_Z)
    3: (+255,   +0),  #  Y right (ABS_RZ)
    4: (  -1,   +1),  #  X hat
    5: (  +1,   -1)   #  Y hat
}


class Thrustmaster(FrontendUsb):
    """ Thrustmaster gamepad implementation
    """
    def __init__(self, path, calibration=DEFAULT_AXIS_CALIBRATION):
        """ Init Thrustmaster object
        """
        super(Thrustmaster, self).__init__("Thrustmaster", path)

        self._intervalMapper = {}
        for axis in calibration.keys():
            self._intervalMapper[axis] = IntervalMapper(calibration[axis])

    def _keyToDigital(self, eventCode):
        code = BUTTON_MAPPING[eventCode]

        return code

    def _absToAnalog(self, eventCode, eventValue):
        axis = AXIS_MAPPING[eventCode]
        value = self._intervalMapper[axis](eventValue)

        return axis, value
