
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

TeensyUsb-based joystick implementation.

Implements
==========

 - B{TeensyUsb}

Documentation
=============

This frontend reads joystick position from a Teensyduino-based joystick implementation.

See hardware/teensy/joystick.ino file.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.common import config
from py4bot.services.logger import Logger
from py4bot.common.intervalMapper import IntervalMapper
from py4bot.inputs.frontends.frontendUsb import FrontendUsb

# Button mapping
BUTTON_MAPPING = {
    0x120:  0,
    0x121:  1,
    0x122:  2,
    0x123:  3,
    0x124:  4,
    0x125:  5,
    0x126:  6 ,
    0x127:  7,
    0x128:  8,
    0x129:  9,
    0x12a: 10,
    0x12b: 11,
    0x12c: 12,
    0x12d: 13,
    0x12e: 14,
    0x12f: 15,
}

# Axis mapping
AXIS_MAPPING = {
    0x00: 0,
    0x01: 1,
    0x02: 2,
    0x05: 3,
    0x06: 4,
    0x07: 5
}

# Axis calibration
DEFAULT_AXIS_CALIBRATION = {
    0: (0x10, 0x70, 0x90, 0xf0),  #  X right
    1: (0x10, 0x70, 0x90, 0xf0),  #  Y right
    2: (0x10, 0x70, 0x90, 0xf0),  # RZ right
    3: (0x10, 0x70, 0x90, 0xf0),  #  X left
    4: (0x10, 0x70, 0x90, 0xf0),  #  Y left
    5: (0x10, 0x70, 0x90, 0xf0)   # RZ left
}


class TeensyUsb(FrontendUsb):
    """ TeensyUsb gamepad implementation
    """
    def __init__(self, path, calibration=DEFAULT_AXIS_CALIBRATION):
        """ Init TeensyUsb object
        """
        super(TeensyUsb, self).__init__("TeensyUsb", path)

        self._intervalMapper = {}
        for axis in calibration.keys():
            self._intervalMapper[axis] = IntervalMapper(calibration[axis], (-1., 0., 0., +1))

    def _keyToDigital(self, eventCode):
        code = BUTTON_MAPPING[eventCode]

        return code

    def _absToAnalog(self, eventCode, eventValue):
        axis = AXIS_MAPPING[eventCode]
        value = self._intervalMapper[axis](eventValue)

        return axis, round(value, 1)
