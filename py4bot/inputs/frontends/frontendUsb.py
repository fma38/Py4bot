# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

USB input frontends

Implements
==========

 - B{FrontendUsb}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import evdev

from py4bot.services.logger import Logger
from py4bot.inputs.frontends.frontend import Frontend


class FrontendUsb(Frontend):
    """ Base class for USB input frontends
    """
    def __init__(self, name, path):
        """ Init FrontendUsb object
        """
        super(FrontendUsb, self).__init__(name)

        # Open device
        self._dev = evdev.InputDevice(path)

    def _keyToDigital(self, eventCode):
        """ Mapping event code -> button num
        """
        raise NotImplementedError

    def _absToAnalog(self, eventCode, eventValue):
        """ Mapping event code/value -> analog num/value
        """
        raise NotImplementedError

    def _decode(self, event):
        """ Decode USB events
        """
        #Logger().trace("FrontendUsb._decode()")

        try:
            if event.type == evdev.ecodes.EV_KEY:
                #Logger().debug("FrontendUsb._decode(): event EV_KEY=(%s, 0x%x)" % (event.code, event.value))
                button = self._keyToDigital(event.code)
                self._state["digital_%03d" % button] = event.value

            elif event.type == evdev.ecodes.EV_ABS:
                #Logger().debug("FrontendUsb._decode(): event EV_ABS=(%s, 0x%x)" % (event.code, event.value))
                analog, value = self._absToAnalog(event.code, event.value)
                value = round(value, 1)
                self._state["analog_%02d" % analog] = value

        except KeyError:
            Logger().debug("FrontendUsb._decode(): event=%s" % event)
            Logger().exception("FrontendUsb._decode()")

    def refresh(self):
        #for event in self._dev.read():
            #self._decode(event)
        event = self._dev.read_one()
        if event is not None:
            self._decode(event)
