# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

'IPEGA PG-9023' controller driver

Implements
==========

 - B{IpegaPG9023}

Documentation
=============

{('EV_ABS', 3L): [(('ABS_X', 0L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_Y', 1L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_Z', 2L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_RZ', 5L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_GAS', 9L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_BRAKE', 10L),
                   AbsInfo(value=0, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                  (('ABS_HAT0X', 16L),
                   AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0)),
                  (('ABS_HAT0Y', 17L),
                   AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))],
 ('EV_KEY', 1L): [('KEY_ESC', 1L),
                  ('KEY_1', 2L),
                  ('KEY_2', 3L),
                  ('KEY_3', 4L),
                  ('KEY_4', 5L),
                  ('KEY_5', 6L),
                  ('KEY_6', 7L),
                  ('KEY_7', 8L),
                  ('KEY_8', 9L),
                  ('KEY_9', 10L),
                  ('KEY_0', 11L),
                  ('KEY_MINUS', 12L),
                  ('KEY_EQUAL', 13L),
                  ('KEY_BACKSPACE', 14L),
                  ('KEY_TAB', 15L),
                  ('KEY_Q', 16L),
                  ('KEY_W', 17L),
                  ('KEY_E', 18L),
                  ('KEY_R', 19L),
                  ('KEY_T', 20L),
                  ('KEY_Y', 21L),
                  ('KEY_U', 22L),
                  ('KEY_I', 23L),
                  ('KEY_O', 24L),
                  ('KEY_P', 25L),
                  ('KEY_LEFTBRACE', 26L),
                  ('KEY_RIGHTBRACE', 27L),
                  ('KEY_ENTER', 28L),
                  ('KEY_LEFTCTRL', 29L),
                  ('KEY_A', 30L),
                  ('KEY_S', 31L),
                  ('KEY_D', 32L),
                  ('KEY_F', 33L),
                  ('KEY_G', 34L),
                  ('KEY_H', 35L),
                  ('KEY_J', 36L),
                  ('KEY_K', 37L),
                  ('KEY_L', 38L),
                  ('KEY_SEMICOLON', 39L),
                  ('KEY_APOSTROPHE', 40L),
                  ('KEY_GRAVE', 41L),
                  ('KEY_LEFTSHIFT', 42L),
                  ('KEY_BACKSLASH', 43L),
                  ('KEY_Z', 44L),
                  ('KEY_X', 45L),
                  ('KEY_C', 46L),
                  ('KEY_V', 47L),
                  ('KEY_B', 48L),
                  ('KEY_N', 49L),
                  ('KEY_M', 50L),
                  ('KEY_COMMA', 51L),
                  ('KEY_DOT', 52L),
                  ('KEY_SLASH', 53L),
                  ('KEY_RIGHTSHIFT', 54L),
                  ('KEY_KPASTERISK', 55L),
                  ('KEY_LEFTALT', 56L),
                  ('KEY_SPACE', 57L),
                  ('KEY_CAPSLOCK', 58L),
                  ('KEY_F1', 59L),
                  ('KEY_F2', 60L),
                  ('KEY_F3', 61L),
                  ('KEY_F4', 62L),
                  ('KEY_F5', 63L),
                  ('KEY_F6', 64L),
                  ('KEY_F7', 65L),
                  ('KEY_F8', 66L),
                  ('KEY_F9', 67L),
                  ('KEY_F10', 68L),
                  ('KEY_NUMLOCK', 69L),
                  ('KEY_SCROLLLOCK', 70L),
                  ('KEY_KP7', 71L),
                  ('KEY_KP8', 72L),
                  ('KEY_KP9', 73L),
                  ('KEY_KPMINUS', 74L),
                  ('KEY_KP4', 75L),
                  ('KEY_KP5', 76L),
                  ('KEY_KP6', 77L),
                  ('KEY_KPPLUS', 78L),
                  ('KEY_KP1', 79L),
                  ('KEY_KP2', 80L),
                  ('KEY_KP3', 81L),
                  ('KEY_KP0', 82L),
                  ('KEY_KPDOT', 83L),
                  ('KEY_ZENKAKUHANKAKU', 85L),
                  ('KEY_102ND', 86L),
                  ('KEY_F11', 87L),
                  ('KEY_F12', 88L),
                  ('KEY_RO', 89L),
                  ('KEY_KATAKANA', 90L),
                  ('KEY_HIRAGANA', 91L),
                  ('KEY_HENKAN', 92L),
                  ('KEY_KATAKANAHIRAGANA', 93L),
                  ('KEY_MUHENKAN', 94L),
                  ('KEY_KPJPCOMMA', 95L),
                  ('KEY_KPENTER', 96L),
                  ('KEY_RIGHTCTRL', 97L),
                  ('KEY_KPSLASH', 98L),
                  ('KEY_SYSRQ', 99L),
                  ('KEY_RIGHTALT', 100L),
                  ('KEY_HOME', 102L),
                  ('KEY_UP', 103L),
                  ('KEY_PAGEUP', 104L),
                  ('KEY_LEFT', 105L),
                  ('KEY_RIGHT', 106L),
                  ('KEY_END', 107L),
                  ('KEY_DOWN', 108L),
                  ('KEY_PAGEDOWN', 109L),
                  ('KEY_INSERT', 110L),
                  ('KEY_DELETE', 111L),
                  (['KEY_MIN_INTERESTING', 'KEY_MUTE'], 113L),
                  ('KEY_VOLUMEDOWN', 114L),
                  ('KEY_VOLUMEUP', 115L),
                  ('KEY_POWER', 116L),
                  ('KEY_KPEQUAL', 117L),
                  ('KEY_PAUSE', 119L),
                  ('KEY_KPCOMMA', 121L),
                  (['KEY_HANGEUL', 'KEY_HANGUEL'], 122L),
                  ('KEY_HANJA', 123L),
                  ('KEY_YEN', 124L),
                  ('KEY_LEFTMETA', 125L),
                  ('KEY_RIGHTMETA', 126L),
                  ('KEY_COMPOSE', 127L),
                  ('KEY_STOP', 128L),
                  ('KEY_AGAIN', 129L),
                  ('KEY_PROPS', 130L),
                  ('KEY_UNDO', 131L),
                  ('KEY_FRONT', 132L),
                  ('KEY_COPY', 133L),
                  ('KEY_OPEN', 134L),
                  ('KEY_PASTE', 135L),
                  ('KEY_FIND', 136L),
                  ('KEY_CUT', 137L),
                  ('KEY_HELP', 138L),
                  ('KEY_MENU', 139L),
                  ('KEY_CALC', 140L),
                  ('KEY_SLEEP', 142L),
                  ('KEY_WWW', 150L),
                  (['KEY_COFFEE', 'KEY_SCREENLOCK'], 152L),
                  ('KEY_BACK', 158L),
                  ('KEY_FORWARD', 159L),
                  ('KEY_EJECTCD', 161L),
                  ('KEY_NEXTSONG', 163L),
                  ('KEY_PLAYPAUSE', 164L),
                  ('KEY_PREVIOUSSONG', 165L),
                  ('KEY_STOPCD', 166L),
                  ('KEY_HOMEPAGE', 172L),
                  ('KEY_REFRESH', 173L),
                  ('KEY_EDIT', 176L),
                  ('KEY_SCROLLUP', 177L),
                  ('KEY_SCROLLDOWN', 178L),
                  ('KEY_KPLEFTPAREN', 179L),
                  ('KEY_KPRIGHTPAREN', 180L),
                  ('KEY_F13', 183L),
                  ('KEY_F14', 184L),
                  ('KEY_F15', 185L),
                  ('KEY_F16', 186L),
                  ('KEY_F17', 187L),
                  ('KEY_F18', 188L),
                  ('KEY_F19', 189L),
                  ('KEY_F20', 190L),
                  ('KEY_F21', 191L),
                  ('KEY_F22', 192L),
                  ('KEY_F23', 193L),
                  ('KEY_F24', 194L),
                  ('KEY_UNKNOWN', 240L),
                  (['BTN_LEFT', 'BTN_MOUSE'], 272L),
                  ('BTN_RIGHT', 273L),
                  ('BTN_MIDDLE', 274L),
                  (['BTN_A', 'BTN_GAMEPAD', 'BTN_SOUTH'], 304L),
                  (['BTN_B', 'BTN_EAST'], 305L),
                  ('BTN_C', 306L),
                  (['BTN_NORTH', 'BTN_X'], 307L),
                  (['BTN_WEST', 'BTN_Y'], 308L),
                  ('BTN_Z', 309L),
                  ('BTN_TL', 310L),
                  ('BTN_TR', 311L),
                  ('BTN_TL2', 312L),
                  ('BTN_TR2', 313L),
                  ('BTN_SELECT', 314L),
                  ('BTN_START', 315L),
                  ('BTN_MODE', 316L),
                  ('BTN_THUMBL', 317L),
                  ('BTN_THUMBR', 318L),
                  ('?', 319L)],
 ('EV_LED', 17L): [('LED_NUML', 0L),
                   ('LED_CAPSL', 1L),
                   ('LED_SCROLLL', 2L),
                   ('LED_COMPOSE', 3L),
                   ('LED_KANA', 4L)],
 ('EV_MSC', 4L): [('MSC_SCAN', 4L)],
 ('EV_REL', 2L): [('REL_X', 0L), ('REL_Y', 1L), ('REL_WHEEL', 8L)],
 ('EV_SYN', 0L): [('SYN_REPORT', 0L),
                  ('SYN_CONFIG', 1L),
                  ('SYN_MT_REPORT', 2L),
                  ('SYN_DROPPED', 3L),
                  ('?', 4L),
                  ('?', 17L),
                  ('?', 20L)]}

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.services.logger import Logger
from py4bot.common.intervalMapper import IntervalMapper
from py4bot.inputs.frontends.frontendUsb import FrontendUsb

# Button mapping
BUTTON_MAPPING = {
    0x072:   0,  # KEY_VOLUMEDOWN   -> digital_000
    0x073:   1,  # KEY_VOLUMEUP     -> digital_001
    0x0a3:   2,  # KEY_NEXTSONG     -> digital_002
    0x0a4:   3,  # KEY_PLAYPAUSE    -> digital_003
    0x0a5:   4,  # KEY_PREVIOUSSONG -> digital_004
    0x112:   5,  # BTN_MIDDLE       -> digital_005
    0x130:   6,  # BTN_A            -> digital_006
    0x131:   7,  # BTN_B            -> digital_007
    0x133:   8,  # BTN_NORTH        -> digital_008
    0x134:   9,  # BTN_WEST         -> digital_009
    0x136:  10,  # BTN_TL           -> digital_010
    0x137:  11,  # BTN_TR           -> digital_011
    0x138:  12,  # BTN_TL2          -> digital_012
    0x139:  13,  # BTN_TR2          -> digital_013
    0x13a:  14,  # BTN_SELECT       -> digital_014
    0x13b:  15,  # BTN_START        -> digital_015
}

# Axis mapping
AXIS_MAPPING = {
    0x00:  0,  # ABS_X      -> analog_00
    0x01:  1,  # ABS_Y      -> analog_01
    0x02:  2,  # ABS_Z      -> analog_02
    0x05:  3,  # ABS_RZ     -> analog_03
    0x10:  4,  # ABS_HAT0X  -> analog_04
    0x11:  5,  # ABS_HAT0Y  -> analog_05
}

# Axis calibration
DEFAULT_AXIS_CALIBRATION = {
    0: (  +0, +255),  # X left (ABS_X)
    1: (+255,   +0),  # Y left (ABS_Y)
    2: (  +0, +255),  # X right (ABS_Z)
    3: (+255,   +0),  # Y right (ABS_RZ)
    4: (  -1,   +1),  # X hat
    5: (  +1,   -1),  # Y hat
#    0: (-128, +127),  #  X left  (ABS_X)
#    1: (+127, -128),  #  Y left  (ABS_Y)
#    2: (-128,  127),  #  X right (ABS_Z)
#    3: (+255,   +0),  #  Y right (ABS_RZ)
#    4: (  -1,   +1),  #  X hat
#    5: (  +1,   -1)   #  Y hat
}


class IpegaPG9023(FrontendUsb):
    """ IpegaPG9023 gamepad implementation
    """
    def __init__(self, path, calibration=DEFAULT_AXIS_CALIBRATION):
        """ Init IpegaPG9023 object
        """
        super(IpegaPG9023, self).__init__("IpegaPG9023", path)

        self._intervalMapper = {}
        for axis in calibration.keys():
            self._intervalMapper[axis] = IntervalMapper(calibration[axis])

    def _keyToDigital(self, eventCode):
        code = BUTTON_MAPPING[eventCode]

        return code

    def _absToAnalog(self, eventCode, eventValue):
        axis = AXIS_MAPPING[eventCode]
        value = self._intervalMapper[axis](eventValue)

        return axis, value
