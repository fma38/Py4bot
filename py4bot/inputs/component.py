# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller component

Implements
==========

 - B{Component}

Documentation
=============

Component is the base class for DigitalComponent and AnalogComponent.

 - command          command to call (callable) - Mandatory
 - mapper           mapper to use (callable) - optional

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.services.logger import Logger


class Component(object):
    """ Base class for input components
    """
    def __init__(self, command, mapper):
        """ Init Component object
        """
        super(Component, self).__init__()

        self._command = command
        self._mapper = mapper

        self._previousFrontendState = {}
        self._frontendState = {}

    @property
    def name(self):
        return self._name

    def _frontendStateChanged(self, key):
        """ Check if frontend state changed
        """
        raise NotImplementedError

    def _update(self):
        """ Update component
        """
        raise NotImplementedError

    def update(self, frontendState):
        """ Update component with frontend state
        """
        #Logger().debug("Component.update(): frontendState=%s" % frontendState)

        self._frontendState = frontendState
        self._update()
        self._previousFrontendState.update(self._frontendState)


class DigitalComponent(Component):
    """ Base class for digital components
    """
    def _frontendStateChanged(self, key):
        return self._previousFrontendState.get(key, 0) != self._frontendState.get(key, 0)


class AnalogComponent(Component):
    """ Base class for analog components

    This class mainly add a treshold param for change detection.
    """
    def __init__(self, command, mapper, threshold=0.):
        """ Init AnalogComponent object
        """
        super(AnalogComponent, self).__init__(command, mapper)

        self._threshold = threshold

    def _frontendStateChanged(self, key):
        """ Check if frontend state changed

        This component uses the treshold value.
        """
        return self._previousFrontendState.get(key, 0) != self._frontendState.get(key, 0) and \
               (abs(self._frontendState.get(key, 0)) >= self._threshold or self._frontendState.get(key, 0) == 0)
