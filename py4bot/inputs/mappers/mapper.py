# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller mappers

Implements
==========

 - B{Mapper}
 - B{MapperStraight}
 - B{MapperSet}
 - B{MapperSetValue}
 - B{MapperWalk}

Documentation
=============

Mappers are used to translate/convert/set params coming from Components before sending to commands.

Usage
=====

>>> mapper = Mapper()
>>> mapper(3)
((3,), {})
>>> mapper(1, z=2)
((1,), {'z': 2})

>>> mapper = MapperSet('z')
>>> mapper(3)
((), {'z': 3})
>>> mapper = MapperSet('x', 'y', 'z')
>>> mapper(1, 2, 3)
((), {'x': 1, 'y': 2, 'z': 3})

>>> mapper = MapperSetValue(z=+5)
>>> mapper()
((), {'z': 5})
>>> mapper = MapperSetValue(x=1, y=2, z=3)
>>> mapper()
((), {'x': 1, 'y': 2, 'z': 3})

>>> mapper = MapperWalk()
>>> mapper(1, 2, 3)
((),
 {'rotDir': -1,
  'rotSpeed': 3,
  'trDir': 63.43494882292201,
  'trSpeed': 1.0})

TODO
====

 - find better names!
 - doc

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.common import config
from py4bot.common.exception import Py4botValueError
from py4bot.common.utils import cartesian2polar
from py4bot.services.logger import Logger


class Mapper(object):
    """ Base class for mappers
    """
    def __init__(self, *args, **kwargs):
        """ Init Mapper object
        """
        super(Mapper, self).__init__()

        #Logger().debug("Mapper.__init__(): args=%s, kwargs=%s" % (repr(args), repr(kwargs)))
        self._args = args
        self._kwargs = kwargs

    def __str__(self):
        return self.__class__.__name__

    def __call__(self, *args, **kwargs):
        raise NotImplementedError


class MapperStraight(Mapper):
    """ Straight mapper

    This mapper simply copies input args to output.
    """
    def __init__(self):
        super(MapperStraight, self).__init__()

    def __call__(self, *args, **kwargs):
        """ Make the mapper a callable

        @return: args, kwargs
        @rtype: tuple of (tuple, dict)
        """
        return args, kwargs


class MapperSet(Mapper):
    """

    This mapper sends input values to specific output args
    """
    def __init__(self, *args):
        if len(args) == 0:
            raise Py4botValueError("Missing args")

        super(MapperSet, self).__init__(*args)

    def __call__(self, *args):
        if len(args) != len(self._args):
            raise Py4botValueError("Number of args mismatch")

        kwargs = {}
        for i, arg in enumerate(args):
            kwargs[self._args[i]] = arg

        return (), kwargs


class MapperSetMultiply(MapperSet):
    """

    """
    def __init__(self, *args, **kwargs):
        super(MapperSet, self).__init__(*args)

        self._coef = kwargs['coef']

    def __call__(self, *args):
        if len(args) != len(self._args):
            raise Py4botValueError("Number of args mismatch")

        kwargs = {}
        for i, arg in enumerate(args):
            kwargs[self._args[i]] = self._coef * arg

        return (), kwargs


class MapperSetValue(Mapper):
    """

    Use to send predefined values to specific output args (mainly usefull for Button)

    -> MapperPredefined?
    """
    def __init__(self, **kwargs):
        if len(kwargs) == 0:
            raise Py4botValueError("Missing args")

        super(MapperSetValue, self).__init__(**kwargs)

    def __call__(self):
        return (), self._kwargs


class MapperToggle(Mapper):
    """

    Use to toggle value each time it is called.
    """
    def __init__(self):
        self._toggle = False

    def __call__(self):
        self._toggle = not self._toggle

        return (self._toggle,), {}


class MapperWalk(Mapper):
    """ Mapper dedicated to walk command
    """
    def __init__(self):
        super(MapperWalk, self).__init__()

    def __call__(self, x, y, rz):

        # Get linear speed/direction
        linearSpeed, direction = cartesian2polar(x, y)

        # Get yaw rotation angle speed
        angularSpeed = -rz

        # Adjust final speed
        #speed = max(linearSpeed, abs(angularSpeed))

        return (), {'linearSpeed': linearSpeed, 'direction': direction, 'angularSpeed': angularSpeed}

