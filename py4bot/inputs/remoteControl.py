# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Remote controller management

Implements
==========

 - B{RemoteControl}

Documentation
=============

The main loop poll the frontend, manage edges (clicked, press...) and call command.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time
import collections
import threading

from py4bot.common.exception import Py4botError, Py4botValueError
from py4bot.services.logger import Logger


class RemoteControl(threading.Thread):
    """
    """
    def __init__(self, robot):
        """
        """
        super(RemoteControl, self).__init__(name="RemoteControl")

        self._robot = robot

        self._frontend = self._createFrontend()

        self._configs = collections.deque()
        self._components = {'all': set()}
        self._buildComponents()

        self.setDaemon(True)

    @property
    def robot(self):
        return self._robot

    @property
    def frontend(self):
        return self._frontend

    @property
    def components(self):
        return self._components

    @property
    def configs(self):
        return self._configs

    @property
    def config(self):
        try:
            return self._configs[0]
        except IndexError:
            return None

    def _createFrontend():
        """
        """
        raise NotImplementedError

    def _addConfig(self, config):
        """
        """
        if config in self._configs or config == 'all':
            raise Py4botValueError("config already registered (%s)" % config)
        self._configs.append(config)
        self._components[config] = set()

    def _addComponent(self, cls, **kwargs):
        """
        """
        configs = kwargs.get('configs', ('all',))
        if not isinstance(configs, (list, tuple)):
            configs = (configs,)

        # Remove 'configs' key from args
        kwargs.pop('configs', None)

        for config in configs:
            try:
                self._components[config].add(cls(**kwargs))   # do we really want to instanciante a component per config?
            except KeyError:
                raise Py4botValueError("unregistered config (%s)" % config)

    def _buildComponents():
        """
        """
        raise NotImplementedError

    def selectPrevConfig(self):
        """

        @todo: check context (walk...)?
        """
        Logger().trace("RemoteControl.selectPreviousConfig()")

        self._configs.rotate(1)
        Logger().info("New controller config is '%s'" % self.config)

    def selectNextConfig(self):
        """

        @todo: check context (walk...)?
        """
        Logger().trace("RemoteControl.selectNextConfig()")

        self._configs.rotate(-1)
        Logger().info("New controller config is '%s'" % self.config)

    def run(self):
        """ Main thread loop
        """
        Logger().trace("RemoteControl.loop()")

        # Main loop
        self._running = True
        Logger().info("Starting RemoteControl loop...")
        while self._running:
            try:
                self._frontend.refresh()

                # self._frontend.feedback(self._info)  # TODO

                if self.config is not None:
                    configs = ('all', self.config)
                else:
                    configs = ('all',)
                for config in configs:
                    for component in self._components[config]:
                        component.update(self._frontend.state)

                time.sleep(0.01)

            except Py4botError:
                Logger().exception("RemoteControl.loop()")  #, debug=True)

            except:
                Logger().exception("RemoteControl.loop()")
                Logger().critical("RemoteControl unhandled exception")

        Logger().info("RemoteControl stopped")

    def stop(self):
        """
        """
        Logger().trace("RemoteControl.stop()")

        self._running = False


if __name__ == '__main__':
    import unittest

    from py4bot.inputs.frontends.dummy import FrontendDummy

    # Mute logger
    Logger().setLevel('error')

    class Func(object):
        def __init__(self, name):
            super(Func, self).__init__()
            self._name = name
        def __call__(**kwargs):
            pass
        def __repr__(self):
            return self._name

    class API(object):
        def __getattr__(self, name):
            return Func(name)

    class RemoteControlTestCase(unittest.TestCase):

        def setUp(self):
            api = API()
            class MyRemote(RemoteControl):
                pass

            self.myRemote = MyRemote()

        def tearDown(self):
            pass

        #def test_misc(self):
            #print self.myController.configs
            #print self.myController.components

    unittest.main()
