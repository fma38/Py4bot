# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller button management

Implements
==========

 - B{Button}

Documentation
=============

Button takes all DigitalComponent param, and have additionnal:

 - key              frontend key name (str) - Mandatory
 - modifier         frontend key modifier name (str) - Optional
 - trigger          way to validate the button (str) - Optional, default to 'click'
 -                  can be in ('click', 'double-click', 'press', 'release', 'hold', 'double-hold')
 - mapper           function mapper (callable) - Optional

press:
             ________....
            |
            |
            |
            |
            |
    ________|

hold:
             ________________....
            |
            |
            |
            |
            |
    ________|

             <  HOLD_DELAY  >

click:
             ____________
            |            |
            |            |
            |            |
            |            |
            |            |
    ________|            |________

            <CLICK_DELAY>

double-hold:
             ____________          ________________....
            |            |        |
            |            |        |
            |            |        |
            |            |        |
            |            |        |
    ________|            |________|

             <CLICK_DELAY>
             <         DOUBLE_HOLD_DELAY          >

double-clik:
             ____________          ____________
            |            |        |            |
            |            |        |            |
            |            |        |            |
            |            |        |            |
            |            |        |            |
    ________|            |________|            |________

             <CLICK_DELAY>
             <       DOUBLE_CLICK_DELAY       >

release:
            ....________
                        |
                        |
                        |
                        |
                        |
                        |________

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time

from py4bot.common.exception import Py4botError
from py4bot.services.logger import Logger
from py4bot.inputs.component import DigitalComponent
from py4bot.inputs.mappers.mapper import MapperStraight

# Button delays, in ms
CLICK_DELAY = 150
DOUBLE_CLICK_DELAY = 250
HOLD_DELAY = 500


class Button(DigitalComponent):
    """
    """
    def __init__(self, command, key, modifier=None, trigger='click', mapper=MapperStraight()):
        """
        """
        super(Button, self).__init__(command, mapper)

        self._key = key
        self._modifier = modifier
        self._trigger = trigger

        self._fsmState = {}
        self._pressTime = {}

    def _update(self):

        # Check if no modifier other than self._modifier is pressed
        # Modifiers are only keys starting with 'digital_'
        for key, value in self._frontendState.items():
            if value and key.startswith('digital_'):
                if self._modifier is None and key != self._key or \
                   self._modifier is not None and key not in (self._key, self._modifier):
                    return

        if self._modifier is None or self._frontendState.get(self._modifier, False):
            if self._isTriggered(self._key):
                Logger().debug("Button._update(): %s was triggered" % self._key)
                try:

                    # Apply mapper
                    #Logger().debug("Button._update(): call %s mapper" % self._mapper)
                    args, kwargs = self._mapper()
                    #Logger().debug("Button._update(): mapper returned %s, %s" % (repr(args), repr(kwargs)))

                    # Call command
                    #Logger().debug("Button._update(): call %s command" % self._command)
                    self._command(*args, **kwargs)

                except Py4botError:
                    Logger().exception("Button._update()")

    def _isTriggered(self, button):
        """
        """
        trigger = {}

        fsmState = self._fsmState.get(button, 'idle')
        if fsmState == 'idle':
            if self._frontendStateChanged(button) and self._frontendState.get(button, 0):
                trigger['press'] = True
                self._pressTime[button] = time.time()
                self._fsmState[button] = 's1'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from idle to s1" % button)

        elif fsmState == 's1':
            if time.time() - self._pressTime[button] > HOLD_DELAY / 1000.:
                trigger['hold'] = True
                self._fsmState[button] = 's4'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s1 to s4" % button)

            elif self._frontendStateChanged(button) and not self._frontendState[button]:
                trigger['release'] = True
                if time.time() - self._pressTime[button] < CLICK_DELAY / 1000.:
                    self._fsmState[button] = 's2'
                    #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s1 to s2" % button)
                else:
                    self._fsmState[button] = 'idle'
                    #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s1 to idle" % button)

        elif fsmState == 's2':
            if time.time() - self._pressTime[button] > DOUBLE_CLICK_DELAY / 1000.:
                trigger['click'] = True
                self._fsmState[button] = 'idle'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s2 to idle" % button)

            elif self._frontendStateChanged(button) and self._frontendState[button]:
                trigger['press'] = True
                if time.time() - self._pressTime[button] < DOUBLE_CLICK_DELAY / 1000.:
                    self._pressTime[button] = time.time()
                    self._fsmState[button] = 's3'
                    #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s2 to s3" % button)
                else:
                    self._pressTime[button] = time.time()
                    self._fsmState[button] = 's1'
                    #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s2 to s1" % button)

        elif fsmState == 's3':
            if time.time() - self._pressTime[button] > HOLD_DELAY / 1000.:
                trigger['double-hold'] = True
                self._fsmState[button] = 's4'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s3 to s5" % button)

            elif self._frontendStateChanged(button) and not self._frontendState[button]:
                trigger['release'] = True
                if time.time() - self._pressTime[button] < CLICK_DELAY / 1000.:
                    trigger['double-click'] = True
                self._fsmState[button] = 'idle'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s3 to idle" % button)

        elif fsmState == 's4':
            if self._frontendStateChanged(button) and not self._frontendState[button]:
                trigger['release'] = True
                self._fsmState[button] = 'idle'
                #Logger().debug("Button._isTriggered(): button %s, switch FSM state from s4 to idle" % button)

        # Handle trigger
        if True in trigger.values():
            #Logger().debug("Button._isTriggered(): trigger=%s" % trigger)
            for key, value in trigger.items():
                if key == self._trigger and value:
                    return True

        return False
