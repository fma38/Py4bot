# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input controller joystick management

Implements
==========

 - B{Joystick}

Documentation
=============

A  Joystick can be used to translate/rotate the robot: X/Y axes are used to compute speed/direction, using the mapper.

It takes all AnalogComponent params, and have additionnal:

 - keys             frontend key(s) name (tuple/list of str) - Mandatory
 - modifier         frontend key modifier name (str) - Optional
 - curves           curve(s) type of the transmitted values (tuple/list of str) - Optional, default to 'lin'
                    can be in ('lin', log', exp', 'pow2', pow3')
 - mapper           function mapper (callable) - Optional

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.common.exception import Py4botError, Py4botValueError
from py4bot.services.logger import Logger
from py4bot.inputs.component import AnalogComponent
from py4bot.inputs.curves.curve import CurveStd
from py4bot.inputs.mappers.mapper import MapperStraight


class Joystick(AnalogComponent):
    """
    """
    def __init__(self, command, keys, threshold=0., modifier=None, curves=CurveStd(), mapper=MapperStraight()):
        """
        """
        super(Joystick, self).__init__(command, mapper, threshold)

        if not isinstance(keys, (tuple, list)) or len(keys) < 2:
            raise Py4botValueError("Joystick needs at least 2 keys")

        self._keys = keys
        self._modifier = modifier

        if isinstance(curves, (tuple, list)):
            if len(curves) != len(keys):
                raise Py4botValueError("Joystick needs as much curves as keys")
            self._curves = curves
        else:
            self._curves = len(keys) * (curves,)

    def _update(self):

        # Check if no modifier other than self._modifier is pressed
        # Modifiers are only keys starting with 'digital_'
        for key, value in self._frontendState.items():
            if value and key.startswith('digital_'):
                if self._modifier is None  or \
                   self._modifier is not None and key != self._modifier:
                    return

        if self._modifier is None or self._frontendState.get(self._modifier, False):
            updated = False
            values = []
            for key, curve in zip(self._keys, self._curves):

                # Read value from frontend
                value = self._frontendState.get(key, 0.)  # 0. handle very first calls
                try:

                    # Apply curve
                    #Logger().debug("Joystick._update(): call %s curve" % curve)
                    value = curve(value)

                except Py4botError:
                    Logger().exception("Joystick._update()")

                values.append(value)
                if self._frontendStateChanged(key):
                    values[-1] = self._frontendState[key]
                    updated = True

            if updated:
                try:

                    #Logger().debug("Joystick._update(): call %s mapper" % self._mapper)
                    args, kwargs = self._mapper(*values)
                    #Logger().debug("Joystick._update(): mapper returned %s, %s" % (repr(args), repr(kwargs)))
                    #Logger().debug("Joystick._update(): call %s command" % self._command)
                    self._command(*args, **kwargs)

                except Py4botError:
                    Logger().exception("Joystick._update()")
