# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Gaits implementation

Implements
==========

 - B{GaitSequencer}

Documentation
=============

The gait sequencer is responsible of generating positions, using a given gait.

Its run() method should run in a thread (or coroutine).

Usage
=====

>>> sequencer = GaitSequencer()
>>> sequencer.start()
>>> sequencer.linearSpeed = 0.5
>>> sequencer.walkStart()
>>> sequencer.walkStop()

TODO
====

 * use a class of interpolation of leg movement when lifting it up

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time
import threading

from py4bot.common import config
from py4bot.common.singleton import Singleton
from py4bot.common.signal import Signal
from py4bot.common.exception import Py4botError, Py4botIKError, Py4botFsmError
from py4bot.services.logger import Logger
from py4bot.common.fsm import FSM
from py4bot.gaits.gaitManager import GaitManager


class GaitSequencer_(FSM, threading.Thread):
    """ Gait sequencer

    This sequencer is a singleton running in a thread, which generates feet positions according to inputs (from gamepad or so).

    This class implements a finite-states machine to switch between the different phase of the gait.
    It manages the changes of speed/direction, gait, which can only occure in certain phases/states.

    @ivar _gait: current gait
    @type _gait: Gait

    @ivar _step: current step (sub-steps are allowed; use decimals - ???!!!???)
    @type _step: float

    @ivar _state: current state of the finite-state machine
                  'idle': do nothing
                  'start': start sequence
                  'pause': pause sequence
                  'walk': walking sequence
                  'static_walk': static walking sequence
                  'stop': stop sequence
    @type _state: str

    @ivar updateWalkSignal: Signal sent when walk position changed
    @type updateWalkSignal: Signal
    """
    #__metaclass__ = Singleton

    def __init__(self):
        """ GaitSequencer implementation
        """
        #super(GaitSequencer_, self).__init__(name="GaitSequencer")
        FSM.__init__(self)
        threading.Thread.__init__(self, name="GaitSequencer")

        self._state = 'idle'
        self._step = 0

        self._linearSpeed = 0.
        self._direction = 0.
        self._angularSpeed = 0.

        self._updateWalkSignal = Signal()

        self._lock = threading.RLock()

        self.setDaemon(True)

    @property
    def state(self):
        return self._state

    @property
    def step(self):
        return self._step

    @property
    def updateWalkSignal(self):
        return self._updateWalkSignal

    @property
    def linearSpeed(self):
        return self._linearSpeed

    @linearSpeed.setter
    def linearSpeed(self, linearSpeed):
        self._linearSpeed = linearSpeed

    @property
    def direction(self):
        return self._direction

    @direction.setter
    def direction(self, direction):
        self._direction = direction

    @property
    def angularSpeed(self):
        return self._angularSpeed

    @angularSpeed.setter
    def angularSpeed(self, angularSpeed):
        self._angularSpeed = angularSpeed

    def _createTransitions(self):
        self._addTransition(event='to_idle', src='stop', dst='idle', onAfter=self._onIdle)
        self._addTransition(event='to_idle', src='static_walk', dst='idle', onAfter=self._onIdle)
        self._addTransition(event='to_start', src='idle', dst='start', onAfter=self._onStart)
        self._addTransition(event='to_walk', src='start', dst='walk', onAfter=self._onWalk)
        self._addTransition(event='to_pause', src='start', dst='pause', sync=False, onBefore=self._onRegisterPause, onAfter=self._onPause)
        self._addTransition(event='to_pause', src=('walk', 'step'), dst='pause', onAfter=self._onPause)
        self._addTransition(event='to_step', src='pause', dst='step', onAfter=self._onStep)
        self._addTransition(event='to_static_walk', src='idle', dst='static_walk', onAfter=self._onStaticWalk)
        self._addTransition(event='to_resume', src='pause', dst='walk', onAfter=self._onResume)
        self._addTransition(event='to_stop', src=('start', 'walk'), dst='stop', sync=False, onBefore=self._onRegisterStop, onAfter=self._onStop)
        self._addTransition(event='to_stop', src=('pause', 'step'), dst='walk_then_stop', onAfter=self._onResume)
        self._addTransition(event='to_stop', src='walk_then_stop', dst='stop', onAfter=self._onStop)

    def _onRegisterPause(self, event):
        """
        """
        Logger().info("Pause registered")

    def _onRegisterStop(self, event):
        """
        """
        Logger().info("Stop registered")

    def _onIdle(self, event):
        """
        """
        Logger().info("Enter idle")

    def _onStart(self, event):
        """
        """
        Logger().info("Start sequence")
        self._step = 0

    def _onWalk(self, event):
        """
        """
        Logger().info("Walk sequence")
        self._step = 0

    def _onPause(self, event):
        """
        """
        self._pauseAt = time.time()
        Logger().info("Paused")

    def _onStep(self, event):
        """
        """
        Logger().info("Step")

    def _onStaticWalk(self, event):
        """
        """
        Logger().info("Static walk sequence")
        self._step = 0

    def _onResume(self, event):
        """
        """
        Logger().info("Resume walk")

    def _onStop(self, event):
        """
        """
        Logger().info("Stop sequence")
        self._step = 0

    def selectPrevGait(self):
        """ Select previous gait

        The gait switch can only occurs at idle state.
        """
        Logger().trace("GaitSequencer.setectPrevGait()")
        if self._state == 'idle':
            GaitManager().selectPrev()
            Logger().info("New gait is '%s'" % GaitManager().gait.name)

        else:
            Logger().warning("Can't switch gait while walking")

    def selectNextGait(self):
        """ Select next gait

        The gait switch can only occurs at idle state.
        """
        Logger().trace("GaitSequencer.setectNextGait()")
        if self._state == 'idle':
            GaitManager().selectNext()
            Logger().info("New gait is '%s'" % GaitManager().gait.name)

        else:
            Logger().warning("Can't switch gait while walking")

    def reset(self):
        """ Reset sequencer
        """
        self._state = 'idle'
        self._step = 0

        self._linearSpeed = 0.
        self._direction = 0.
        self._angularSpeed = 0.

    def walkStart(self):
        """ Ask the sequencer to start walking
        """
        self._lock.acquire()
        try:
            Logger().info("Start triggered")
            self.trigger('to_start')
        finally:
            self._lock.release()

    def walkPause(self):
        """ Ask the sequencer to pause walking
        """
        self._lock.acquire()
        try:
            Logger().info("Pause triggered")
            self._pauseAt = time.time()
            self.trigger('to_pause')
        finally:
            self._lock.release()

    def walkStep(self):
        """ Ask the sequencer to walk a step
        """
        self._lock.acquire()
        try:
            Logger().info("Step triggered")
            self.trigger('to_step')
        finally:
            self._lock.release()

    def walkResume(self):
        """ Ask the sequencer to resume walking
        """
        self._lock.acquire()
        try:
            Logger().info("Resume triggered")
            self.trigger('to_resume')
        finally:
            self._lock.release()

    def walkStop(self):
        """ Ask the sequencer to stop walking
        """
        self._lock.acquire()
        try:
            Logger().info("Stop triggered")
            self.trigger('to_stop')
        finally:
            self._lock.release()

    def walkStatic(self):
        """ Ask the sequencer to static walk
        """
        self._lock.acquire()
        try:
            Logger().info("Static triggered")
            self.trigger('to_static_walk')
        finally:
            self._lock.release()

    def walk(self, linearSpeed, direction, angularSpeed):
        """

        Called by a Controller object.
        """
        Logger().debug("GaitSequencer.walk(): linearSpeed=%.1f, direction=%d, angularSpeed=%.1f" % (linearSpeed, direction, angularSpeed))

        if round(linearSpeed, 1) != 0. or round(angularSpeed, 1) != 0:
            self._linearSpeed = linearSpeed
            self._direction = direction
            self._angularSpeed = angularSpeed

            if self._state == 'idle':
                self.walkStart()

            elif self._state == 'pause':
                self.walkResume()

        else:
            if self._state != 'idle':
                self.walkPause()

    def run(self):
        """ GaitSequencer main loop

        State machine.

        @todo: split and move loop in each 'onAfter' handlers?
        """
        Logger().trace("GaitSequencer.run()")

        Logger().info("Starting GaitSequencer loop...")

        # Main loop
        self._running = True
        while self._running:
            #Logger().debug("GaitSequencer.run(): state=%s, step=%.1f" % (self.state, self._step))
            try:
                self._lock.acquire()
                try:
                    if self._state == 'start':  # TODO: handle error -> stay on current step, and switch to ???
                        #Logger().debug("GaitSequencer.run(): start sequence, step=%.1f" % self._step)

                        try:
                            feetTarget, duration = GaitManager().gait.startSequence(self._step, self._linearSpeed, self._direction, self._angularSpeed)
                            self._updateWalkSignal.emit(feetTarget, duration)
                            self._step += 1

                        except StopIteration:
                            self._step = 0
                            try:
                                self.trigger('to_walk')
                            except Py4botFsmError:
                                self.transition()  # execute pending stop/pause transition

                    elif self._state == 'walk':
                        #Logger().debug("GaitSequencer.run(): walk sequence, step=%.1f" % self._step)

                        feetTarget, duration = GaitManager().gait.walkSequence(self._step, self._linearSpeed, self._direction, self._angularSpeed)
                        self._updateWalkSignal.emit(feetTarget, duration)

                        self._step += 1
                        self._step %= GaitManager().gait.nbSteps

                        # In case a stop condition is pending
                        if self._step == 0 and self.transitionPending():
                            self.transition()

                    elif self._state == 'walk_then_stop':
                        #Logger().debug("GaitSequencer.run(): walk sequence, step=%.1f" % self._step)

                        # Check if ready to switch to stop sequence
                        if self._step == 1:
                            self.trigger('to_stop')
                            continue

                        feetTarget, duration = GaitManager().gait.walkSequence(self._step, self._linearSpeed, self._direction, self._angularSpeed)
                        self._updateWalkSignal.emit(feetTarget, duration)

                        self._step += 1
                        self._step %= GaitManager().gait.nbSteps

                    elif self._state == 'pause':
                        #Logger().debug("GaitSequencer.run(): pause sequence, step=%.1f" % self._step)

                        # Handle auto return to stop delay
                        if config.WALK_PAUSE_TO_STOP_DELAY is not None and time.time() - self._pauseAt > config.WALK_PAUSE_TO_STOP_DELAY:
                            self.trigger('to_stop')

                    elif self._state == 'static_walk':
                        #Logger().debug("GaitSequencer.run(): static walk sequence, step=%.1f" % self._step)

                        try:
                            feetTarget, duration = GaitManager().gait.staticWalkSequence(self._step, linearSpeed=0.25)
                            self._updateWalkSignal.emit(feetTarget, duration)
                            self._step += 1

                        except StopIteration:
                            self._step = 0
                            self.trigger('to_idle')

                    elif self._state == 'step':
                        #Logger().debug("GaitSequencer.run(): step sequence, step=%.1f" % self._step)

                        feetTarget, duration = GaitManager().gait.walkSequence(self._step, self._linearSpeed, self._direction, self._angularSpeed)
                        self._updateWalkSignal.emit(feetTarget, duration)

                        self._step += 1
                        self._step %= GaitManager().gait.nbSteps

                        # Back to pause
                        self.trigger('to_pause')

                    elif self._state == 'stop':
                        #Logger().debug("GaitSequencer.run(): stop sequence, step=%.1f" % self._step)

                        try:
                            feetTarget, duration = GaitManager().gait.stopSequence(self._step, self._linearSpeed, self._direction, self._angularSpeed)
                            self._updateWalkSignal.emit(feetTarget, duration)
                            self._step += 1

                        except StopIteration:
                            self._step = 0
                            self.trigger('to_idle')
                finally:
                    self._lock.release()

                time.sleep(0.01)

            except Py4botIKError:
                Logger().warning("Ik error; skipping step")

            except Py4botError:
                Logger().exception("GaitSequencer.run()")  #, debug=True)

            except:
                Logger().exception("GaitSequencer.run()")
                Logger().critical("GaitSequencer crashed")
                raise SystemExit

        Logger().info("GaitSequencer stopped")

    def stop(self):
        """ stop sequencer
        """
        Logger().trace("GaitSequencer.stop()")

        self._running = False


gaitSequencer = GaitSequencer_()
def GaitSequencer():
    return gaitSequencer


def echo(feetTarget, duration):
    print("feetTarget['RM']=(x=%.1f, y=%.1f, z=%.1f), duration=%.1f" % \
          (feetTarget['RM'][1][0], feetTarget['RM'][1][1], feetTarget['RM'][1][2], duration))


def main():
    from py4bot.gaits.gaitRiple import GaitRiple

    Logger().setLevel('debug')

    GAIT_PARAMS = {
        'length': 20.,
        'angle': 10.,
        'height': 20.,
        'minLength': 4.,
        'minAngle': 2.,
        'speedMin': 50.,
        'speedMax': 200.
    }

    gait = GaitRiple('riple', (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)), GAIT_PARAMS)
    GaitManager().add(gait)

    gaitSeq = GaitSequencer()
    gaitSeq.updateWalkSignal.connect(echo)
    gaitSeq.start()
    gaitSeq._direction = 90.
    try:
        time.sleep(2)
        gaitSeq.trigger('to_start')
        time.sleep(4.5)
        gaitSeq.trigger('to_pause')
        time.sleep(2)
        gaitSeq.transition()
        time.sleep(2)
        gaitSeq.trigger('to_step')
        time.sleep(1)
        gaitSeq.trigger('to_step')
        time.sleep(2)
        gaitSeq.trigger('to_resume')
        time.sleep(2)
        gaitSeq.trigger('to_stop')
        time.sleep(5)
        gaitSeq.trigger('to_static_walk')
        time.sleep(5)
    finally:
        gaitSeq.stop()
        gaitSeq.join()


if __name__ == "__main__":
    main()
