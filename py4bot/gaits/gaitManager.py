# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Gaits management

Implements
==========

 - B{GaitManager}

Documentation
=============

Manage gaits.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""
import collections

from py4bot.common import config
#from py4bot.common.singleton import Singleton
from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger


class GaitManager_(object):
    """ Manager class for gaits
    """
    #__metaclass__ = Singleton

    def __init__(self):
        """ Init GaitManager object
        """
        super(GaitManager_, self).__init__()

        self._gaits = collections.OrderedDict()
        self._gaitIndex = 0

    @property
    def gait(self):
        """ Return currently selected gait
        """
        try:
            gaitName = list(self._gaits.keys())[self._gaitIndex]
        except IndexError:
            raise Py4botValueError("no gait available")

        return self._gaits[gaitName]

    def add(self, gait):
        """ Add a new gait
        """
        self._gaits[gait.name] = gait

    def select(self, gaitName):
        """ Select gait by name

        @param gaitName: name of the gait to select
        @type gaitName: str
        """
        Logger().trace("GaitManager.select()")

        try:
            self._gaitIndex = list(self._gaits.keys()).index(gaitName)
        except ValueError:
            raise Py4botValueError("unknown '%s' gait" % gaitName)

    def selectNext(self):
        """ Switch to next gait
        """
        Logger().trace("GaitManager.selectNext()")

        self._gaitIndex += 1
        if self._gaitIndex >= len(self._gaits):
            self._gaitIndex = 0

    def selectPrev(self):
        """ Switch to previous gait
        """
        Logger().trace("GaitManager.selectPrev()")

        self._gaitIndex -= 1
        if self._gaitIndex < 0:
            self._gaitIndex = len(self._gaits) - 1


gaitManager = GaitManager_()
def GaitManager():
    return gaitManager
