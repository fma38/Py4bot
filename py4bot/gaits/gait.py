# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Gaits implementation

Implements
==========

 - B{Gait}

Documentation
=============

The gait is made of B{steps}. The number of steps depends on the gait: for example, a B{tripod} gait has 4 steps.
In each step, one or more legs can move at the same time, synchronized. They form a B{group}.

A B{cycle} contains all the steps of the gait to return to the inital position (which can be different than neutral!)

Usage
=====

To implement a new gait, juste inherit from Gait, and implement _createTables() method.
This method should return 4 tables:

 - start table
 - walk table
 - stop table
 - static table

TODO
====

 - allow to pause anywhere in the cycle

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math
import copy
import collections

import numpy

from py4bot.common import config
from py4bot.common.exception import Py4botValueError
from py4bot.common.rotations import Rz
from py4bot.services.logger import Logger


class Gait(object):
    """ Gait base class

    @ivar _name: name of the gait
    @type _name: str

    @ivar _startTable: start positions table
    @type _startTable: tuple

    @ivar _walkTable: walk positions table
    @type _walkTable: tuple of tuple of float

    @ivar _stopTable: stop positions table
    @type _stopTable: tuple

    @ivar _legsIndexGroups: group of legs index to move together
    @type _legsIndexGroups: tuple

    @ivar _stepLength: full length of a step (from neutral to full forward or full backward), in mm
    @type _stepLength: float

    @ivar _stepAngle: full angle of a step when turning on itself, in °
    @type _stepAngle: float

    @ivar _stepHeight: height to use to lift up active legs group, in mm
    @type _stepHeight: float

    @ivar _step: current step (sub-steps are allowed; use decimals)
    @type _step: float
    """

    def __init__(self, name, legsIndexGroups, params):
        """ Gait implementation

        @param name: name of the gait
        @type name: str

        @param legsIndexGroups: groups of legs moving together
        @type legsIndexGroups: tuple

        @param params: gait params
        @type params: dict
        """
        super(Gait, self).__init__()

        self._name = name
        self._legsIndexGroups = legsIndexGroups
        self._params = params

        self._startTable, self._walkTable, self._staticTable, self._stopTable = self._createTables()

        self._stepLength = self._params['length']
        self._stepAngle = self._params['angle']
        self._stepHeightMin = self._params['height']['min']
        self._stepHeightMax = self._params['height']['max']
        self._speedMin = self._params['speed']['min']
        self._speedMax = self._params['speed']['max']

    @property
    def name(self):
        return self._name

    @property
    def nbSteps(self):
        return len(self._walkTable)

    @property
    def legsIndexGroups(self):
        return self._legsIndexGroups

    def _createTables(self):
        """ Create and returns start/walk/static/stop tables
        """
        raise NotImplementedError

    def _walk(self, table, step, linearSpeed, direction, angularSpeed):
        """ Low level walk implementation

        @param table: table to use in the current walking step
        @type table: tuple

        @param step: current step
        @type step: int

        @param linearSpeed: linear moving speed, in range (0, 1.)
        @type linearSpeed: float

        @param direction: direction to translate, in ° (trigo: E=0, N=90, W=180, S=270)
        @type direction: int

        @param angularSpeed: angular speed, in range (0., 1.)
        @type angularSpeed: float
        """
        feetTarget = {}

        # Iterate over all groups
        for numGroup, legsIndexGroup in enumerate(self._legsIndexGroups):

            # Get gait table coef.
            coef = table[step][numGroup][0]
            lift = table[step][numGroup][1]
            #Logger().debug("Gait._walk(): numGroup=%d, coef=%.2f, lift=%d" % (numGroup, coef, lift))

            # Compute rotation matrix (arround robot center)
            if angularSpeed:
                sens = angularSpeed / abs(angularSpeed)
                angle = coef * sens * self._stepAngle
                footR = Rz(angle)
            else:
                footR = numpy.identity(3)

            # Compute translation vector
            if linearSpeed:
                footT = numpy.matrix([[math.cos(math.radians(direction))],
                                      [math.sin(math.radians(direction))],
                                      [0.]])
                footT *= coef * self._stepLength
            else:
                footT = numpy.zeros((3, 1))

            # Lift leg if needed
            # TODO: use min/max (inverted?)
            footT[2] = lift * self._stepHeightMax

            # Iterate over all legs in the group
            for legIndex in legsIndexGroup:
                feetTarget[legIndex] = (footR, footT)

        speed = linearSpeed if linearSpeed else abs(angularSpeed)
        movingSpeed = self._speedMin + (self._speedMax - self._speedMin) * speed
        duration = self._stepLength / movingSpeed

        return feetTarget, duration

    def startSequence(self, step, linearSpeed, direction, angularSpeed):
        """ Start sequence

        @param step: current step
        @type step: int

        @param linearSpeed: linear moving speed, in range (0, 1.)
        @type linearSpeed: float

        @param direction: direction to translate, in ° (trigo: E=0, N=90, W=180, S=270)
        @type direction: int

        @param angularSpeed: angular speed, in range (0., 1.)
        @type angularSpeed: float
        """
        if len(self._startTable) == 0 or step >= len(self._startTable):
            raise StopIteration

        return self._walk(self._startTable, step, linearSpeed, direction, angularSpeed)

    def walkSequence(self, step, linearSpeed, direction, angularSpeed):
        """ Walk sequence

        @param step: current step
        @type step: int

        @param linearSpeed: linear moving speed, in range (0, 1.)
        @type linearSpeed: float

        @param direction: direction to translate, in ° (trigo: E=0, N=90, W=180, S=270)
        @type direction: int

        @param angularSpeed: angular speed, in range (0., 1.)
        @type angularSpeed: float
        """
        return self._walk(self._walkTable, step, linearSpeed, direction, angularSpeed)

    def stopSequence(self, step, linearSpeed, direction, angularSpeed):
        """ Stop sequence

        @param step: current step
        @type step: int

        @param linearSpeed: linear moving speed, in range (0, 1.)
        @type linearSpeed: float

        @param direction: direction to translate, in ° (trigo: E=0, N=90, W=180, S=270)
        @type direction: int

        @param angularSpeed: angular speed, in range (0., 1.)
        @type angularSpeed: float
        """
        if len(self._stopTable) == 0 or step >= len(self._stopTable):
            raise StopIteration

        return self._walk(self._stopTable, step, linearSpeed, direction, angularSpeed)

    def staticWalkSequence(self, step, linearSpeed):
        """ Static walk sequence

        @param step: current step
        @type step: int

        @param linearSpeed: linear moving speed, in range (0, 1.)
        @type linearSpeed: float
        """
        if len(self._staticTable) == 0 or step >= len(self._staticTable):
            raise StopIteration

        return self._walk(self._staticTable, step, linearSpeed, direction=0, angularSpeed=0)
