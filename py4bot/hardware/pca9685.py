# -*- coding: utf-8 -*-

""" Py4bot framework.

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

Copyright (c) 2012-2013 Limor Fried, Kevin Townsend and Mikey Sklar for Adafruit Industries.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 - Neither the name of the nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module purpose
==============

PCA 9685 chip suport.

Implements
==========

 - B{Pca9685}
 - B{Pca9685Output}
 - B{Pca9685Servo}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@author: Limor Fried
@author: Kevin Townsend
@author: Mikey Sklar
@copyright: (C) 2014-2021 Frédéric Mantegazza
@copyright: (C) 2012-2013 Adafruit Industries
@license: ???
"""

from __future__ import division

import time

from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger

# Registers/etc.
MODE1         = 0x00
MODE2         = 0x01
SUBADR1       = 0x02
SUBADR2       = 0x03
SUBADR3       = 0x04
ALLCALLADR    = 0x05
LED0_ON_L     = 0x06
LED0_ON_H     = 0x07
LED0_OFF_L    = 0x08
LED0_OFF_H    = 0x09
ALL_LED_ON_L  = 0xfa
ALL_LED_ON_H  = 0xfb
ALL_LED_OFF_L = 0xfc
ALL_LED_OFF_H = 0xfd
PRE_SCALE     = 0xfe
TestMode      = 0xff

# Bits MODE 1
RESTART       = 0x80
EXTCLK        = 0x40
AI            = 0x20
SLEEP         = 0x10
SUB1          = 0x08
SUB2          = 0x04
SUB3          = 0x02
ALLCALL       = 0x01

# Bits MODE 2
INVRT         = 0x10
OCH           = 0x08
OUTDRV        = 0x04
OUTNE         = 0x03


class Pca9685(object):
    """
    """
    RESOLUTION = 4096
    NB_OUTPUT = 16

    def __init__(self, i2cBus, address):
        """
        """
        super(Pca9685, self).__init__()

        self._i2cBus = i2cBus
        self._address = address

        self.setAllPwm(0, 0)
        self._i2cBus.write8(self._address, MODE2, OUTDRV)
        self._i2cBus.write8(self._address, MODE1, ALLCALL)
        time.sleep(0.005)

        mode = self._i2cBus.read8(self._address, MODE1)
        self._i2cBus.write8(self._address, MODE1, mode & ~SLEEP)
        time.sleep(0.005)

    @property
    def address(self):
        return self._address

    @property
    def i2cBus(self):
        return self._i2cBus

    def writeRegister(self, register, value):
        """
        """
        self._i2cBus.write8(self._address, register, value)

    def readRegister(self, register):
        """
        """
        return self._i2cBus.read8(self._address, register)

    def setFrequency(self, freq):
        """ Sets the PWM frequency
        """
        prescaleValue = round(25e6 / (Pca9685.RESOLUTION * freq)) -1  # 25MHz, 12-bit

        mode = self._i2cBus.read8(self._address, MODE1);
        self._i2cBus.write8(self._address, MODE1, (mode & ~RESTART) | SLEEP)
        self._i2cBus.write8(self._address, PRE_SCALE, int(prescaleValue))
        self._i2cBus.write8(self._address, MODE1, mode)
        time.sleep(0.005)
        self._i2cBus.write8(self._address, MODE1, mode | RESTART)

    def setOn(self, channel, on):
        """ Set pwm on timing for a single channel
        """
        self._i2cBus.write8(self._address, LED0_ON_L+4*channel, on & 0xFF)
        self._i2cBus.write8(self._address, LED0_ON_H+4*channel, on >> 8)

    def setOff(self, channel, off):
        """ Set pwm off timing for a single channel
        """
        self._i2cBus.write8(self._address, LED0_OFF_L+4*channel, off & 0xFF)
        self._i2cBus.write8(self._address, LED0_OFF_H+4*channel, off >> 8)

    def setAllOn(self, on):
        """ Set pwm on timing for all channels
        """
        self._i2cBus.write8(self._address, ALL_LED_ON_L, on & 0xFF)
        self._i2cBus.write8(self._address, ALL_LED_ON_H, on >> 8)

    def setAllOff(self, off):
        """ Set pwm off timing for all channels
        """
        self._i2cBus.write8(self._address, ALL_LED_OFF_L, off & 0xFF)
        self._i2cBus.write8(self._address, ALL_LED_OFF_H, off >> 8)

    def setPwm(self, channel, on, off):
        """ Set complete pwm for a single channel
        """
        self.setOn(channel, on)
        self.setOff(channel, off)

    def setAllPwm(self, on, off):
        """ Set complete pwm for all channels
        """
        self.setAllOn(on)
        self.setAllOff(off)

    def writeBlock(self, reg, onOff):
        """ Writes an array of bytes
        """
        self._i2cBus.writeBlock(self._address, reg, onOff)


class Pca9685Output(object):
    """
    """
    def __init__(self, pca9685, channel):
        """
        """
        super(Pca9685Output, self).__init__()

        self._pca9685 = pca9685
        self._channel = channel

        self._on = 0   # ticks
        self._off = 0  # ticks

    @property
    def pca9685(self):
        return self._pca9685

    @property
    def channel(self):
        return self._channel

    @property
    def on(self):
        return self._on

    @on.setter
    def on(self, on):
        self._pca9685.setOn(self._channel, on)
        self._on = on

    @property
    def off(self):
        return self._off

    @off.setter
    def off(self, off):
        self._pca9685.setOff(self._channel, off)
        self._off = off


class Pca9685Servo(Pca9685Output):
    """
    """
    def __init__(self, pca9685, channel, timePerClick):
        """
        """
        super(Pca9685Servo, self).__init__(pca9685, channel)

        self._timePerClick = timePerClick

        self._pulse = 0  # in µs

    @property
    def pulse(self):
        return self._pulse

    @pulse.setter
    def pulse(self, pulse):
        self.off = int(round(pulse / self._timePerClick))
        self._pulse = pulse
