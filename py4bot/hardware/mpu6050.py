# -*- coding: utf-8 -*-

""" Py4bot framework.

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 - Neither the name of the nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module purpose
==============

MPU-6050 support.

Implements
==========

 - B{Mpu6050}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2017-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math
import copy

from py4bot.hardware.i2cBus import I2cBus

GRAVITIY_MS2 = 9.80665

GYRO_CONFIGS = {
    '250':  {'range': 0x00, 'scale': 131.0},
    '500':  {'range': 0x08, 'scale':  65.5},
    '1000': {'range': 0x10, 'scale':  32.8},
    '2000': {'range': 0x18, 'scale':  16.4},
}

ACCEL_CONFIGS = {
    '2g':  {'range': 0x00, 'scale': 16384},
    '4g':  {'range': 0x08, 'scale':  8192},
    '8g':  {'range': 0x10, 'scale':  4096},
    '16g': {'range': 0x18, 'scale':  2048},
}

GYRO_CONFIG_REG = 0x1b
ACCEL_CONFIG_REG = 0x1c

ACCEL_START_BLOCK_REG = 0x3b
ACCEL_XOUT_H_INDEX = 0
ACCEL_XOUT_L_INDEX = 1
ACCEL_YOUT_H_INDEX = 2
ACCEL_YOUT_L_INDEX = 3
ACCEL_ZOUT_H_INDEX = 4
ACCEL_ZOUT_L_INDEX = 5

TEMP_START_BLOCK_REG = 0x41
TEMP_OUT_H_INDEX = 0
TEMP_OUT_L_INDEX = 1

GYRO_START_BLOCK_REG = 0x43
GYRO_XOUT_H_INDEX = 0
GYRO_XOUT_L_INDEX = 1
GYRO_YOUT_H_INDEX = 2
GYRO_YOUT_L_INDEX = 3
GYRO_ZOUT_H_INDEX = 4
GYRO_ZOUT_L_INDEX = 5

PWR_MGMT_1_REG = 0x6B
PWR_MGMT_2_REG = 0x6C


class Mpu6050(object):
    """
    """
    def __init__(self, i2cBus, address=0x68, gyroConfig='250', accelConfig='2g'):
        """ Init Mpu6050 object
        """

        self._i2cBus = i2cBus
        self._address = address
        self._gyroConfig = gyroConfig
        self._accelConfig = accelConfig

        self._rawGyroData = [0, 0, 0, 0, 0, 0]
        self._rawAccelData = [0, 0, 0, 0, 0, 0]
        self._rawTempData = [0, 0]

        self._gyro = {
            'raw': {'x': 0, 'y': 0, 'z': 0},
            'scaled': {'x': 0, 'y': 0, 'z': 0}
        }

        self._temp = {
            'raw': 0,
            'scaled': 0
        }

        self._accel = {
            'raw': {'x': 0, 'y': 0, 'z': 0},
            'scaled': {'x': 0, 'y': 0, 'z': 0}
        }

        self._orientation = {
            'raw': {'yaw': 0, 'pitch': 0, 'roll': 0},
            'filtered': {'yaw': 0, 'pitch': 0, 'roll': 0}
        }

        # We need to wake up the module as it start in sleep mode
        self._i2cBus.write8(self._address, PWR_MGMT_1_REG, 0)

        # Set the scales
        self._i2cBus.write8(self._address, GYRO_CONFIG_REG, GYRO_CONFIGS[self._gyroConfig]['range'])
        self._i2cBus.write8(self._address, ACCEL_CONFIG_REG, ACCEL_CONFIGS[self._accelConfig]['range'])

    @property
    def gyro(self):
        return copy.deepcopy(self._gyro)

    @property
    def accel(self):
        return copy.deepcopy(self._accel)

    @property
    def temp(self):
        return copy.deepcopy(self._temp)

    @property
    def orientation(self):
        return copy.deepcopy(self._orientation)

    def _twosComplement(self, high, low):
        """ Compute twos complement
        """
        value = (high << 8) + low
        if value >= 0x8000:
            value = -((65535 - value) + 1)
        return value

    def _distance(self, x, y):
        """ Compute the distance between two point in 2D space
        """
        return math.sqrt((x * x) + (y * y))

    def _rotationX(self, x, y, z):
        """ Compute the rotation around the X axis
        """
        return math.degrees(math.atan2(y, self._distance(x, z)))

    def _rotationY(self, x, y, z):
        """ Compute the rotation around the Y axis
        """
        return -math.degrees(math.atan2(x, self._distance(y, z)))

    def refresh(self, tau=0.25, dt=0.01):
        """ Refresh datas

        Read the raw data from the sensor, scale it appropriately and store for later use.
        Also compute orientation using complementary filter.

        @param tau: time constant
        @type tau: float

        @param dt: delta t
        @type dt: float
        """
        self._rawGyroData = self._i2cBus.readBlock(self._address, GYRO_START_BLOCK_REG, 6)
        self._rawAccelData = self._i2cBus.readBlock(self._address, ACCEL_START_BLOCK_REG, 6)
        self._rawTempData = self._i2cBus.readBlock(self._address, TEMP_START_BLOCK_REG, 2)

        self._gyro['raw']['x'] = self._twosComplement(self._rawGyroData[GYRO_XOUT_H_INDEX], self._rawGyroData[GYRO_XOUT_L_INDEX])
        self._gyro['raw']['y'] = self._twosComplement(self._rawGyroData[GYRO_YOUT_H_INDEX], self._rawGyroData[GYRO_YOUT_L_INDEX])
        self._gyro['raw']['z'] = self._twosComplement(self._rawGyroData[GYRO_ZOUT_H_INDEX], self._rawGyroData[GYRO_ZOUT_L_INDEX])

        self._accel['raw']['x'] = self._twosComplement(self._rawAccelData[ACCEL_XOUT_H_INDEX], self._rawAccelData[ACCEL_XOUT_L_INDEX])
        self._accel['raw']['y'] = self._twosComplement(self._rawAccelData[ACCEL_YOUT_H_INDEX], self._rawAccelData[ACCEL_YOUT_L_INDEX])
        self._accel['raw']['z'] = self._twosComplement(self._rawAccelData[ACCEL_ZOUT_H_INDEX], self._rawAccelData[ACCEL_ZOUT_L_INDEX])

        self._temp['raw'] = self._twosComplement(self._rawTempData[TEMP_OUT_H_INDEX], self._rawTempData[TEMP_OUT_L_INDEX])

        self._gyro['scaled']['x'] = self._gyro['raw']['x'] / GYRO_CONFIGS[self._gyroConfig]['scale']
        self._gyro['scaled']['y'] = self._gyro['raw']['y'] / GYRO_CONFIGS[self._gyroConfig]['scale']
        self._gyro['scaled']['z'] = self._gyro['raw']['z'] / GYRO_CONFIGS[self._gyroConfig]['scale']

        self._temp['scaled'] = self._temp['raw'] / 340 + 36.53

        self._accel['scaled']['x'] = self._accel['raw']['x'] / ACCEL_CONFIGS[self._accelConfig]['scale']
        self._accel['scaled']['y'] = self._accel['raw']['y'] / ACCEL_CONFIGS[self._accelConfig]['scale']
        self._accel['scaled']['z'] = self._accel['raw']['z'] / ACCEL_CONFIGS[self._accelConfig]['scale']

        self._orientation['raw']['pitch'] = self._rotationX(self._accel['scaled']['x'], self._accel['scaled']['y'], self._accel['scaled']['z'])
        self._orientation['raw']['roll'] =  self._rotationY(self._accel['scaled']['x'], self._accel['scaled']['y'], self._accel['scaled']['z'])

        a = tau / (tau + dt)
        self._orientation['filtered']['pitch'] = a * (self._orientation['filtered']['pitch'] + self._gyro['scaled']['x'] * dt) + (1 - a) * self._orientation['raw']['pitch']
        self._orientation['filtered']['roll'] = a * (self._orientation['filtered']['roll'] + self._gyro['scaled']['y'] * dt) + (1 - a) * self._orientation['raw']['roll']
