# -*- coding: utf-8 -*-

""" Py4bot framework.

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

Copyright (c) 2012-2013 Limor Fried, Kevin Townsend and Mikey Sklar for Adafruit Industries.
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

 - Redistributions of source code must retain the above copyright notice,
   this list of conditions and the following disclaimer.
 - Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.
 - Neither the name of the nor the names of its contributors may be used to endorse or
   promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR
IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Module purpose
==============

I²C bus management.

Implements
==========

 - B{I2cBus}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@author: Limor Fried
@author: Kevin Townsend
@author: Mikey Sklar
@copyright: (C) 2014-2021 Frédéric Mantegazza
@copyright: (C) 2012-2013 Adafruit Industries
@license: ???
"""

import smbus

from py4bot.common.exception import Py4botHardwareError


class I2cBus(object):
    """
    """
    def __init__(self, busNum=None):
        """ Init I2cBus object
        """
        super(I2cBus, self).__init__()

        self._bus = self._createBus(busNum)

    @property
    def bus(self):
        return self._bus

    def _createBus(self, num):
        """ Default I2C bus implementation
        """
        return smbus.SMBus(num)

    def read8(self, address, reg):
        """ Read 8-bit value from the specified register/address
        """
        try:
            self._bus.write_byte(address, reg)
            return self._bus.read_byte(address)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def write8(self, address, reg, value):
        """ Write 8-bit value to the specified register/address
        """
        try:
            self._bus.write_byte_data(address, reg, value)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def read16(self, address, reg):
        """ Read 16-bit value from the specified register/address
        """
        try:
            self._bus.write_byte(address, reg)
            return self._bus.read_word_data(address, reg)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def write16(self, address, reg, value):
        """ Write 16-bit value to the specified register/address
        """
        try:
            self._bus.write_word_data(address, reg, value)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def read(self, address):
        """ Read 8-bit value from the bus
        """
        try:
            return self._bus.read_byte(address)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def write(self, address, value):
        """ Write 8-bit value on the bus
        """
        try:
            return self._bus.write_byte(address, value)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def readBlock(self, address, reg, length):
        """ Read a block of data from the specified register/address
        """
        try:
            return self._bus.read_i2c_block_data(address, reg, length)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")

    def writeBlock(self, address, reg, values):
        """ Write a block of data from the specified register/address
        """
        try:
            self._bus.write_i2c_block_data(address, reg, values)
        except IOError:
            raise Py4botHardwareError("Error accessing I²C bus")


class I2cFakeBus(I2cBus):
    """
    """
    def _createBus(self, num):
        return None

    def read8(self, address, reg):
        return 0x00

    def write8(self, address, reg, value):
        pass

    def read16(self, address, reg):
        return 0x0000

    def write16(self, address, reg, value):
        pass

    def write(self, address, value):
        pass

    def writeBlock(self, address, reg, values):
        pass
