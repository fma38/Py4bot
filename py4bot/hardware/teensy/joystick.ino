/* Py4bot framework.
 *
 *  - Py4bot (http://www.py4bot.org) is Copyright:
 *   - (C) 2014-2017 Frédéric Mantegazza
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * or see:
 *
 *  http://www.gnu.org/licenses/gpl.html
 */

/*
 * This sketch must be run on a Teensy++.
 *
 * It has been written for an old Saitek Cyborg 3D Digital, but can be adapted for other joysticks.
 *
 * You must select Joystick from the "Tools > USB Type" menu before compiling.
*/

#include <Bounce2.h>

//#include "T6963C.h"

//#define DEBUG 1

const uint8_t DEBOUNCE_DELAY = 10; // 10ms is appropriate for most mechanical pushbuttons
const uint8_t REFRESH_RATE = 100;  // Hz

const uint8_t AXIS[] = {
    0,   // X,  joystick right - mapped to ABS_X        (0x00)
    1,   // Y,  joystick right - mapped to ABS_Y        (0x01)
    2,   // RZ, joystick right - mapped to ABS_Z        (0x02)
//    3,   // X,  joystick left  - mapped to ABS_THROTTLE (0x06)
//    4,   // Y,  joystick left  - mapped to ABS_RUDDER   (0x07)
//    5    // RZ, joystick left  - mapped to ABS_RZ       (0x05)
};

const uint8_t BUTTONS[] = {

    // Joystick right
    10,   // trigger - mapped to BTN_BASE2                   (0x120)
    11,   // left    - mapped to BTN_TOP2                    (0x121)
    12,   // middle  - mapped to BTN_PINKIE                  (0x122)
    13,   // right   - mapped to BTN_BASE                    (0x123)
    14,   // hat N   - mapped to BTN_JOYSTICK/BTN_TRIGGER    (0x124)
    15,   // hat S   - mapped to BTN_THUMB                   (0x125)
    16,   // hat E   - mapped to BTN_THUMB2                  (0x126)
    17,   // hat W   - mapped to BTN_TOP                     (0x127)

    // Joystick left
    20,   // trigger - mapped to BTN_C                       (0x138)
    21,   // left    - mapped to BTN_DEAD                    (0x129)
    22,   // middle  - mapped to BTN_GAMEPAD/BTN_SOUTH/BTN_A (0x12a)
    23,   // right   - mapped to BTN_EAST/BTN_B              (0x12b)
    24,   // hat N   - mapped to BTN_BASE3                   (0x12f)
    25,   // hat S   - mapped to BTN_BASE4                   (0x130)
    26,   // hat E   - mapped to BTN_BASE5                   (0x131)
    27,   // hat W   - mapped to BTN_BASE6                   (0x132)

    // Misc
     0,   //         - mapped to BTN_NORTH/BTN_X             (0x133)
     1,   //         - mapped to BTN_WEST/BTN_Y              (0x134)
     4,   //         - mapped to BTN_Z                       (0x135)
     5,   //         - mapped to BTN_TL                      (0x136)
     6,   //         - mapped to BTN_TR                      (0x137)
     7,   //         - mapped to BTN_TL2                     (0x138)

//      ,   //         - mapped to BTN_TR2                     (0x139)
//      ,   //         - mapped to BTN_SELECT                  (0x13a)
//      ,   //         - mapped to BTN_START                   (0x13b)
//      ,   //         - mapped to BTN_MODE                    (0x13c)
//      ,   //         - mapped to BTN_THUMBL                  (0x13d)
//      ,   //         - mapped to BTN_THUMBR                  (0x13e)
//      ,   //         - mapped to BTN_DIGI/BTN_TOOL_PEN       (0x140)
//      ,   //         - mapped to BTN_TOOL_RUBBER             (0x141)
//      ,   //         - mapped to BTN_TOOL_BRUSH              (0x142)
//          //         - mapped to BTN_TOOL_PENCIL             (0x143)
};

const uint8_t SAMPLES = 10;
uint8_t rawAxis[6][SAMPLES];
uint16_t axis[6];
uint8_t prevAxis[6];
uint8_t buttons[32], prevButtons[32];
uint8_t index = 0;

// Define Bounce objects
// The Bounce object automatically deals with contact chatter or "bounce",
// and it makes detecting changes very simple.
Bounce *bouncer;

//T6963C *lcd;


void setup()
{
#ifdef DEBUG

    // Init debug
    Serial.begin(9600);
#else

    // Init Joystick
    Joystick.useManualSend(true);
#endif

    // Init BT com.
    Serial1.begin(115200);

    // Init axis readings
    for (uint8_t i=0; i<sizeof(AXIS); i++) {
        axis[i] = 0;
        prevAxis[i] = 0;
        for (uint8_t j=0; j<SAMPLES; j++) {
            rawAxis[i][j] = 0;
        }
    }

    bouncer = (Bounce *)malloc(sizeof(BUTTONS) * sizeof(Bounce));
    for (uint8_t i=0; i<sizeof(BUTTONS); i++) {

        // Init digital inputs (buttons)
        pinMode(BUTTONS[i], INPUT_PULLUP);
        buttons[i] = 0;
        prevButtons[i] = 0;

        // Create bounce objects
        bouncer[i] = Bounce();
        bouncer[i].attach(BUTTONS[i]);
        bouncer[i].interval(DEBOUNCE_DELAY);
    }

    // Create LCD object
//    lcd = new T6963C(240, 128, 6);
}


void loop()
{

    // Read up to 6 analog inputs and use them as axis
    for (uint8_t i=0; i<sizeof(AXIS); i++) {
        axis[i] = axis[i] - rawAxis[i][index];
        rawAxis[i][index] = analogRead(AXIS[i]) >> 2;  // remove noisy bits
        axis[i] = axis[i] + rawAxis[i][index];

#if not DEBUG
        switch (i) {
            case 0:
                Joystick.X(axis[i] / SAMPLES);
                break;

            case 1:
                Joystick.Y(axis[i] / SAMPLES);
                break;

            case 2:
                Joystick.Z(axis[i] / SAMPLES);
                break;

            case 3:
                Joystick.sliderLeft(axis[i] / SAMPLES);
                break;

            case 4:
                Joystick.sliderRight(axis[i] / SAMPLES);
                break;

            case 5:
                Joystick.Zrotate(axis[i] / SAMPLES);
                break;

            default:
                break;
        }
#endif

    }
    index++;
    if (index >= SAMPLES) {
        index = 0;
    }

    // Read up to 32 digital pins and use them as buttons
    uint8_t index = 1;
    for (uint8_t i=0; i<sizeof(BUTTONS); i++) {

        // Update buttons
        bouncer[i].update();
        if (bouncer[i].read()) {
            buttons[i] = 0;
        }
        else {
            buttons[i] = 1;
        }

#if not DEBUG
        Joystick.button(index++, buttons[i]);
#endif
    }

#if not DEBUG
    // Complete USB joystick buttons (up to 32)
    while (index <= 32) {
        Joystick.button(index++, 0);
    }
#endif

    // check to see if any axis/button changed since last time
    boolean anyChange = false;
    for (uint8_t i=0; i<sizeof(AXIS); i++) {
        if (axis[i] != prevAxis[i]) {
            anyChange = true;
        }
        prevAxis[i] = axis[i];
    }
    for (uint8_t i=0; i<sizeof(BUTTONS); i++) {
        if (buttons[i] != prevButtons[i]) {
            anyChange = true;
        }
        prevButtons[i] = buttons[i];
    }

    // If any axis/button changed, update USB/BT
    if (anyChange) {

#ifdef DEBUG
      // Send datas to debug console
        Serial.print("Axis: ");
        for (uint8_t i=0; i<sizeof(AXIS); i++) {
            Serial.print(axis[i], HEX);
            Serial.print(" ");
        }
        Serial.println();
        Serial.print("Buttons: ");
        for (uint8_t i=0; i<sizeof(BUTTONS); i++) {
            Serial.print(buttons[i], DEC);
            Serial.print(" ");
        }
        Serial.println();
#else

        // Send datas to USB joystick channel
        Joystick.send_now();
#endif

        // Send datas to BT
        for (uint8_t i=0; i<sizeof(AXIS); i++) {
            Serial1.print(axis[i], HEX);
            Serial1.print(" ");
        }
        for (uint8_t i=0; i<sizeof(BUTTONS); i++) {
            Serial1.print(buttons[i], DEC);
            Serial1.print(" ");
        }
        Serial1.println();
    }

    // Wait to match refresh rate (not very accurate)
//    delay(1000 / REFRESH_RATE);
}
