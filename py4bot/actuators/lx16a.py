# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

LX16A servo management

Implements
==========

 - B{LX16A}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2017-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from py4bot.actuators.actuator import Actuator

DEFAULT_OFFSET = 0
DEFAULT_RATIO = 240 / 1000
DEFAULT_DEVIATION = 0

DEFAULT_ENABLE = True

NEUTRAL = 500


class LX16A(Actuator):
    """ Lewansoul LX16A servos

    @ivar _offset: angle joint offset, in °
    @type _offset: float

    @ivar _deviation: neutral adjustement
    @type _deviation: int

    @ivar _ratio: angle vs points ratio, in °/pts
    @type _ratio: float
    """
    def __init__(self, joint, num, offset=DEFAULT_OFFSET, deviation=DEFAULT_DEVIATION, ratio=DEFAULT_RATIO):
        """
        """
        super(LX16A, self).__init__(joint, num)

        self._offset = offset
        self._deviation = deviation
        self._ratio = ratio

        self._enable = DEFAULT_ENABLE

        self._deviation = None
        self._voltage = None
        self._temperature = None
        self._position = None

    def __str__(self):
        return "LX16A(num=%d)" % self._num

    @property
    def enable(self):
        return self._enable

    @enable.setter
    def enable(self, value):
        self._enable = value

    @property
    def deviation(self):
        return self._deviation

    @deviation.setter
    def deviation(self, value):
        self._deviation = value

    @property
    def voltage(self):
        return self._voltage

    @voltage.setter
    def voltage(self, value):
        self._voltage = value

    @property
    def temperature(self):
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        self._temperature = value

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = value

    @property
    def point(self):
        return int((self._joint.angle - self._offset) / self._ratio + NEUTRAL)

    @point.setter
    def point(self, point):
        self._joint.angle = self._offset + (point - NEUTRAL) * self._ratio
