# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management as pool

Implements
==========

 - B{ActuatorPool}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.common.exception import Py4botTypeError, Py4botValueError
from py4bot.common.orderedSet import OrderedSet
from py4bot.services.logger import Logger
from py4bot.actuators.actuator import Actuator


class ActuatorPool(object):
    """ Base Actuator pool object

    Manage a pool of actuators.
    Used to group actuators and move them together
    """
    def __init__(self, driver):
        """ Init ActuatorPool object
        """
        super(ActuatorPool, self).__init__()

        self._driver = driver

        self._actuators = OrderedSet()

    @property
    def actuators(self):
        return self._actuators

    def add(self, actuator):
        """ Add an actuator to the pool
        """
        if not isinstance(actuator, Actuator):
            raise Py4botTypeError("Can only add Actuator type object")

        self._actuators.add(actuator)

    def move(self, staggeringDelay=0.):
        """

        @param staggeringDelay: delay between each servo move, in s
                                usefull for first move to avoid all servos moving at the same time
        @type staggeringDelay: float
        """
        Logger().trace("ActuatorPool.move()")

        self._driver.move(self._actuators, staggeringDelay)

    def moveSync(self, duration):
        """
        """
        Logger().trace("ActuatorPool.moveSync()")
        #Logger().debug("ActuatorPool.moveSync(): duration=%.1f" % duration)

        # Wait end of previous synchronized move
        self._driver.waitEndOfSyncMove()

        self._driver.moveSync(self._actuators, duration)
