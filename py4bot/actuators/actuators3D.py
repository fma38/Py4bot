# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Leg joints

Implements
==========

 - B{Joint}
 - B{Coxa}
 - B{Femur}
 - B{Tibia}
  -B{Tars}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

import visual

from py4bot.actuators.actuator import Actuator

LEGS_WIDTH = 15
LEGS_HEIGHT = 5


class Actuator3D(Actuator, visual.frame):
    """ 3D Actuator object
    """
    def __init__(self, joint, num, length, height, width, axis):
        """ Init Actuator3D object
        """
        super(Actuator3D, self).__init__(joint, num)
        #Actuator.__init__(self, joint, num)
        #visual.frame.__init__(self, make_trail=True)

        self._length = length
        self._height = height
        self._width = width
        self._axis = axis

        self._angle = 0.

    def __str__(self):
        return "Actuator3D(num=%d)" % self._num

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, angle):
        angle_ = angle - self._angle
        self.rotate(angle=math.radians(angle_), axis=self._axis)
        self._angle = angle


class Coxa3D(Actuator3D):

    """
    """
    def __init__(self, joint, num, length, height=LEGS_HEIGHT, width=LEGS_WIDTH):
        """
        """
        super(Coxa3D, self).__init__(joint, num, length, height, width, (0, 1, 0))

        visual.cylinder(frame=self, pos=(0, -(height+5)/2, 0), radius=(height+1)/2, axis=(0, 1, 0), length=height+5, color=visual.color.cyan)
        visual.box(frame=self, pos=(length/2, 0, 0), length=length, height=height, width=width , color=visual.color.red)


class Femur3D(Actuator3D):
    """
    """
    def __init__(self, joint, num, length, height=LEGS_HEIGHT, width=LEGS_WIDTH):
        """
        """
        super(Femur3D, self).__init__(joint, num, length, height, width, (0, 0, 1))

        visual.cylinder(frame=self, pos=(0, 0, -(width+5)/2), radius=(height+1)/2, axis=(0, 0, 1), length=width+5, color=visual.color.cyan)
        visual.box(frame=self, pos=(length/2, 0, 0), length=length, height=height, width=width , color=visual.color.green)
        self.rotate(angle=math.radians(180), axis=self._axis)


class Tibia3D(Actuator3D):
    """
    """
    def __init__(self, joint, num, length, height=LEGS_HEIGHT, width=LEGS_WIDTH):
        """
        """
        super(Tibia3D, self).__init__(joint, num, length, height, width, (0, 0, 1))

        visual.cylinder(frame=self, pos=(0, 0, -(width+5)/2), radius=(height+1)/2, axis=(0, 0, 1), length=width+5, color=visual.color.cyan)
        visual.box(frame=self, pos=(length/2, 0, 0), length=length, height=height, width=width , color=visual.color.blue)
        self.rotate(angle=math.radians(180), axis=self._axis)


class Tars3D(Actuator3D):
    """
    """
    def __init__(self, joint, num, length, height=LEGS_HEIGHT, width=LEGS_WIDTH):
        """
        """
        super(Tars3D, self).__init__(joint, num, length, height, width, (0, 0, 1))

        visual.cylinder(frame=self, pos=(0, 0, -(width+5)/2), radius=(height+1)/2, axis=(0, 0, 1), length=width+5, color=visual.color.cyan)
        visual.box(frame=self, pos=((length-5)/2, 0, 0), length=length-5/2, height=height, width=width , color=visual.color.yellow)
        visual.sphere(frame=self, pos=(length-5/2, 0, 0), radius=5)
        self.rotate(angle=math.radians(180), axis=self._axis)
