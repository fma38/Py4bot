# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

RC servo management

Implements
==========

 - B{Servo}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from py4bot.actuators.actuator import Actuator

DEFAULT_OFFSET = 0
DEFAULT_NEUTRAL = 1500
DEFAULT_RATIO = 90 / 1000


class Servo(Actuator):
    """ Standard RC servos

    @ivar _offset: angle joint offset, in °
    @type _offset: float

    @ivar _neutral: pulse width for neutral
    @type _neutral: int

    @ivar _ratio: angle vs pulse width ratio, in deg/µs
    @type _ratio: float
    """
    def __init__(self, joint, num, offset=DEFAULT_OFFSET, neutral=DEFAULT_NEUTRAL, ratio=DEFAULT_RATIO):
        """
        """
        super(Servo, self).__init__(joint, num)

        self._offset = offset
        self._neutral = neutral
        self._ratio = ratio

    def __str__(self):
        return "Servo(num=%d)" % self._num

    @property
    def pulse(self):
        return int((self._joint.angle - self._offset) / self._ratio + self._neutral)

    @pulse.setter
    def pulse(self, pulse):
        self._joint.angle = self._offset + (pulse - self._neutral) * self._ratio
