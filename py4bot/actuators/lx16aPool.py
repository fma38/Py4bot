# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Lewansoul bus servos management as pool

Implements
==========

 - B{LX16APool}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2017-2021 Frédéric Mantegazza
@license: GPL
"""

from py4bot.services.logger import Logger
from py4bot.common.exception import Py4botTypeError
from py4bot.actuators.actuatorPool import ActuatorPool
from py4bot.actuators.lx16a import LX16A


class LX16APool(ActuatorPool):
    """ Manage a pool of LX16A servos
    """
    @property
    def servos(self):
        return self._actuators

    def add(self, servo):
        if not isinstance(servo, LX16A):
            raise Py4botTypeError("Can only add LX16A type actuator")

        super(LX16APool, self).add(servo)

    def writeEnable(self):
        """
        """
        Logger().trace("ActuatorPool.writeEnable()")

        self._driver.writeEnable(self._actuators)

    def writeDeviation(self):
        """
        """
        Logger().trace("ActuatorPool.writeDeviation()")

        self._driver.writeDeviation(self._actuators)

    def saveDeviation(self):
        """
        """
        Logger().trace("ActuatorPool.saveDeviation()")

        self._driver.saveDeviation(self._actuators)
