# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Matrix computations

Implements
==========

 - B{Matrix}

Documentation
=============

Usage
=====

# Thanks to Ernesto P. Adorio for use of _cholesky and _choleskyInverse functions
@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import math


class Matrix:
    """ Basic matrix implementation, without any dependency
    """
    def __init__(self, value):
        """
        """
        self._value = value
        self._xDimension = len(value)
        self._yDimension = len(value[0])
        if value == [[]]:
            self._xDimension = 0

    def __repr__(self):
        return "Matrix(%s)" % repr(self._value)

    def __str__(self):
        s  = ""
        for i in range(self._xDimension):
            s += str(self._value[i])
            s += '\n'

        return s

    def __add__(self, other):
        if self._xDimension != other._xDimension or self._yDimension != other._yDimension:
            raise ValueError("Matrices must be of equal dimensions to add")

        result = Matrix([[]])
        result.zero(self._xDimension, self._yDimension)
        for i in range(self._xDimension):
            for j in range(self._yDimension):
                result._value[i][j] = self._value[i][j] + other._value[i][j]

            return result

    def __sub__(self, other):
        if self._xDimension != other._xDimension or self._yDimension != other._yDimension:
            raise ValueError("Matrices must be of equal dimensions to subtract")

        result = Matrix([[]])
        result.zero(self._xDimension, self._yDimension)
        for i in range(self._xDimension):
            for j in range(self._yDimension):
                result._value[i][j] = self._value[i][j] - other._value[i][j]

            return result

    def __mul__(self, other):
        # Not supported by micropython
        # call __mul__() directly
        if self._yDimension != other._xDimension:
            raise ValueError("Matrices must be m*n and n*p to multiply")

        result = Matrix([[]])
        result.zero(self._xDimension, other._yDimension)
        for i in range(self._xDimension):
            for j in range(other._yDimension):
                for k in range(self._yDimension):
                    result._value[i][j] += self._value[i][k] * other._value[k][j]

            return result

    def _cholesky(self, ztol=1.0e-5):
        """
        """
        result = Matrix([[]])
        result.zero(self._xDimension, self._xDimension)

        for i in range(self._xDimension):
            S = sum([(result._value[k][i]) ** 2 for k in range(i)])
            d = self._value[i][i] - S
            if abs(d) < ztol:
                result._value[i][i] = 0.
            else:
                if d < 0.:
                    raise ValueError("Matrix not positive-definite")
                result._value[i][i] = math.sqrt(d)
            for j in range(i+1, self._xDimension):
                S = sum([result._value[k][i] * result._value[k][j] for k in range(self._xDimension)])
                if abs(S) < ztol:
                    S = 0.
                result._value[i][j] = (self._value[i][j] - S) / result._value[i][i]

        return result

    def _cholesky2(self, ztol=1.0e-5):
        """
        """
        result = Matrix([[]])
        result.zero(self._xDimension, self._xDimension)

        for i in range(self._xDimension):
            S = sum([(result._value[k][i]) ** 2 for k in range(i)])
            d = self._value[i][i] - S
            if abs(d) < ztol:
                result._value[i][i] = 0.
            else:
                if d < 0.:
                    raise ValueError("Matrix not positive-definite")
                result._value[i][i] = math.sqrt(d)
            for j in range(i+1, self._xDimension):
                S = sum([result._value[k][i] * result._value[k][j] for k in range(self._xDimension)])
                if abs(S) < ztol:
                    S = 0.
                result._value[i][j] = (self._value[i][j] - S) / result._value[i][i]

        return result

    def _choleskyInverse(self):
        """
        """
        result = Matrix([[]])
        result.zero(self._xDimension, self._xDimension)

        #for j in reversed(range(self._xDimension)):  # reversed() not available on micropython
        for j in range(self._xDimension-1, -1, -1):
            tjj = self._value[j][j]
            S = sum([self._value[j][k]*result._value[j][k] for k in range(j+1, self._xDimension)])
            result._value[j][j] = 1.0/tjj**2 - S/tjj
            #for i in reversed(range(j)):  # reversed() not available on micropython
            for i in range(j-1, -1, -1):
                result._value[j][i] = result._value[i][j] = -sum([self._value[i][k]*result._value[k][j] for k in range(i+1, self._xDimension)]) / self._value[i][i]

        return result

    @property
    def T(self):
        """ Transposed matrix
        """
        result = Matrix([[]])
        result.zero(self._yDimension, self._xDimension)
        for i in range(self._xDimension):
            for j in range(self._yDimension):
                result._value[j][i] = self._value[i][j]
        return result

    @property
    def I(self):
        """ Inverse matrix
        """
        aux = self._cholesky2()
        result = aux._choleskyInverse()
        return result

    def zero(self, xDimension, yDimension):
        """ Zero matrix
        """
        if xDimension < 1 or yDimension < 1:
            raise ValueError("Invalid matrix dimension")
        else:
            self._xDimension = xDimension
            self._yDimension = yDimension
            self._value = [[0 for row in range(yDimension)] for col in range(xDimension)]

    def identity(self, dimension):
        """ Identity matrix
        """
        if dimension < 1:
            raise ValueError("Invalid matrix dimension")
        else:
            self._xDimension = dimension
            self._yDimension = dimension
            self._value = [[0 for row in range(dimension)] for col in range(dimension)]
            for i in range(dimension):
                self._value[i][i] = 1
