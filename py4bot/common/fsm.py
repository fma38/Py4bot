# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Finite States Machine implementation

Implements
==========

 - B{FSM}

Documentation
=============

This module implements a Finite State Machine (FSM).

Usage
=====

TODO
====

 - add decorators support?

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from py4bot.common.exception import Py4botFsmError
from py4bot.services.logger import Logger


class FSM(object):
    """ Finite State Machine (FSM) object

    @ivar _initialState: initial state when launching the fsm
    @type _initialState: str

    @ivar _state: current state of the fsm
    @type _state: str

    @ivar _transitions: map (event, currentState) --> (nextState, action)
    @type _transitions: dict

    @ivar _pendingTransition: async. transition
    @type _pendingTransition: tuple
    """
    def __init__(self, initialState='idle'):
        """ Init the FSM

        @todo:
        """
        super(FSM, self).__init__()

        self._initialState = initialState

        self._state = initialState
        self._transitions = {}
        self._pendingTransition = None

        self._createTransitions()

    @property
    def state(self):
        return self._state

    def _createTransitions(self):
        """ Create the transitions

        Must be overloaded in sub-classes
        """
        raise NotImplementedError

    def _addTransition(self, event, src, dst, sync=True, onBefore=None, onLeave=None, onEnter=None, onReEnter=None, onAfter=None):
        """ Add a transition

        Add a transition from state 'src' to 'dst' for event name 'event'.

        When the transition matches, the execution does:
         - onBefore(event)
         - onLeave(event)
         - onEnter(event)    # when dst != src
         - onReEnter(event)  # when dst == src
         - onAfter(event)

        If onBefore() returns 'cancel', the event is canceled.

        @param event: name of the transition event
        @type event: src

        @param src: source state this transition apply to
        @type src: str or list/tuple of str

        @param dst: destination state this transition leads to
        @type dst: str

        @param sync: if False, holds the current state until transition() method is called
        @type sync: bool

        @param onBefore: called before triggering the event
        @type onBefore: callable

        @param onLeave: called before leaving the current state
        @type onLeave: callable

        @param onEnter: called before reaching the destination state (when dst != src)
        @type onEnter: callable

        @param onReEnter: called before re-entering the destination state (when dst == src)
        @type onReEnter: callable

        @param onAfter: called after triggering the event
        @type onAfter: callable
        """
        if not isinstance(src, (list, tuple)):
            src = (src,)
        for s in src:
            try:
                self._transitions[event][s] = (dst, sync, onBefore, onLeave, onEnter, onReEnter, onAfter)
            except KeyError:
                self._transitions[event] = {s: (dst, sync, onBefore, onLeave, onEnter, onReEnter, onAfter)}

    def reset(self):
        """ Reset FSM
        """
        self._state = self._initialState

    def transitionPending(self):
        """
        """
        return self._pendingTransition is not None

    def trigger(self, name, **kwargs):
        """ Trigger the given event

        @param name: name of the event to process
        @type name: str
        """
        Logger().debug("FSM.trigger(): event=%s" % name)

        if self._pendingTransition:
            raise Py4botFsmError("An async. transition is already registered")

        else:
            src = self._state
            try:

                # Get all transitions associated with this event
                eventTransitions = self._transitions[name]
                try:

                    # Get the transition for the specified destination state...
                    dst, sync, onBefore, onLeave, onEnter, onReEnter, onAfter = self._transitions[name][src]
                except KeyError:

                    # ...or check if a transition associated with a wilcard-source is available
                    try:
                        dst, sync, onBefore, onLeave, onEnter, onReEnter, onAfter = self._transitions[name]['*']
                    except KeyError:
                        Logger().warning("Ignoring undefined event ('%s') for current state ('%s')" % (name, src))
                        return

            except KeyError:
                Logger().warning("Ignoring unknow event ('%s')" % name)
                return

            if dst == '=':
                dst = src

            # Build event dict
            event = {'name': name, 'src': self._state, 'dst': dst}
            event.update(kwargs)

            if onBefore:
                result = onBefore(event)
                if result == 'cancel':
                    raise Py4botFsmError("Event canceled")

            if dst != src:

                # Create the transition
                self._pendingTransition = (event, onLeave, onEnter, onAfter)

                # Hook for async. transition
                if sync:
                    self.transition()

            else:
                Logger().debug("FSM.trigger(): stay on '%s' state" % src)
                if onLeave:
                    onLeave(event)
                if onReEnter:
                    onReEnter(event)
                if onAfter:
                    onAfter(event)

    def transition(self):
        """
        """
        Logger().trace("FSM.transition()")
        try:
            event, onLeave, onEnter, onAfter = self._pendingTransition
            Logger().debug("FSM.transition(): switching to '%s' state" % event['dst'])

            # Reset registered transition
            self._pendingTransition = None

            # Switch to destination state
            self._state = event['dst']

            if onLeave:
                onLeave(event)
            if onEnter:
                onEnter(event)
            if onAfter:
                onAfter(event)

        except TypeError:
            Logger().warning("No registered transition available")


if __name__ == "__main__":
    import unittest
    import time

    from py4bot.common.exception import Py4botError


    Logger().setLevel('trace')


    class TestFSM(FSM):
        """
        """
        def _createTransitions(self):
            self._addTransition(event='a', src='idle', dst='1')
            self._addTransition(event='b', src='1', dst='2')

            self._addTransition(event='c', src='idle', dst='1', onAfter=self._onAfter_Ok)
            self._addTransition(event='d', src='idle', dst='1', onAfter=self._onAfter_Error)

            self._addTransition(event='e', src='*', dst='1')

            self._addTransition(event='f', src='idle', dst='1', onBefore=lambda event: 'cancel')

            self._addTransition(event='g', src='idle', dst='1', sync=False)

            self._addTransition(event='h', src='idle', dst='=')

            self._addTransition(event='i', src=('idle', '1'), dst='2')

        def _onAfter_Ok(self, event):
            print("TestFSM._onAfter_Ok(): event=", event)

        def _onAfter_Error(self, event):
            raise Py4botError("An exception occured in _onAfter()")


    class FSMTestCase(unittest.TestCase):

        def setUp(self):
            self.fsm = TestFSM()

        def tearDown(self):
            pass

        def test_constructor(self):
            fsm = TestFSM()
            self.assertEqual(fsm.state, 'idle')
            fsm = TestFSM('standby')
            self.assertEqual(fsm.state, 'standby')

        def test_trigger_1(self):
            self.fsm.trigger('a')
            self.assertEqual(self.fsm.state, '1')
            self.fsm.trigger('b')
            self.assertEqual(self.fsm.state, '2')

        def test_trigger_2(self):
            self.fsm.trigger('b')
            self.assertEqual(self.fsm.state, 'idle')

        def test_trigger_3(self):
            self.fsm.trigger('z')
            self.assertEqual(self.fsm.state, 'idle')

        def test_trigger_4(self):
            self.fsm.trigger('c')
            self.assertEqual(self.fsm.state, '1')

        def test_trigger_5(self):
            with self.assertRaises(Py4botError):
                self.fsm.trigger('d')
            self.assertEqual(self.fsm.state, '1')

        def test_trigger_6(self):
            self.fsm.trigger('e')
            self.assertEqual(self.fsm.state, '1')

        def test_trigger_7(self):
            with self.assertRaises(Py4botError):
                self.fsm.trigger('f')
            self.assertEqual(self.fsm.state, 'idle')

        def test_trigger_8(self):
            self.fsm.trigger('g')
            self.assertEqual(self.fsm.state, 'idle')
            self.fsm.transition()
            self.assertEqual(self.fsm.state, '1')

        def test_trigger_9(self):
            self.fsm.trigger('h')
            self.assertEqual(self.fsm.state, 'idle')

        def test_trigger_10(self):
            self.fsm.trigger('i')
            self.assertEqual(self.fsm.state, '2')

        def test_trigger_10(self):
            self.fsm.trigger('a')
            self.fsm.trigger('i')
            self.assertEqual(self.fsm.state, '2')


    unittest.main()
