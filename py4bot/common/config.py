# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Global configuration

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import numpy
try:
    numpy.set_printoptions(formatter={'float': lambda x: "%+ 6.1f" % x})
except TypeError:
    numpy.set_printoptions(precision=1)

# Name and version
APP_NAME = "Py4bot"

# Logger
LOGGER_LEVEL = "info"
LOGGER_STREAM_FORMAT = "%(threadName)s::%(message)s"
#LOGGER_FILE_FORMAT = "%(asctime)s::%(threadName)s::%(levelname)s::%(message)s"
#LOGGER_DIR = "/tmp"
#LOGGER_MAX_BYTES = 256 * 1024
#LOGGER_BACKUP_COUNT = 1  # set to 0 to disable logging on file

# Delay
WALK_PAUSE_TO_STOP_DELAY = 10.  # in s. use None to disable

I2C_BUS = 1  # i²c bus number

# PCA9685 driver
PCA9685_DRIVER_PWM_FREQUENCY = 50  # PWM frequency, in Hz
PCA9685_DRIVER_CURVE = "linear"  # response curve, in ("linear", "smooth", "damped", "accel")

# Maestro driver
MAESTRO_DRIVER_CURVE = "linear"  # response curve, in ("linear", "smooth", "damped", "accel")

# SERVO_NODE driver
#SERVO_NODE_DRIVER_CURVE = "linear"  # response curve, in ("linear", "smooth", "damped", "accel")

# Misc
RESPONSE_PERIOD = 0.02  # response curve (min) period, in s
