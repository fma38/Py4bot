# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Rotation matrix functions

Implements
==========

 - B{Rx}
 - B{Ry}
 - B{Rz}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

import numpy


def Rx(pitch):
    """ Rotation matrix arround x (pitch)
    """
    pitch = math.radians(pitch)
    return numpy.matrix([[1,               0,                0],
                         [0, math.cos(pitch), -math.sin(pitch)],
                         [0, math.sin(pitch),  math.cos(pitch)]])

def Ry(roll):
    """ Rotation matrix arround y (roll)
    """
    roll = math.radians(roll)
    return numpy.matrix([[ math.cos(roll), 0, math.sin(roll)],
                         [              0, 1,             0],
                         [-math.sin(roll), 0, math.cos(roll)]])

def Rz(yaw):
    """ Rotation matrix arround z (yaw)
    """
    yaw = math.radians(yaw)
    return numpy.matrix([[math.cos(yaw), -math.sin(yaw), 0],
                         [math.sin(yaw),  math.cos(yaw), 0],
                         [            0,              0, 1]])

def matrixToAxisAngle(R):
    """ Rotation matrix to axis/angle
    """
    d = 1 / math.sqrt(math.pow(R[2, 1] - R[1, 2], 2) + math.pow(R[0, 2] - R[2, 0], 2) + math.pow(R[1, 0] - R[0, 1], 2))
    x = (R[2, 1] - R[1, 2]) * d
    y = (R[0, 2] - R[2, 0]) * d
    z = (R[1, 0] - R[0, 1]) * d
    angle = math.acos((R[0, 0] + R[1, 1] + R[2, 2] - 1) / 2)

    return numpy.matrix([[x], [y], [z]]), angle


if __name__ == "__main__":
    import time
    import unittest


    class RotationTestCase(unittest.TestCase):

        def setUp(self):
            self.R = Rz(math.radians(15)) * Ry(math.radians(15)) * Rx(math.radians(15))

        def tearDown(self):
            pass

        def test_timing1(self):
            startTime = time.time()
            axis = None
            angle = 0.
            for i in range(100000):
                axis, angle = matrixToAxisAngle(self.R)
            print(axis, angle)
            print("matrixToAxisAngle: %.6f" % ((time.time() - startTime) / 100000))


    unittest.main()
