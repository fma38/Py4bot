# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{ResponseFactory}
 - B{Response}
 - B{LinearResponse}
 - B{SCurveResponse}
 - B{DampedResponse}

Documentation
=============

See U{http://sol.gfxile.net/interpolation}. Thanks to Yannick for the link!

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time

from py4bot.common import config
from py4bot.common.exception import Py4botHardwareError
#from py4bot.common.singleton import Singleton


class ResponseFactory_(object):
    """ Factory to build a Response
    """
    #__metaclass__ = Singleton

    def __init__(self):
        """ Init factory
        """
        super(ResponseFactory_, self).__init__()

    def create(self, response, start, end, duration):
        """ Create a Response object
        """
        if response == "linear":
            return LinearResponse(start, end, duration)
        elif response == "smooth":
            return SmoothResponse(start, end, duration)
        elif response == "damped":
            return DampedResponse(start, end, duration)
        elif response == "accel":
            return AccelResponse(start, end, duration)
        else:
            raise Py4botValueError("Unknown response curve (%s)" % response)


responseFactory_ = ResponseFactory_()
def ResponseFactory():
    return ResponseFactory


class Response(object):
    """ Base class of Response objects
    """
    def __init__(self, start, end, duration=1., period=config.RESPONSE_PERIOD):
        """ Init the object.

        @param start: start of range
        @type start: float

        @param end: end of range
        @type end: float

        @param duration: total duration of the range, in s
        @type duration float

        @param period: minimum period between yields, in s
        @type duration: float
        """
        super(Response, self).__init__()

        self._start = start
        self._end = end
        self._duration = duration
        self._period = period

        self._sign = cmp(self._end, self._start)
        self._startTime = None
        self._nextTime = None

    def __iter__(self):
        """ Define Response as an iterator.
        """
        self._startTime = self._nextTime = time.time()
        while True:
            yield self.next()

    def _func(self, x):
        """
        """
        raise NotImplementedError

    def next(self):
        """ Return the next value.

        The loop is to ensure we don't yield values more often than period.
        """
        while True:

            # Compute current time
            t = time.time() - self._startTime

            # Test end of iteration condition
            if t > self._duration + self._period / 2:
                raise StopIteration

            # Adjust final step (opt)
            if t > self._duration:
                t = self._duration

            # Test next iteration condition
            if self._startTime + t >= self._nextTime:
                x = t / self._duration
                y = self._func(x)
                value = self._end * y + self._start * (1 - y)

                # Update next yield time
                self._nextTime += self._period

                return value

            # Wait a little bit
            time.sleep(0.01)


class LinearResponse(Response):
    """ Linear Response object
    """
    def _func(self, x):
        return x


class SmoothResponse(Response):
    """ Smooth Response object
    """
    def _func(self, x):
        #return x * x * (3 - 2 * x)
        return x * x * x * (x * (x * 6 -15) + 10)

class DampedResponse(Response):
    """ Damped Response object
    """
    def _func(self, x):
        return 1 - (1 - x) * (1 - x) * (1 - x)


class AccelResponse(Response):
    """ Accelerated Response object
    """
    def _func(self, x):
        return x * x


if __name__ == '__main__':
    import unittest
    import itertools

    class TestCase(unittest.TestCase):
        """
        """
        def setUp(self):
            self._linear = LinearResponse(1000, 2000, 1., 0.02)
            self._smooth = SmoothResponse(1000, 2000, 1., 0.02)
            self._damped = DampedResponse(1000, 2000, 1., 0.02)
            self._accel = AccelResponse(1000, 2000, 1., 0.02)

        def tearDown(self):
            pass

        def test_iter(self):
            """
            gnuplot> plot "data" using 1:2 with lines, "" using 1:3 with lines, "" using 1:4 with lines, "" using 1:5 with line
            """
            startTime = time.time()
            for values in itertools.izip(self._linear, self._smooth, self._damped, self._accel):
                print("%6.3f" % (time.time() - startTime), "%8.3f %8.3f %8.3f %8.3f" % (values))

    unittest.main()
