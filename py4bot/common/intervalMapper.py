# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Interval mapper

Implements
==========

 - B{IntervalMapper}

Documentation
=============

The goal of the IntervalMapper object is to convert an input interval to an output one.
It is mainly usefull for joysticks, to map their values to [-1:+1], and use as inputs.

It can insert one or more deadzone, at neutral position, to avoid false moves. See example below.

Usage
=====

   to
    ^
    |
   +1.                          ________
    |                          /
    |                         /
    |                        /
    0.              ________/
    |              /
    |             /
    |            /
   -1.  ________/
    |
    +------- 0x00--0x70--0x90--0xff---------> from

>>> mapper = IntervalMapper((0x00, 0x70, 0x90, 0xff), (-1., 0., 0., +1.))
>>> mapper(0x00)
-1.
>>> mapper(0x38)
-0.5
>>> mapper(0x70)
0
>>> mapper(0x102)
1.

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from py4bot.common.utils import pairwise


class IntervalMapper(object):
    """ Interval mapper object
    """
    def __init__(self, fromInterval, toInterval=(-1., +1)):
        """
        """
        if len(fromInterval) != len(toInterval):
            raise Py4botValueError("Intervals lengths mismatch")

        self._from = fromInterval
        self._to = toInterval

    def __str__(self):
        return "IntervalMapper(%s, %s)" % (self._from, self._to)

    def __call__(self, fromValue):
        sign = (self._from[1] - self._from[0]) / abs(self._from[1] - self._from[0])
        for (from0, to0), (from1, to1) in pairwise(zip(self._from, self._to)):
            if sign*from0 <= sign*fromValue <= sign*from1:
                value = to0 + (sign*fromValue - sign*from0) * (to1 - to0) / abs(from1 - from0)
                break

        else :
            if sign*fromValue < sign*self._from[0]:
                value = self._to[0]

            elif sign*fromValue > sign*self._from[-1]:
                value = self._to[-1]

        return value


if __name__ == '__main__':
    import unittest

    class TestCase(unittest.TestCase):
        """
        """
        def setUp(self):
            self.mapper1 = IntervalMapper((0x2a+0x10, 0x82-0x10, 0x88+0x10, 0xe2-0x10), (-1., 0., 0., 1.))
            self.mapper2 = IntervalMapper((0x80, -0x7f), (-1., 1.))
            self.mapper3 = IntervalMapper((0x80, -0x7f), (1., -1.))
            self.mapper4 = IntervalMapper((-0x80, 0x7f), (1., -1.))

        def tearDown(self):
            pass

        def test_call_1(self):
            self.assertEqual(self.mapper1(0x00), -1.)
            self.assertEqual(self.mapper1(0x39), -1.)

        def test_call_2(self):
            self.assertAlmostEqual(self.mapper1(0x38), -1., places=1)
            self.assertAlmostEqual(self.mapper1(0x48), -0.75, places=1)
            self.assertAlmostEqual(self.mapper1(0x56), -0.5, places=1)
            self.assertAlmostEqual(self.mapper1(0x64), -0.25, places=1)
            self.assertAlmostEqual(self.mapper1(0x72), -0., places=1)

        def test_call_3(self):
            self.assertEqual(self.mapper1(0x73), 0.)
            self.assertEqual(self.mapper1(0x85), 0.)
            self.assertEqual(self.mapper1(0x97), 0.)

        def test_call_4(self):
            self.assertAlmostEqual(self.mapper1(0x98), 0., places=1)
            self.assertAlmostEqual(self.mapper1(0xa6), 0.25, places=1)
            self.assertAlmostEqual(self.mapper1(0xb5), 0.5, places=1)
            self.assertAlmostEqual(self.mapper1(0xc3), 0.75, places=1)
            self.assertAlmostEqual(self.mapper1(0xd2), 1., places=1)

        def test_call_5(self):
            self.assertEqual(self.mapper1(0xd3), 1.)
            self.assertEqual(self.mapper1(0xff), 1.)

        def test_call_6(self):
            self.assertAlmostEqual(self.mapper2(0x100), -1., places=1)
            self.assertAlmostEqual(self.mapper2(0x80), -1., places=1)
            self.assertAlmostEqual(self.mapper2(0x40), -0.5, places=1)
            self.assertAlmostEqual(self.mapper2(0), 0., places=1)
            self.assertAlmostEqual(self.mapper2(-0x40), 0.5, places=1)
            self.assertAlmostEqual(self.mapper2(-0x7f), 1., places=1)
            self.assertAlmostEqual(self.mapper2(-0x100), 1., places=1)

        def test_call_7(self):
            self.assertAlmostEqual(self.mapper3(0x100), 1., places=1)
            self.assertAlmostEqual(self.mapper3(0x80), 1., places=1)
            self.assertAlmostEqual(self.mapper3(0x40), 0.5, places=1)
            self.assertAlmostEqual(self.mapper3(0), 0., places=1)
            self.assertAlmostEqual(self.mapper3(-0x40), -0.5, places=1)
            self.assertAlmostEqual(self.mapper3(-0x7f), -1., places=1)
            self.assertAlmostEqual(self.mapper3(-0x100), -1., places=1)

        def test_call_8(self):
            self.assertAlmostEqual(self.mapper4(0x100), -1., places=1)
            self.assertAlmostEqual(self.mapper4(0x80), -1., places=1)
            self.assertAlmostEqual(self.mapper4(0x40), -0.5, places=1)
            self.assertAlmostEqual(self.mapper4(0), 0., places=1)
            self.assertAlmostEqual(self.mapper4(-0x40), 0.5, places=1)
            self.assertAlmostEqual(self.mapper4(-0x7f), 1., places=1)
            self.assertAlmostEqual(self.mapper4(-0x100), 1., places=1)

    unittest.main()
