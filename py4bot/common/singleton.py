# -*- coding: utf-8 -*-

""" Py4bot framework

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Singleton pattern.

Implements
==========

 - B{Singleton}

Documentation
=============

Juste set this class as __metaclass__ attribute value of your class which need to be a Singleton.

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

class Singleton_(type):
    """ Singleton metaclass

    @ivar _instance: instance of the class
    @type _instance: object
    """
    def __init__(self, *args, **kwargs):
        """ Init the metaclass
        """
        super(Singleton, self).__init__(*args, **kwargs)

        self._instance = None

    def __call__(self, *args, **kwargs):
        if self._instance is None:
            self._instance = super(Singleton, self).__call__(*args, **kwargs)

        return self._instance


class Singleton(type):
    """
    """
    def __init__(cls, name, bases, dict_):
        """
        """
        super(Singleton, cls).__init__(name, bases, dict_)
        original_new = cls.__new__

        def my_new(cls, *args, **kwds):
            """
            """
            if cls.instance == None:
                cls.instance = original_new(cls, *args, **kwds)

            return cls.instance

        cls.instance = None
        cls.__new__ = staticmethod(my_new)


if __name__ == '__main__':
    import unittest


    class SingletonTest(object):

        __metaclass__ = Singleton


    class SingletonTestCase(unittest.TestCase):

        def setUp(self):
            pass

        def tearDown(self):
            pass

        def test_constructor(self):
            s1 = SingletonTest()
            s2 = SingletonTest()
            self.assertIs(s1, s2)


    unittest.main()
