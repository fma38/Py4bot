# -*- coding: utf-8 -*-

""" Robot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

PID controller

Implements
==========

 - B{PidController}

Documentation
=============

The update() method should be called as fast as possible, with the current value.

See U{https://en.wikipedia.org/wiki/PID_controller}

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2017-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time


class PidController:
    """ Discrete PID controller
    """

    def __init__(self, Kp=1, Ti=1, Td=1, iMax=500):
        """ Init PidController object
        """
        self._Kp = Kp
        self._Ti = Ti
        self._Td = Td
        self._iMax = iMax

        self._setpoint = 0
        self._lastError = 0
        self._lastProcessVariable = 0
        self._integral = 0

        self._previousTime = None

    def update(self, processVariable):
        """ Compute output
        """

        # Compute error
        error = self._setpoint - processVariable

        if self._previousTime is not None:
            deltaT = time.time() - self._previousTime

            # compute integral term
            self._integral += 1 / self._Ti * error * deltaT

            if abs(self._integral) > self._iMax:
                sign = self._integral / abs(self._integral)
                self._integral = sign  * self._iMax

            # Compute derivative term
            d = self._Td * (error - self._lastError) / deltaT

            self._lastError = error

        # First iteration
        else:
            d = 0

        self._previousTime = time.time()

        # Compute and return the manipulated variable (MV)
        return self._Kp * (error + self._integral + d)

    def update2(self, processVariable):
        """ Compute output

        Use process variable instead of error for p and d terms.
        """

        # Compute error
        error = self._setpoint - processVariable

        if self._previousTime is not None:
            deltaT = time.time() - self._previousTime

            # compute integral term
            self._integral += 1 / self._Ti * error * deltaT

            if abs(self._integral) > self._iMax:
                sign = self._integral / abs(self._integral)
                self._integral = sign  * self._iMax

            # Compute derivative term
            d = self._Td * (processVariable - self._lastProcessVariable) / deltaT

            self._lastProcessVariable = processVariable

        # First iteration
        else:
            d = 0

        self._previousTime = time.time()

        # Compute and return the manipulated variable (MV)
        return self._Kp * (-processVariable + self._integral - d)

    @property
    def Kp(self):
        return self._Kp

    @Kp.setter
    def Kp(self, value):
        self._Kp = value

    @property
    def Ki(self):
        return self._Kp / Self._Ti

    @property
    def Kd(self):
        return self._Kp * self._Td

    @property
    def Ti(self):
        return self._Ti

    @Ti.setter
    def Ti(self, value):
        self._Ti = value

    @property
    def Td(self):
        return self._Td

    @Td.setter
    def Td(self, value):
        self._Td = value

    @property
    def iMax(self):
        return self._iMax

    @iMax.setter
    def iMax(self, value):
        self._iMax = value

    @property
    def setpoint(self):
        return self._setpoint

    @setpoint.setter
    def setpoint(self, value):
        self._setpoint = value
        self._lastError = 0
        self._integral = 0
