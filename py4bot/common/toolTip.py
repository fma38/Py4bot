#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Tk Tooltip feature

Implements
==========

 - B{ToolTip}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@author: Michael Lange
@copyright: (C) 2017-2021 Frédéric Mantegazza
@copyright: (C) Michael Lange
@license: GPL
"""

from __future__ import division

import Tkinter as tk


class ToolTip(object):
    """ Tooltip widget for Tkinter

    (C) Michael Lange <klappnase (at) freakmail (dot) de>

    The ToolTip class provides a flexible tooltip widget for Tkinter; it is based on IDLE's ToolTip
    module which unfortunately seems to be broken.

    Init options:

    anchor:         where the text should be positioned inside the widget, must be on of "n", "s", "e", "w", "nw" and so on;
                    default is "center"
    bd:             borderwidth of the widget; default is 1 (NOTE: don't use "borderwidth" here)
    bg:             background color to use for the widget; default is "lightyellow" (NOTE: don't use "background")
    delay:          time in ms that it takes for the widget to appear on the screen when the mouse pointer has
                    entered the parent widget; default is 500
    fg:             foreground (i.e. text) color to use; default is "black" (NOTE: don't use "foreground")
    followmouse:    if set to 1 the tooltip will follow the mouse pointer instead of being displayed
                    outside of the parent widget; this may be useful if you want to use tooltips for
                    large widgets like listboxes or canvases; default is 0
    font:           font to use for the widget; default is system specific
    justify:        how multiple lines of text will be aligned, must be "left", "right" or "center"; default is "left"
    padx:           extra space added to the left and right within the widget; default is 4
    pady:           extra space above and below the text; default is 2
    relief:         one of "flat", "ridge", "groove", "raised", "sunken" or "solid"; default is "solid"
    state:          must be "normal" or "disabled"; if set to "disabled" the tooltip will not appear; default is "normal"
    text:           the text that is displayed inside the widget
    textvariable:   if set to an instance of Tkinter.StringVar() the variable's value will be used as text for the widget
    width:          width of the widget; the default is 0, which means that "wraplength" will be used to limit the widgets width
    wraplength:     limits the number of characters in each line; default is 300

    Widget methods:

    configure(**opts):  change one or more of the widget's options as described above; the changes will take effect the
                        next time the tooltip shows up; NOTE: followmouse cannot be changed after widget initialization

    Other widget methods that might be useful if you want to subclass ToolTip:

    _onEnter():         callback when the mouse pointer enters the parent widget
    _onLeave():         called when the mouse pointer leaves the parent widget
    _onMotion():        is called when the mouse pointer moves inside the parent widget if followmouse is set to 1 and the
                        tooltip has shown up to continually update the coordinates of the tooltip window
    _coords():          calculates the screen coordinates of the tooltip window
    _create_contents(): creates the contents of the tooltip window (by default a Tkinter.Label)

    Ideas gleaned from PySol
    """
    def __init__(self, master, text, delay=500, **opts):
        """
        """
        super(ToolTip, self).__init__()

        self._master = master

        self._opts = {
            'anchor': "center", 'bd': 1, 'bg': "lightyellow", 'delay': delay, 'fg': "black",
            'followmouse': 0, 'font': None, 'justify': "left", 'padx': 4, 'pady': 2,
            'relief': "solid", 'state': "normal", 'text': text, 'textvariable': None,
            'width': 0, 'wraplength': 300
        }
        self.configure(**opts)

        self._tipwindow = None
        self._id = None

        # Bind signals
        self._id1 = self._master.bind("<Enter>", self._onEnter, '+')
        self._id2 = self._master.bind("<Leave>", self._onLeave, '+')
        self._id3 = self._master.bind("<ButtonPress>", self._onLeave, '+')
        if self._opts['followmouse']:
            self._id4 = self._master.bind("<Motion>", self._onMotion, '+')
            self._followmouse = True
        else:
            self._followmouse = False

    def configure(self, **opts):
        """
        """
        for key in opts:
            if self._opts.has_key(key):
                self._opts[key] = opts[key]
            else:
                raise KeyError("Unknown option (%s)" % key)

    def _onEnter(self, event=None):
        """
        """
        self._schedule()

    def _onLeave(self, event=None):
        """
        """
        self._unschedule()
        self._hide()

    def _onMotion(self, event=None):
        """
        """
        if self._tipwindow and self._followmouse:
            x, y = self._coords()
            self._tipwindow.wm_geometry("+%d+%d" % (x, y))

    def _schedule(self):
        """
        """
        self._unschedule()
        if self._opts['state'] == 'disabled':
            return

        self._id = self._master.after(self._opts['delay'], self._show)

    def _unschedule(self):
        """
        """
        id = self._id
        self._id = None
        if id:
            self._master.after_cancel(id)

    def _show(self):
        """
        """
        if self._opts['state'] == 'disabled':
            self._unschedule()
            return

        if not self._tipwindow:
            self._tipwindow = tw = tk.Toplevel(self._master)

            # Hide the window until we know the geometry
            tw.withdraw()
            tw.wm_overrideredirect(1)

            if tw.tk.call("tk", "windowingsystem") == 'aqua':
                tw.tk.call("::tk::unsupported::MacWindowStyle", "style", tw._w, "help", "none")

            self._create_contents()
            tw.update_idletasks()
            x, y = self._coords()
            tw.wm_geometry("+%d+%d" % (x, y))
            tw.deiconify()

    def _hide(self):
        """
        """
        tw = self._tipwindow
        self._tipwindow = None
        if tw:
            tw.destroy()

    def _coords(self):
        """

        The tip window must be completely outside the master widget;
        otherwise when the mouse enters the tip window we get
        a leave event and it disappears, and then we get an enter
        event and it reappears, and so on forever :-(
        Or we take care that the mouse pointer is always outside the tipwindow :-)
        """
        tw = self._tipwindow
        twx, twy = tw.winfo_reqwidth(), tw.winfo_reqheight()
        w, h = tw.winfo_screenwidth(), tw.winfo_screenheight()

        # Calculate the y coordinate:
        if self._followmouse:
            y = tw.winfo_pointery() + 20

            # Make sure the tipwindow is never outside the screen:
            if y + twy > h:
                y = y - twy - 30
        else:
            y = self._master.winfo_rooty() + self._master.winfo_height() + 3
            if y + twy > h:
                y = self._master.winfo_rooty() - twy - 3

        # We can use the same x coord in both cases:
        x = tw.winfo_pointerx() - twx / 2
        if x < 0:
            x = 0
        elif x + twx > w:
            x = w - twx
        return x, y

    def _create_contents(self):
        """
        """
        opts = self._opts.copy()
        for opt in ('delay', 'followmouse', 'state'):
            del opts[opt]
        label = tk.Label(self._tipwindow, **opts)
        label.pack()
