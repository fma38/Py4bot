# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Custom exceptions

Implements
==========

 - Py4botError
 - Py4botValueError
   Py4botIKError
 - Py4botAttributeError
 - Py4botTypeError
 - Py4botInterrupt
 - Py4botHardwareError
 - Py4botFsmError

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""


class Py4botError(Exception):
    """ Base class for Py4bot errors
    """

class Py4botValueError(Py4botError):
    """ Py4bot value error
    """

class Py4botIKError(Py4botError):
    """ Py4bot IK computation error
    """

class Py4botAttributeError(Py4botError):
    """ Py4bot attribute error
    """

class Py4botTypeError(Py4botError):
    """ Py4bot type error
    """

class Py4botInterrupt(Py4botError):
    """ Py4bot interrupt
    """

class Py4botHardwareError(Py4botError):
    """ Py4bot Hardware error
    """

class Py4botFsmError(Py4botError):
    """ Py4bot error is FSM
    """
