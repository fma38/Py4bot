#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Servos management

Implements
==========

 - B{View}
 - B{Controller}
 - B{main}

Documentation
=============

This tool helps calibrating servos.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import sys
import time
import math
import argparse
import collections

import Tkinter as tk

from py4bot.services.logger import Logger
from py4bot.common.exception import Py4botValueError
from py4bot.common.toolTip import ToolTip
from py4bot.core.joints import Joint
from py4bot.actuators.servo import Servo
from py4bot.actuators.servoPool import ServoPool

sys.path.insert(0, ".")
try:
    import settings
    try:
        settings.SERVOS_CALIBRATION
    except AttributeError:
        Logger().warning("Can't find SERVOS_CALIBRATION in 'settings' module; will use default values")
    try:
        settings.LEGS_SERVOS_MAPPING
    except AttributeError:
        Logger().warning("Can't find LEGS_SERVOS_MAPPING in 'settings' module; no mapping will be added as comment in the generated config")
except ImportError:
    Logger().warning("Can't import 'settings' module; will use default config")
else:
    Logger().info("Loading previous config from 'settings' module")

DEFAULT_ENABLE = False
DEFAULT_OFFSET = 0
DEFAULT_OFFSET_NAMED = {'coxa': 0, 'femur': 180, 'tibia': 90, 'tars': 180}
DEFAULT_NEUTRAL = 1500
DEFAULT_RATIO = 90 / 1000
DEFAULT_TEST_ANGLE = 0

DEFAULT_PULSE = 0

OFFSET_MIN = -270
OFFSET_MAX = +270
NEUTRAL_MIN = 250
NEUTRAL_MAX = 2750
RATIO_MIN = 45 / 1000
RATIO_MAX = 135 / 1000
TEST_ANGLE_MIN = -90
TEST_ANGLE_MAX = 90

FONT_SMALL = ("Helvetica", 10)
FONT_NORMAL = ("Helvetica", 11)
FONT_LARGE = ("Helvetica", 12)
FONT_FIXED = ("Monospace", 11)

LEGS_SERVOS_MAPPING_REVERSE = {}


class View(object):
    """
    """
    def __init__(self, root, params):
        """
        """
        super(View, self).__init__()

        self._root = root
        self._params = params

        self._createWidgets()

    @property
    def root(self):
        return self._root

    def _createWidgets(self):
        """
        """
        self._root.grid()

        tk.Label(self._root, text="Servo Num:", fg="red", font=FONT_NORMAL).grid(row=0, column=0, sticky=tk.E)
        frame = tk.Frame(self._root)
        frame.grid(row=0, column=1, columnspan=3, sticky=tk.W)

        self.servoNumSpinbox = tk.Spinbox(frame, values=self._params['servoNums'], width=2, font=FONT_NORMAL)
        self.servoNumSpinbox.grid(row=0, column=0, sticky=tk.W)
        ToolTip(self.servoNumSpinbox, "Current servo number", font=FONT_SMALL)

        self.servoDoc = tk.Label(frame, text="", width=9, fg="green", font=FONT_FIXED)
        self.servoDoc.grid(row=0, column=1, sticky=tk.W)
        ToolTip(self.servoNumSpinbox, "Current servo doc", font=FONT_SMALL)

        self.servoEnable = tk.IntVar()
        self.servoEnableCheckButton = tk.Checkbutton(frame, text="Enable", fg="red", variable=self.servoEnable, font=FONT_NORMAL)
        self.servoEnableCheckButton.grid(row=0, column=2, sticky=tk.W)
        ToolTip(self.servoEnableCheckButton, "Enable servo output", font=FONT_SMALL)

        self.servoInvert = tk.IntVar()
        self.servoInvertCheckButton = tk.Checkbutton(frame, text="Invert", fg="red", variable=self.servoInvert, font=FONT_NORMAL)
        self.servoInvertCheckButton.grid(row=0, column=3, sticky=tk.W)
        ToolTip(self.servoInvertCheckButton, "Invert servo direction", font=FONT_SMALL)

        self.resetButton = tk.Button(self._root, text="Reset", font=FONT_NORMAL)
        self.resetButton.grid(row=0, column=4, sticky=tk.W+tk.E)
        ToolTip(self.resetButton, "Reset current servo params", font=FONT_SMALL)

        tk.Label(self._root, text="Offset:", fg="red", font=FONT_NORMAL).grid(row=1, column=0, sticky=tk.E)
        self.offsetScale = tk.Scale(self._root, from_=OFFSET_MIN, to=OFFSET_MAX, resolution=90, tickinterval=90, orient=tk.HORIZONTAL, length=450, font=FONT_NORMAL)
        self.offsetScale.grid(row=1, column=1, sticky=tk.W)
        self.offsetScale.set(DEFAULT_OFFSET)
        ToolTip(self.offsetScale, "Servo offset (°)", font=FONT_SMALL)

        tk.Label(self._root, text="Neutral:", fg="red", font=FONT_NORMAL).grid(row=2, column=0, sticky=tk.E)
        self.neutralScale = tk.Scale(self._root, from_=NEUTRAL_MIN, to=NEUTRAL_MAX, resolution=5, tickinterval=250, orient=tk.HORIZONTAL, length=450, font=FONT_NORMAL)
        self.neutralScale.grid(row=2, column=1, sticky=tk.W)
        self.neutralScale.set(DEFAULT_NEUTRAL)
        ToolTip(self.neutralScale, "Neutral pulse width (µs)", font=FONT_SMALL)

        tk.Label(self._root, text="Ratio:", fg="red", font=FONT_NORMAL).grid(row=3, column=0, sticky=tk.E)
        self.ratioScale = tk.Scale(self._root, from_=RATIO_MIN, to=RATIO_MAX, resolution=0.001, tickinterval=0.015, orient=tk.HORIZONTAL, length=450, font=FONT_NORMAL)
        self.ratioScale.grid(row=3, column=1, sticky=tk.W)
        self.ratioScale.set(DEFAULT_RATIO)
        ToolTip(self.ratioScale, "Ratio (°/µs)", font=FONT_SMALL)

        tk.Label(self._root, text="Test angle:", fg="red", font=FONT_NORMAL).grid(row=4, column=0, sticky=tk.E)
        self.testAngleScale = tk.Scale(self._root, from_=TEST_ANGLE_MIN, to=TEST_ANGLE_MAX, resolution=5, tickinterval=15, orient=tk.HORIZONTAL, length=450, font=FONT_NORMAL)
        self.testAngleScale.grid(row=4, column=1, sticky=tk.W)
        self.testAngleScale.set(DEFAULT_TEST_ANGLE)
        ToolTip(self.testAngleScale, "Test angle (°)", font=FONT_SMALL)

        tk.Label(self._root, text="offset:", fg="blue", font=FONT_NORMAL).grid(row=5, column=0, sticky=tk.E)
        self.offset = tk.Label(self._root, text="%d°" % DEFAULT_OFFSET, font=FONT_NORMAL)
        self.offset.grid(row=5, column=1, sticky=tk.W)
        tk.Label(self._root, text="neutral:", fg="blue", font=FONT_NORMAL).grid(row=6, column=0, sticky=tk.E)
        self.neutral = tk.Label(self._root, text="%dµs" % DEFAULT_NEUTRAL, font=FONT_NORMAL)
        self.neutral.grid(row=6, column=1, sticky=tk.W)
        tk.Label(self._root, text="ratio:", fg="blue", font=FONT_NORMAL).grid(row=7, column=0, sticky=tk.E)
        self.ratio = tk.Label(self._root, text="%.3f" % DEFAULT_RATIO, font=FONT_NORMAL)
        self.ratio.grid(row=7, column=1, sticky=tk.W)

        self.statusLabel = tk.Label(self._root, text="", relief=tk.RIDGE, font=FONT_LARGE)
        self.statusLabel.grid(row=9, column=0, rowspan=len(self._params['servoNums'])+2, columnspan=5, sticky=tk.W+tk.E)

        self.configToplevel = tk.Toplevel(self._root)
        self.configToplevel.resizable(0, 0)
        self.configEntry = tk.Text(self.configToplevel, height=len(self._params['servoNums'])+2, font=FONT_FIXED)
        self.configEntry.grid(row=0, column=0, columnspan=3, sticky=tk.W+tk.E)

    def destroy(self):
        """
        """
        self._root.destroy()


class Controller(object):
    """
    """
    def __init__(self, model, view):
        """
        """
        super(Controller, self).__init__()

        self._model = model
        self._view = view

        self._afterId = None
        self._bindEvents()

        self._refreshInputGui()
        self._refreshOutputGui()

    @property
    def servoNum(self):
        return int(self._view.servoNumSpinbox.get())

    @property
    def servoEnable(self):
        return bool(self._view.servoEnable.get())

    @property
    def servoInvert(self):
        return bool(self._view.servoInvert.get())

    @property
    def servoDir(self):
        if self._view.servoInvert.get():
            return -1
        else:
            return +1

    @property
    def servoOffset(self):
        return self._view.offsetScale.get()

    @property
    def servoNeutral(self):
        return self._view.neutralScale.get()

    @property
    def servoRatio(self):
        return self._view.ratioScale.get()

    @property
    def servoTestAngle(self):
        return self._view.testAngleScale.get()

    def _bindEvents(self):
        """
        """
        self._view.servoNumSpinbox.configure(command=self._onServoNum)
        self._view.servoEnableCheckButton.configure(command=self._onServoEnable)
        self._view.servoInvertCheckButton.configure(command=self._onServoInvert)
        self._view.resetButton.configure(command=self._onReset)
        self._view.offsetScale.configure(command=self._onOffsetScale)
        self._view.neutralScale.configure(command=self._onNeutralScale)
        self._view.ratioScale.configure(command=self._onRatioScale)
        self._view.testAngleScale.configure(command=self._onTestAngleScale)

        self._view.root.protocol("WM_DELETE_WINDOW", self._onQuit)

    def _setStatusLine(self, text, fg="black", delay=5000):
        """
        """
        Logger().trace("Controller._setStatusLine()")

        if self._afterId is not None:
            self._view._root.after_cancel(self._afterId)
        self._view.statusLabel.configure(text=text, fg=fg)
        if delay:
            self._afterId = self._view._root.after(delay, self._clearStatusLine)

    def _clearStatusLine(self):
        """
        """
        Logger().trace("Controller._clearStatusLine()")

        self._view.statusLabel.configure(text="")

    def _getCurrentServo(self):
        """
        """
        Logger().trace("Controller._getCurrentServo()")

        for servo in self._model['servoPool'].servos:
            if servo.num == self.servoNum:
                return servo
        else:
            raise Py4botValueError("Unknown servo num")

    def _servoDoc(self, num):
        """
        """
        try:
            return LEGS_SERVOS_MAPPING_REVERSE[num]
        except KeyError:
            return {'leg': "", 'joint': ""}

    def _refreshInputGui(self):
        """ Refresh GUI

        Refresh input values.
        """
        Logger().trace("Controller._refreshInputGui()")

        servo = self._getCurrentServo()
        self._view.servoDoc.configure(text="%(leg)s %(joint)-5s" % self._servoDoc(self.servoNum))
        self._view.servoEnable.set(servo.guiEnable)
        if servo._ratio < 0:
            self._view.servoInvert.set(True)
        else:
            self._view.servoInvert.set(False)
        self._view.offsetScale.set(servo._offset)
        self._view.neutralScale.set(servo._neutral)
        self._view.ratioScale.set(abs(servo._ratio))
        self._view.testAngleScale.set(servo.guiTestAngle)

        joint = self._servoDoc(self.servoNum)['joint']
        if joint == "coxa":
            self._setStatusLine("'coxa' should turn CCW (from top view) when increasing angle", "darkblue", delay=0)
        elif joint == "femur":
            self._setStatusLine("'%s' should go up when increasing angle" % joint, "darkblue", delay=0)
        elif joint in ("tibia", "tars"):
            self._setStatusLine("'%s' should go out when increasing angle" % joint, "darkblue", delay=0)
        else:
            self._clearStatusLine()

    def _refreshOutputGui(self):
        """ Refresh GUI

        Refresh output values.
        """
        Logger().trace("Controller._refreshOutputGui()")

        servo = self._getCurrentServo()
        self._view.offset.configure(text="%d°" % servo._offset)
        self._view.neutral.configure(text="%dµs" % servo._neutral)
        self._view.ratio.configure(text="%.3f°/µs" % servo._ratio)

        # Refresh LEGS_SERVOS_MAPPING settings window
        self._view.configEntry.delete(1., tk.END)
        self._view.configEntry.insert(0., self._generateOutput())

    def _generateOutput(self):
        """
        """
        Logger().trace("Controller._generateOutput()")

        output = "SERVOS_CALIBRATION = {\n"
        for servo in self._model['servoPool'].servos:
            output += "    %2d: {'offset': %+4d., 'neutral': %4d, 'ratio': %+5.3f},  # %s\n" % \
                      (servo.num, servo._offset, servo._neutral, servo._ratio, "%(leg)s %(joint)-5s" % self._servoDoc(servo.num))
        output += "}\n"

        return output

    def _refreshServoPosition(self, duration=0):
        """
        """
        Logger().trace("Controller._refreshServoPosition()")

        servo = self._getCurrentServo()
        if self.servoEnable:
            pulse = int(servo._neutral + self.servoTestAngle / servo._ratio)
        else:
            pulse = 0
        Logger().debug("Controller._refreshServoPosition(): servo pulse=%d" % pulse)

        servo.pulse = pulse

        self._model['servoPool'].move()

    def _onServoNum(self, *args):
        """
        """
        Logger().debug("Controller._onServoNum(): num servo=%d" % self.servoNum)

        self._refreshInputGui()
        self._refreshOutputGui()

    def _onServoEnable(self, *args):
        """
        """
        Logger().debug("Controller._onServoEnable(): servo enable=%s" % self.servoEnable)

        servo = self._getCurrentServo()
        servo.guiEnable = self.servoEnable

        self._refreshOutputGui()
        self._refreshServoPosition()

    def _onServoInvert(self, *args):
        """
        """
        Logger().debug("Controller._onServoInvert(): servo invert=%s" % self.servoInvert)

        servo = self._getCurrentServo()
        servo._ratio = self.servoDir * abs(servo._ratio)

        self._refreshOutputGui()
        self._refreshServoPosition(duration=1)

    def _onReset(self, *args):
        """
        """
        Logger().trace("Controller._onReset()")

        servo = self._getCurrentServo()
        try:
            servo._offset = settings.SERVOS_CALIBRATION[self.servoNum]['offset']
            servo._neutral = settings.SERVOS_CALIBRATION[self.servoNum]['neutral']
            servo._ratio = settings.SERVOS_CALIBRATION[self.servoNum]['ratio']
        except (NameError, AttributeError):
            try:
                servo._offset = DEFAULT_OFFSET_NAMED[LEGS_SERVOS_MAPPING_REVERSE[self.servoNum]['joint']]
            except KeyError:
                servo._offset = DEFAULT_OFFSET
            servo._neutral = DEFAULT_NEUTRAL
            servo._ratio = DEFAULT_RATIO

        servo.guiEnable = DEFAULT_ENABLE
        servo.guiTestAngle = DEFAULT_TEST_ANGLE

        self._refreshInputGui()
        self._refreshServoPosition()

    def _onOffsetScale(self, *args):
        """
        """
        Logger().debug("Controller._onOffsetScale(): offset=%s" % self.servoOffset)

        servo = self._getCurrentServo()
        servo._offset = self.servoOffset

        self._refreshOutputGui()
        self._refreshServoPosition()

    def _onNeutralScale(self, *args):
        """
        """
        Logger().debug("Controller._onNeutralScale(): neutral=%s" % self.servoNeutral)

        servo = self._getCurrentServo()
        servo._neutral = self.servoNeutral

        self._refreshOutputGui()
        self._refreshServoPosition()

    def _onRatioScale(self, *args):
        """
        """
        Logger().debug("Controller._onRatioScale(): ratio=%s" % self.servoRatio)

        servo = self._getCurrentServo()
        servo._ratio = self.servoDir * self.servoRatio

        self._refreshOutputGui()
        self._refreshServoPosition()

    def _onTestAngleScale(self, *args):
        """
        """
        Logger().debug("Controller._onTestAngleScale(): test angle=%s" % self.servoTestAngle)

        servo = self._getCurrentServo()
        servo.guiTestAngle = self.servoTestAngle

        self._refreshOutputGui()
        self._refreshServoPosition()

    def _onQuit(self):
        """
        """
        print
        print self._generateOutput()
        self._view.destroy()


def main():
    """
    """
    def hexdec(value):
        """
        """
        try:
            return str(int(value))
        except ValueError:
            return str(int(value, 16))

    # Parser options
    parser = argparse.ArgumentParser(description="This tool is used to calibrate servos.")
    parser.add_argument("-l", "--logger",
                        choices=("trace", "debug", "info", "warning", "error", "exception", "critical"), default="info",
                        dest="loggerLevel", metavar="LEVEL",
                        help="logger level")
    #parser.add_argument("-p", "--path",
                        #default=".",
                        #help="path to search for 'settings' module")
    parser.add_argument("-d", "--driver",
                        choices=("pru", "pca9685", "veyron", "pololuserial", "usc32", "fake"), default="fake",
                        help="servos driver to use")

    # Only for pca9685
    pca9685Group = parser.add_argument_group("pca9685")
    pca9685Group.add_argument("-b", "--bus",
                              type=int, default=1,
                              help="I²C bus to use")
    pca9685Group.add_argument("-f", "--frequency",
                              type=float, default=50.,
                              help="PWM frequency, in Hz")
    pca9685Group.add_argument("-a", "--address",
                              type=hexdec, nargs='+',
                              help="I²C address of PCA 9685 chips")
    pca9685Group.add_argument("-c", "--curve",
                              choices=("linear", "smooth", "damped", "accel"), default="linear",
                              help="response curve")

    # Only for veyron/pololuserial/usc32
    veyronPololuGroup = parser.add_argument_group("veyron/pololuserial/usc32")
    veyronPololuGroup.add_argument("-p", "--port",
                                   default="/dev/ttyACM0",
                                   help="serial port")
    veyronPololuGroup.add_argument("-r", "--baudrate",
                                   type=int, default=9600,
                                   help="serial baudrate")

    # Only for fake
    fakeGroup = parser.add_argument_group("fake")
    fakeGroup.add_argument("-n", "--number",
                           type=int, default=32,
                           help="Number of fake servos")

    args = parser.parse_args()

    Logger().setLevel(args.loggerLevel)

    Logger().debug("main(): args=%s" % args)

    nbServos = None

    if args.driver == "pru":
        from py4bot.drivers.servosPRU import ServosPRU
        nbServos = 32
        driver = ServosPRU()

    elif args.driver == "pca9685":
        from py4bot.drivers.pca9685Driver import Pca9685Driver
        from py4bot.common import config
        config.I2C_BUS = args.bus
        config.PCA9685_DRIVER_PWM_FREQUENCY = args.frequency
        config.PCA9685_DRIVER_CURVE = args.curve
        nbServos = 16 * len(args.address)
        driver = Pca9685Driver(args.address)

    elif args.driver == "veyron":
        from py4bot.drivers.veyron import Veyron
        nbServos = 24
        driver = Veyron(args.port, args.baudrate)

    elif args.driver == "pololuserial":
        from py4bot.drivers.pololuSerial import PololuSerial
        nbServos = 8
        driver = PololuSerial(args.port, args.baudrate)

    elif args.driver == "usc32":
        from py4bot.drivers.usc32 import USC32
        nbServos = 32
        driver = USC32(args.port, args.baudrate)

    elif args.driver == "fake":
        from py4bot.drivers.fakeDriver import FakeDriver
        nbServos = args.number
        driver = FakeDriver()

    # Create the legs servos mapping reverse table
    try:
        for leg, val in settings.LEGS_SERVOS_MAPPING.items():
            for joint, num in val.items():
                LEGS_SERVOS_MAPPING_REVERSE[num] = {'joint': joint, 'leg': leg}
    except (NameError, AttributeError):
        for num in range(nbServos):
                LEGS_SERVOS_MAPPING_REVERSE[num] = {'joint': "", 'leg': ""}

    # Create servo pool
    servoPool = ServoPool(driver)  # TODO when using legsIndex and all settings params -> manage joints with names
    try:
        servoNums = settings.SERVOS_CALIBRATION.keys()
    except (NameError, AttributeError):
        servoNums = range(nbServos)

    for num in servoNums:
        joint = Joint("Servo %2d" % num)
        servo = Servo(joint, num)

        try:
            servo._offset = settings.SERVOS_CALIBRATION[num]['offset']
            servo._neutral = settings.SERVOS_CALIBRATION[num]['neutral']
            servo._ratio = settings.SERVOS_CALIBRATION[num]['ratio']
        except (NameError, KeyError, AttributeError):
            #Logger().exception("main()", debug=True)
            try:
                servo._offset = DEFAULT_OFFSET_NAMED[LEGS_SERVOS_MAPPING_REVERSE[num]['joint']]
            except KeyError:
                servo._offset = DEFAULT_OFFSET
            servo._neutral = DEFAULT_NEUTRAL
            servo._ratio = DEFAULT_RATIO

        servo.guiEnable = DEFAULT_ENABLE
        servo.guiTestAngle = DEFAULT_TEST_ANGLE

        servo.pulse = DEFAULT_PULSE

        servoPool.add(servo)

    # Create Tkinter application
    root = tk.Tk()
    root.title("Servos Calibrator")
    root.resizable(0, 0)

    # Create View
    params = {'servoNums': servoNums}
    view = View(root, params)

    # Create controller
    model = {'servoPool': servoPool}  # Use a class?
    controller = Controller(model, view)

    root.mainloop()

    # Stop servos
    #for servo in servoPool.servos:
        #servo.pulse = 0
    #servoPool.move()


if __name__ == '__main__':
    main()

"""
# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs / servos mapping
LEGS_SERVOS_MAPPING = {
    'RF': {'coxa': 16, 'femur': 17, 'tibia': 18, 'tars': 19},
    'RM': {'coxa': 20, 'femur': 21, 'tibia': 22, 'tars': 23},
    'RR': {'coxa': 24, 'femur': 25, 'tibia': 26, 'tars': 27},
    'LR': {'coxa':  8, 'femur':  9, 'tibia': 10, 'tars': 11},
    'LM': {'coxa':  4, 'femur':  5, 'tibia':  6, 'tars':  7},
    'LF': {'coxa':  0, 'femur':  1, 'tibia':  2, 'tars':  3}
}

remap to:

LEGS_SERVOS_MAPPING_REVERSE = {
     0: {'joint': "coxa",  'leg': "LF"},
     1: {'joint': "femur", 'leg': "LF"},
     2: {'joint': "tibia", 'leg': "LF"},
     3: {'joint': "tars",  'leg': "LF"},
     4: {'joint': "coxa",  'leg': "LM"},
     5: {'joint': "femur", 'leg': "LM"},
     6: {'joint': "tibia", 'leg': "LM"},
     7: {'joint': "tars",  'leg': "LM"},
     8: {'joint': "coxa",  'leg': "LR"},
     9: {'joint': "femur", 'leg': "LR"},
    10: {'joint': "tibia", 'leg': "LR"},
    11: {'joint': "tars",  'leg': "LR"},
    16: {'joint': "coxa",  'leg': "RF"},
    17: {'joint': "femur", 'leg': "RF"},
    18: {'joint': "tibia", 'leg': "RF"},
    19: {'joint': "tars",  'leg': "RF"},
    20: {'joint': "coxa",  'leg': "RM"},
    21: {'joint': "femur", 'leg': "RM"},
    22: {'joint': "tibia", 'leg': "RM"},
    23: {'joint': "tars",  'leg': "RM"},
    24: {'joint': "coxa",  'leg': "RR"}
    25: {'joint': "femur", 'leg': "RR"}
    26: {'joint': "tibia", 'leg': "RR"}
    27: {'joint': "tars",  'leg': "RR"}
}
"""
