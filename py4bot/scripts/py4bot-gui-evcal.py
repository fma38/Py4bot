#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Input devices management

Implements
==========

 - B{View}
 - B{Controller}
 - B{main}

Documentation
=============

This tool helps calibrating gamepad/joysticks.

Usage
=====

TODO
====

Use settings.py

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import sys
import time
import pprint
import argparse
import collections

import evdev
import Tkinter as tk

from py4bot.services.logger import Logger
#from py4bot.common.toolTip import ToolTip

FONT_SMALL = ("Helvetica", 10)
FONT_NORMAL = ("Helvetica", 11)
FONT_LARGE = ("Helvetica", 12)
FONT_FIXED = ("Monospace", 11)


class View(object):
    """
    """
    def __init__(self, root, params):
        """
        """
        super(View, self).__init__()

        self._root = root
        self._params = params

        self._createWidgets()

    @property
    def root(self):
        return self._root

    def _createWidgets(self):
        """
        """
        self._root.grid()

        frame = tk.Frame(self._root, relief=tk.SUNKEN, bd=1, padx=2, pady=2)
        frame.grid(row=0, column=0, sticky=tk.W+tk.E+tk.N+tk.S)
        self.keyLabels = {}
        button = 0
        for row, (codeName, codeNum) in enumerate(self._params['EV_KEY'].items()):
            tk.Label(frame, text="%s - 0x%03x -> digital_%03d: " % (codeName, codeNum, button), font=FONT_SMALL).grid(row=row, column=0, sticky=tk.E)
            self.keyLabels[codeNum] = tk.Label(frame, text=" 0", fg="blue", font=FONT_FIXED)
            self.keyLabels[codeNum].grid(row=row, column=1, sticky=tk.E)
            button += 1

        frame = tk.Frame(self._root, relief=tk.SUNKEN, bd=1, padx=2, pady=2)
        frame.grid(row=0, column=1, sticky=tk.W+tk.E+tk.N+tk.S)
        self.absLabels = {}
        self.absInv = {}
        self.absInvCheckButton = {}
        analog = 0
        for row, (codeName, codeNum) in enumerate(self._params['EV_ABS'].items()):
            tk.Label(frame, text="%s - 0x%02x [%+04d:%+04d] -> analog_%02d: " % (codeName, codeNum[0], codeNum[1], codeNum[2], analog), font=FONT_SMALL).grid(row=row, column=0, sticky=tk.E)
            self.absLabels[codeNum[0]] = tk.Label(frame, text="+000", fg="blue", font=FONT_FIXED)
            self.absLabels[codeNum[0]].grid(row=row, column=1, sticky=tk.E)
            self.absInv[codeNum[0]] = tk.IntVar()
            self.absInvCheckButton[codeNum[0]] = tk.Checkbutton(frame, text="Invert", variable=self.absInv[codeNum[0]], font=FONT_SMALL)
            self.absInvCheckButton[codeNum[0]].grid(row=row, column=2, sticky=tk.E)
            analog += 1

        frame = tk.Frame(self._root, relief=tk.SUNKEN, bd=1, padx=2, pady=2)
        frame.grid(row=0, column=2, sticky=tk.W+tk.E+tk.N+tk.S)
        self.relLabels = {}
        for row, (codeName, codeNum) in enumerate(self._params['EV_REL'].items()):
            tk.Label(frame, text="%s - 0x%02x: " % (codeName, codeNum), font=FONT_SMALL).grid(row=row, column=0, sticky=tk.E)
            self.relLabels[codeNum] = tk.Label(frame, text="+000", fg="blue", font=FONT_FIXED)
            self.relLabels[codeNum].grid(row=row, column=1, sticky=tk.E)

        self.statusLabel = tk.Label(self._root, text="", relief=tk.RIDGE, font=FONT_LARGE)
        self.statusLabel.grid(row=1, column=0, columnspan=3, sticky=tk.W+tk.E)

        self.configToplevel = tk.Toplevel(self._root)
        self.configToplevel.resizable(0, 0)
        height = len(self._params['EV_KEY']) + 2 + 1 + 2 * (len(self._params['EV_ABS']) + 2) + 1
        self.configEntry = tk.Text(self.configToplevel, height=height, width=65, font=FONT_FIXED)
        self.configEntry.grid(row=0, column=0, columnspan=3, sticky=tk.W+tk.E)

    def destroy(self):
        """
        """
        self._root.destroy()


class Controller(object):
    """
    """
    def __init__(self, model, view):
        """
        """
        super(Controller, self).__init__()

        self._model = model
        self._view = view

        #self._afterId = None
        self._bindEvents()

        self._run = False

        # Refresh settings window
        self._view.configEntry.delete(1., tk.END)
        self._view.configEntry.insert(0., self._generateOutput())

    def _bindEvents(self):
        """
        """
        print self._view.absInvCheckButton
        for codeName, codeNum in self._model['params']['EV_ABS'].items():
            self._view.absInvCheckButton[codeNum[0]].configure(command=self._onAbsInvert)
        self._view.root.protocol("WM_DELETE_WINDOW", self._onQuit)

    #def _setStatusLine(self, text, fg="black", delay=5000):
        #"""
        #"""
        #Logger().trace("Controller._setStatusLine()")

        #if self._afterId is not None:
            #self._view._root.after_cancel(self._afterId)
        #self._view.statusLabel.configure(text=text, fg=fg)
        #self._afterId = self._view._root.after(delay, self._clearStatusLine)

    #def _clearStatusLine(self):
        #"""
        #"""
        #Logger().trace("Controller._clearStatusLine()")

        #self._view.statusLabel.configure(text="")

    def _onAbsInvert(self, *args):
        """
        """
        #Logger().debug("Controller._onAbsInvert(): abs invert=%s" % self.servoInvert)  # @todo: fix value

        # Refresh settings window
        self._view.configEntry.delete(1., tk.END)
        self._view.configEntry.insert(0., self._generateOutput())

    def _refreshGui(self, codeType, codeNum, codeValue):
        """ Refresh GUI

        Refresh input values.
        """
        Logger().trace("Controller._refreshGui()")

        if codeType == 'EV_KEY':
            if codeValue:
                color = 'red'
            else:
                color = 'black'
            self._view.keyLabels[codeNum].configure(text=" %d" % codeValue)

        elif codeType == 'EV_REL':
            self._view.relLabels[codeNum].configure(text="%+04d" % codeValue)

        elif codeType == 'EV_ABS':
            if self._view.absInv[codeNum].get():
                self._view.absLabels[codeNum].configure(text="%+04d" % (-codeValue))
            else:
                self._view.absLabels[codeNum].configure(text="%+04d" % codeValue)

        # Refresh settings window
        self._view.configEntry.delete(1., tk.END)
        self._view.configEntry.insert(0., self._generateOutput())

    def _generateOutput(self):
        """
        """
        output = "BUTTON_MAPPING = {\n"
        for i, (codeName, codeNum) in enumerate(self._model['params']['EV_KEY'].items()):
            output += "    0x%03x: % 3d,  # %-15s -> digital_%03d\n" % (codeNum, i, codeName, i)
        output += "}\n\n"

        output += "AXIS_MAPPING = {\n"
        for i, (codeName, codeNum) in enumerate(self._model['params']['EV_ABS'].items()):
            output += "    0x%02x: % 2d,  # %-10s -> analog_%02d\n" % (codeNum[0], i, codeName, i)
        output += "}\n\n"

        output += "DEFAULT_AXIS_CALIBRATION = {\n"
        for i, (codeName, codeNum) in enumerate(self._model['params']['EV_ABS'].items()):
            if self._view.absInv[codeNum[0]].get():
                output += "    % 2d: (%+ 4d, %+ 4d),  # %s\n" % (i, codeNum[2], codeNum[1], codeName)
            else:
                output += "    % 2d: (%+ 4d, %+ 4d),  # %s\n" % (i, codeNum[1], codeNum[2], codeName)
        output += "}\n"

        return output

    def _onQuit(self):
        """
        """
        self._run = False
        print
        print self._generateOutput()
        self._view.destroy()

    def run(self):
        """ Controller main loop
        """
        self._run = True
        while self._run:
            event = self._model['device'].read_one()
            if event is not None:
                try:
                    self._refreshGui(evdev.ecodes.EV[event.type], event.code, event.value)
                except KeyError:
                    Logger().warning("Unknown evdev code")

            self._view.root.update()

            time.sleep(0.01)


def main():
    """
    """

    # Parser options
    parser = argparse.ArgumentParser(description="This tool is used to calibrate servos.")
    parser.add_argument("-l", "--logger",
                        choices=("trace", "debug", "info", "warning", "error", "exception", "critical"), default="info",
                        dest="loggerLevel", metavar="LEVEL",
                        help="logger level")
    parser.add_argument("device",
                        help="device path")

    args = parser.parse_args()

    Logger().setLevel(args.loggerLevel)

    device = evdev.InputDevice(args.device)
    capabilities = device.capabilities(verbose=True, absinfo=True)
    #capabilities.update(
        #{
            #('EV_ABS', 3L): [(('ABS_X', 0L), evdev.AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                             #(('ABS_Y', 1L), evdev.AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                             #(('ABS_RZ', 5L), evdev.AbsInfo(value=0, min=-128, max=127, fuzz=0, flat=15, resolution=0)),
                             #(('ABS_THROTTLE', 6L), evdev.AbsInfo(value=128, min=0, max=255, fuzz=0, flat=15, resolution=0)),
                             #(('ABS_HAT0X', 16L), evdev.AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0)),
                             #(('ABS_HAT0Y', 17L), evdev.AbsInfo(value=0, min=-1, max=1, fuzz=0, flat=0, resolution=0))]
        #})
    pprint.pprint(capabilities)

    # Create Tkinter application
    root = tk.Tk()
    root.title("Gamepad/joystick Calibrator")
    root.resizable(0, 0)

    # Create View
    params = {
        'EV_KEY': collections.OrderedDict(),
        'EV_ABS': collections.OrderedDict(),
        'EV_REL': collections.OrderedDict()
    }
    for key, value in capabilities.items():
        if 'EV_KEY' in key:
            for codeName, codeNum in value:
                if isinstance(codeName, (list, tuple)):
                    codeName = codeName[0]
                params['EV_KEY'][codeName] = int(codeNum)

        elif 'EV_ABS' in key:
            for (codeName, codeNum), absInfo in value:
                if isinstance(codeName, (list, tuple)):
                    codeName = codeName[0]
                params['EV_ABS'][codeName] = (int(codeNum), int(absInfo.min), int(absInfo.max))

        elif 'EV_REL' in key:
            for codeName, codeNum in value:
                if isinstance(codeName, (list, tuple)):
                    codeName = codeName[0]
                params['EV_REL'][codeName] = int(codeNum)
    view = View(root, params)

    # Create controller
    model = {'device': device, 'params': params}
    controller = Controller(model, view)

    controller.run()


if __name__ == '__main__':
    main()
