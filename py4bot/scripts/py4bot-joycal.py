#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

USB device events testing tool

Implements
==========

 - B{main}

Documentation
=============

Usage
=====

$ py4bot-joycal.py /dev/input/by-id/...

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import argparse
import evdev

from py4bot.common.exception import Py4botValueError


def findMinMax(path):
    """
    """
    dev = evdev.InputDevice(path)

    minMax = {}
    for event in dev.read_loop():
        if event.type in (evdev.ecodes.EV_ABS, evdev.ecodes.EV_REL):
            try:
                codeName = evdev.ecodes.bytype[event.type][event.code]
            except KeyError:
                codeName = "?"

            #print "type=%s (%s), code=0x%x (%s), value=0x%x" % \
                #(event.type, evdev.ecodes.EV[event.type], event.code, codeName, event.value)

            try:
                minMax[event.code]
            except KeyError:
                minMax[event.code] = [0xff, -0xff]

            if event.value < minMax[event.code][0]:
                minMax[event.code][0] = event.value
            if event.value > minMax[event.code][1]:
                minMax[event.code][1] = event.value

            print minMax

        elif event.type == evdev.ecodes.EV_KEY and event.value == 1:
            break

    return minMax


def main():
    """
    """

    # Options
    parser = argparse.ArgumentParser(description="This tool is used to calibrate custom USB gamepads/joysticks.")
    parser.add_argument("-n", "--neutral", type=int, default=0x10,
                        help="neutral extension (default to 0x10)")
    parser.add_argument("-w", "--working", type=int, default=0x10,
                        help="working reduction (default to 0x10)")
    parser.add_argument("device", type=str,
                        help="device path")

    # Parse
    args = parser.parse_args()

    # Find neutral min/max values
    print "First step is to find the deadzone at neutral position."
    print
    print "Move the joystick along all axis *without force*!"
    print "Click any joystick button when done..."
    print
    deadzone = findMinMax(args.device)
    print deadzone
    print

    # Find working min/max values
    print "Second step is to find the working zone."
    print
    print "Move the joystick along all axis using all the path."
    print "Click any joystick button when done..."
    print
    working = findMinMax(args.device)
    print working
    print

    # Check results
    if deadzone.keys() != working.keys():
        raise Py4botValueError("Axis keys mismatch; please check config and run again.")

    # Generate settings dict
    print "JOYSTICK_CALIBRATION = {"
    for axis in deadzone.keys():
        print "    %d: (%s+%s, %s-%s, %s+%s, %s-%s)," % (axis,
                                                         hex(int(working[axis][0])),
                                                         hex(args.working),
                                                         hex(int(deadzone[axis][0])),
                                                         hex(args.neutral),
                                                         hex(int(deadzone[axis][1])),
                                                         hex(args.neutral),
                                                         hex(int(working[axis][1])),
                                                         hex(args.working))
        if working[axis][0] + args.working > deadzone[axis][0] - args.neutral or \
           deadzone[axis][0] - args.neutral > deadzone[axis][1] + args.neutral or \
           deadzone[axis][1] + args.neutral > working[axis][1] - args.working:
            raise Py4botValueError("Incompatible intervals; please check config and run again.")
    print "}"


if __name__ == '__main__':
    main()
