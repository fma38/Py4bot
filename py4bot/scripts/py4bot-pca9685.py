#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Low-level servos driver

Implements
==========

 - B{createResponse}
 - B{loop}
 - B{main}

Documentation
=============

PCA9685 low-level driver implementation

Usage
=====

This code is intended to be run as a separate process, to run as fast as possible.
Once launched with correct parameters, it waits for commands from stdin, as json data,
containing something like:

{
    "pulses": {
        "0": 1500,
        "1": 1000,
        ...
    },
    "duration": 1.
}

Synchronized move is performed only if json data contains the 'duration' param; otherwise, a immediat move is done.

TODO
====

 - find a way to give chip as param
 - manage errors

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time
import json
import sys
import argparse
import itertools

from py4bot.common.exception import Py4botHardwareError
from py4bot.services.logger import Logger
from py4bot.hardware.pca9685 import Pca9685, Pca9685Servo
from py4bot.common.response import ResponseFactory


def loop(servos, period, curve):
    """ Main loop

    Wait for incoming datas from stdin (JSON formated), and drive servos accordingly.
    """
    Logger().info("py4bot-pca9685 entering loop...")
    while True:
        try:

            # Wait for input
            data = sys.stdin.readline().strip()
            if not data:
                break
            Logger().debug("py4bot-pca9685.loop(): data=%s" % data)

            # Decode json
            cmd = json.loads(data)

            # Move sync
            if cmd.has_key('duration'):
                table = {'nums': [], 'responses': []}
                for num, pulse in cmd['pulses'].items():
                    if servos[int(num)].pulse:
                        start = servos[int(num)].pulse
                    else:
                        start = pulse
                    end = pulse
                    response = ResponseFactory().create(curve, start, end, cmd['duration'])
                    table['nums'].append(int(num))
                    table['responses'].append(response)

                startTime = time.time()
                for pulses in itertools.izip(*table['responses']):
                    #Logger().debug("py4bot-pca9685.loop(): %.3fs -> %d" % (time.time() - startTime, int(round(pulses[0]))))
                    Logger().debug("%.3fs %d" % (time.time() - startTime, int(round(pulses[0]))))
                    for i, pulse in enumerate(pulses):
                        servos[table['nums'][i]].pulse = int(round(pulse))

            # Move immediate
            else:
                for num, pulse in cmd['pulses'].items():
                    servos[int(num)].pulse = pulse

            # Send back reached positions
            retData = {'pulses': {}}
            for num in cmd['pulses'].keys():
                retData['pulses'][int(num)] = servos[int(num)].pulse
            retData.update({'status': "ok"})
            sys.stdout.write("%s\n" % json.dumps(retData))
            sys.stdout.flush()

        except KeyboardInterrupt:
            break

        except:
            Logger().exception("py4bot-pca9685.loop()")

    Logger().info("py4bot-pca9685 loop terminated")


def main():
    """
    """
    def hexdec(value):
        """
        """
        try:
            return str(int(value))
        except ValueError:
            return str(int(value, 16))

    # Parser options
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--logger",
                        choices=("trace", "debug", "info", "warning", "error", "exception", "critical"), default="info",
                        dest="loggerLevel", metavar="LEVEL",
                        help="logger level")
    parser.add_argument("-t", "--target",
                        choices=("real", "fake"), default="fake",
                        help="target to run I²C backend")
    parser.add_argument("-b", "--bus",
                        type=int, default=-1,
                        help="I²C bus to use")
    parser.add_argument("-f", "--frequency",
                        type=float, default=50.,
                        help="PWM frequency, in Hz")
    parser.add_argument("-c", "--curve",
                        choices=("linear", "smooth", "damped", "accel"), default="linear",
                        help="response curve")
    parser.add_argument("-p", "--period",
                        type=float, default=0.02,
                        help="response curve (min) period, in s")
    parser.add_argument("address",
                        type=hexdec, nargs='+',
                        help="I²C address of PCA 9685 chips")
    args = parser.parse_args()

    Logger().setLevel(args.loggerLevel)

    # Create I²C bus
    if args.target == "real":
        from py4bot.hardware.i2cBus import I2cBus
    elif args.target == "fake":
        from py4bot.hardware.i2cBus import I2cFakeBus as I2cBus
    else:
        raise Py4botHardwareError("Unsupported target (%s)" % args.target)

    i2cBus = I2cBus(args.bus)

    # Create servos (I²C outputs)
    servos = []  # use pool!
    for address in args.address:
        try:
            pca9685 = Pca9685(i2cBus, int(address))
        except ValueError:
            pca9685 = Pca9685(i2cBus, int(address, 16))  # address is given as hex value
        pca9685.setFrequency(args.frequency)

        timePerClick = 1e6 / args.frequency / 4096  # use µs
        for channel in range(Pca9685.NB_OUTPUT):
            servo = Pca9685Servo(pca9685, channel, timePerClick)
            servos.append(servo)

    # Launch loop
    loop(servos, args.period, args.curve)


if __name__ == '__main__':
    main()
