#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

USB device events testing tool

Implements
==========

 - B{main}

Documentation
=============

Usage
=====

$ evdev-test.py /dev/input/by-id/usb-Microsoft_Microsoft®_Digital_Media_Keyboard_3000-event-kbd

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""


import pprint

import argparse
import evdev


def main():

    # Options
    parser = argparse.ArgumentParser(description="This tool is used to test events of USB input devices.")
    parser.add_argument("-r", "--read", action="store_true", default=False,
                        help="continuously read keys and print keycodes.")
    parser.add_argument("device", type=str,
                        help="device path")

    # Parse
    args = parser.parse_args()

    dev = evdev.InputDevice(args.device)
    pprint.pprint(dev.capabilities(verbose=True, absinfo=True))
    #print dev.active_keys(verbose=True)

    if args.read:
        for event in dev.read_loop():
            if event.type == evdev.ecodes.EV_SYN:
                if event.code == evdev.ecodes.SYN_MT_REPORT:
                    print "time=%.6f +++ event=%s" % (event.timestamp(), evdev.ecodes.SYN[event.code])
                else:
                    print "time=%.6f --- event=%s" % (event.timestamp(), evdev.ecodes.SYN[event.code])
            else:
                try:
                    codeName = evdev.ecodes.bytype[event.type][event.code]
                except KeyError:
                    codeName = "?"

                print "time=%.6f, type=(%s, %s), code=(0x%x, %s), value=%d" % \
                    (event.timestamp(), event.type, evdev.ecodes.EV[event.type], event.code, codeName, event.value)


if __name__ == '__main__':
    main()
