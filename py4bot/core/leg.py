# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Leg handling

Implements
==========

 - B{Leg}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import math

import numpy

from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger


class Leg(object):
    """

    @ivar _footP: foot position, relative to neutral foot position, in ground space
    @type _footP: numpy.matrix
    """
    def __init__(self, index, joints, footNeutral):
        """

        @param index: leg index
        @type index: str

        @param joints: joints
        @type joints: dict of L{Joint<joints>}

        @param footNeutral: distance between foot and coxa join, in ground plane
        @type footNeutral: float
        """
        super(Leg, self).__init__()

        self._index = index
        self._joints = joints
        self._footNeutral = footNeutral

        self._defaultFootNeutral = footNeutral
        self._newFootNeutral = False
        self._footP = numpy.matrix([[0.],
                                    [0.],
                                    [0.]])

    @property
    def index(self):
        return self._index

    @property
    def footP(self):
        return self._footP

    @footP.setter
    def footP(self, P):
        self._footP = P

    @property
    def footNeutral(self):
        self._newFootNeutral = False
        return self._footNeutral

    @footNeutral.setter
    def footNeutral(self, footNeutral):
        self._newFootNeutral = True
        self._footNeutral = footNeutral

    @property
    def newFootNeutral(self):
        return self._newFootNeutral

    def reset(self):
        """ Reset leg position
        """
        self._footNeutral = self._defaultFootNeutral
        self._newFootNeutral = False
        self._footP = numpy.matrix([[0.],
                                    [0.],
                                    [0.]])

    def setJointsPositions(self, positions):
        """

        @param positions:
        @type positions: dict
        """
        raise NotImplementedError
