# -*- coding: utf-8 -*-

""" Robot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Robot management

Implements
==========

 - B{Robot}

Documentation
=============

Instead of inherit and overload _createXXX(), use some addXXX() methods to set legs, driver...

Have a real robot, and a robot for computation?

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time
import copy

import numpy

from py4bot.common import config
from py4bot.common.exception import Py4botError, Py4botIKError, Py4botInterrupt
from py4bot.common.signal import Signal
from py4bot.services.logger import Logger
from py4bot.core.body import Body
from py4bot.gaits.gaitSequencer import GaitSequencer


class Robot(object):
    """ Robot class
    """
    def __init__(self, *args, **kwargs):
        """ Init Robot object
        """
        super(Robot, self).__init__()

        self._args = args
        self._kwargs = kwargs

        # Create body
        self._body = self._createBody()

        # Create legs and IK
        self._legs, self._legIk = self._createLegs()

        # Create actuator pool
        self._actuatorPool =  self._createActuatorPool()

        # Link gait sequencer
        GaitSequencer().updateWalkSignal.connect(self._updateWalkSlot)

        #self.reset()

        # Init feet neutral positions in robot coordinates space
        self._feetNeutralP = {}
        for leg in self._legs.values():
            self._computeFootNeutralP(leg)

    @property
    def body(self):
        return self._body

    @property
    def legs(self):
        return self._legs

    @property
    def feetNeutralP(self):
        return self._feetNeutralP

    def _computeFootNeutralP(self, leg):
        """ Compute foot neutral position in robot coordinates space
        """
        footNeutralP = numpy.matrix([[leg.footNeutral],
                                     [0.],
                                     [0.]])
        legOriginR, legOriginT = self._body.legsOrigin[leg.index]
        self._feetNeutralP[leg.index] = legOriginR * footNeutralP + legOriginT

    def _createBody(self):
        """ Create robot body
        """
        raise NotImplementedError

    def _createLegs(self):
        """ Create legs and IK
        """
        raise NotImplementedError

    def _createActuatorPool(self):
        """ Create actuator pool
        """
        raise NotImplementedError

    def _updateIK(self):
        """ Compute IK

        For each leg, we compute the foot position in the leg coordinates system

        @todo: manage neutral change -> apply only on legs up
        """
        Logger().trace("Robot._updateIk()")

        for leg in self._legs.values():
            try:
                #Logger().debug("Robot._updateIk(): leg '%s' move to (x=%+5.1f, y=%+5.1f, z=%+5.1f)" % (leg.index, leg.footP[0], leg.footP[1], leg.footP[2]))

                if leg.newFootNeutral and leg.footP[2]:  # the leg lifts up
                    self._computeFootNeutralP(leg)

                footNeutralP = self._feetNeutralP[leg.index]
                #if leg.index == 'RR':
                    #Logger().debug("Robot._updateIk(): leg '%s' neutral is (x=%+5.1f, y=%+5.1f, z=%+5.1f)" % (leg.index, footNeutralP[0], footNeutralP[1], footNeutralP[2]))
                    #Logger().debug("Robot._updateIk(): leg.footP is (x=%+5.1f, y=%+5.1f, z=%+5.1f)" % (leg.footP[0], leg.footP[1], leg.footP[2]))

                # Foot position in ground coordinates system
                footP = leg.footP + footNeutralP

                # Change to body coordinates system
                bodyR = self._body.extra.R * self._body.R
                bodyP = self._body.P + self._body.extra.P
                footP = bodyR.T * (footP - bodyP)  # .I == .T in a rotation matrix (.T is much faster)
                #if leg.index == 'RR':
                    #Logger().debug("Robot._updateIk(): footP in body ref. is (x=%+5.1f, y=%+5.1f, z=%+5.1f)" % (footP[0], footP[1], footP[2]))

                # Leg origin rotation matrix and translation vector
                legOriginR, legOriginP = self._body.legsOrigin[leg.index]

                # Change to leg coordinates system
                footP = legOriginR.T * (footP - legOriginP)  # .I == .T in a rotation matrix (.T is much faster)
                #if leg.index == 'RR':
                    #Logger().debug("Robot._updateIk(): footP in leg ref. is (x'=%+5.1f, y'=%+5.1f, z=%+5.1f)" % (footP[0], footP[1], footP[2]))

                # Compute leg joints positions, solving IK
                jointsPositions = self._legIk[leg.index].solve(footP)
                #if leg.index == 'RR':
                    #Logger().debug("Robot._updateIk(): leg index=%s, jointsPositions=%s" % (leg.index, jointsPositions))

                # Store new leg joints positions
                leg.setJointsPositions(jointsPositions)

            except ValueError:
                Logger().exception("Robot._updateIk()", debug=True)
                raise Py4botIKError

    def _updateWalkSlot(self, walkFeetTarget, duration):
        """ Callback for walking

        Note: walkFeetTarget is relative to neutral, in foot-centered floor space

        @todo: refactor _updateWalkSlot/_updateIk
        """
        Logger().trace("Robot._updateWalkSlot()")
        #Logger().debug("Robot._updateWalkSlot(): walkFeetTarget=%s, duration=%.3f" % (walkFeetTarget, duration))

        # Reset body extra position
        self._body.extra.reset()

        # Save current legs positions
        #legs = copy.deepcopy(self._legs)

        # Store target legs positions as current
        for legIndex, (footR, footT) in walkFeetTarget.items():
            footN = self.feetNeutralP[legIndex]
            footP = footR * (footN + footT) - footN
            self._legs[legIndex].footP = footP

        # Compute and move to new position
        try:
            self._updateIK()
            self._actuatorPool.moveSync(duration)

        except Py4botIKError:
            #Logger().warning("IK error; skipping position")

            # Restore previous legs positions
            #self._legs = copy.deepcopy(legs)  # does not work.... !!!!
            raise

    #def reset(self):
        #""" Reset robot position

        #@todo: only in idle state; may need to cycle if legs change
        #"""
        #Logger().trace("Robot.reset()")

        ## Set neutral body position
        #self._body.reset()

        ## Set neutral leg position
        #for leg in self._legs.values():
            #leg.reset()

        #self._updateIK()
        #self._actuatorPool.move()

    #def resetBody(self):
        #""" Reset body position
        #"""

        ## Set neutral body position
        #self._body.reset()

        #self._updateIK()
        #self._actuatorPool.move()

    #def resetLegs(self):
        #""" Reset body position
        #"""

        ## Set neutral leg position
        #for leg in self._legs.values():
            #leg.reset()

        #self._updateIK()
        #self._actuatorPool.move()

    def setBodyPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None, staggeringDelay=0.):
        """ Set a new body position without changing legs position on floor
        """
        if GaitSequencer().state not in ('idle', 'pause'):
            Logger().warning("Can't set body position while walking")

        else:

            # Save current body position
            # TODO: use something like:
            #       with Backup(self._body) as body:
            body = copy.deepcopy(self._body)

            if x is not None:
                self._body.x = x
            if y is not None:
                self._body.y = y
            if z is not None:
                self._body.z = z
            if yaw is not None:
                self._body.yaw = yaw
            if pitch is not None:
                self._body.pitch = pitch
            if roll is not None:
                self._body.roll = roll
            Logger().debug("Robot.setBodyPosition(): x=%.1f, y=%.1f, z=%.1f, yaw=%.1f, pitch=%.1f, roll=%.1f" %
                           (self._body.x, self._body.y, self._body.z, self._body.yaw, self._body.pitch, self._body.roll))

            try:
                self._updateIK()
                if staggeringDelay:
                    self._actuatorPool.move(staggeringDelay)
                else:
                    self._actuatorPool.moveSync(0.5)

            except Py4botIKError:
                Logger().warning("IK error; skipping body moveement")

                # Restore previous body position
                self._body = body

    def incBodyPosition(self, dx=0., dy=0., dz=0., dyaw=0., dpitch=0., droll=0.):
        """ Increment body position.

        Can be used while walking, to adjust body position/orientation.

        Does not work well: legs on floor slip...
        -> need to memorize previous body position in gait computation (like for feet neutral position)?
        """

        # Save current body position
        body = copy.deepcopy(self._body)

        self._body.x += dx
        self._body.y += dy
        self._body.z += dz
        self._body.yaw += dyaw
        self._body.pitch += dpitch
        self._body.roll += droll
        Logger().debug("Robot.incBodyPosition(): x=%.1f, y=%.1f, z=%.1f, yaw=%.1f, pitch=%.1f, roll=%.1f" %
                       (self._body.x, self._body.y, self._body.z, self._body.yaw, self._body.pitch, self._body.roll))

        if GaitSequencer().state in ('idle', 'pause'):
            try:
                self._updateIK()
                self._actuatorPool.move()

            except Py4botIKError:
                Logger().warning("IK error; skipping body movement")

                # Restore previous body position
                self._body = body

    def setBodyExtraPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None):
        """ Set a new body extra position without changing legs position on floor

        Use this method when you want to change the body position arround the current body position,
        whithout loosing it (set extra position to 0 to go back to previous body position).

        Bind this method to a joystick for continuous move of the body.

        @todo: only if not walking
        """
        if GaitSequencer().state not in ('idle', 'pause'):
            Logger().warning("Can't set body extra position while walking")

        else:

            # Save current body position
            body = copy.deepcopy(self._body)

            if x is not None:
                self._body.extra.x = x
            if y is not None:
                self._body.extra.y = y
            if z is not None:
                self._body.extra.z = z
            if yaw is not None:
                self._body.extra.yaw = yaw
            if pitch is not None:
                self._body.extra.pitch = pitch
            if roll is not None:
                self._body.extra.roll = roll
            Logger().debug("Robot.setBodyExtraPosition(): x=%.1f, y=%.1f, z=%.1f, yaw=%.1f, pitch=%.1f, roll=%.1f" %
                           (self._body.extra.x, self._body.extra.y, self._body.extra.z,
                            self._body.extra.yaw, self._body.extra.pitch, self._body.extra.roll))

            try:
                self._updateIK()
                self._actuatorPool.move()

            except Py4botIKError:
                Logger().warning("IK error; skipping body movement")

                # Restore previous body position
                self._body = body

    def incFeetNeutral(self, dneutral):
        """ Increment neutral feet position

        Neutral feet position is more or less the distance of the feet from the center of the robot.
        For now, it is not possible to set distance for individual feet.
        """
        if GaitSequencer().state == 'pause':
            Logger().warning("Can't set neutral feet position when paused")

        else:
            for leg in self._legs.values():
                leg.footNeutral += dneutral
            if GaitSequencer().state == 'idle':
                GaitSequencer().walkStatic()

    def mainLoopStep(self):
        """

        Can be overriden to do more usefull things!
        """
        time.sleep(0.01)

    def mainLoop(self, duration=None):
        """ Main robot loop

        Can be overriden to do more usefull things!
        """
        Logger().info("Entering robot main loop")
        if duration is not None:
            start = time.time()
        while True:
            try:
                self.mainLoopStep()

                if duration is not None:
                    if time.time() - start > duration:
                        raise Py4botInterrupt

            except (KeyboardInterrupt, Py4botInterrupt):
                Logger().info("Exiting robot main loop")
                break

            except:
                Logger().exception("robot.mainLoop()")























#class ModeLegMove(Mode):
    #""" Move leg mode

    #In this mode, we only move the legs.
    #"""
    #def __init__(self, robot):
        #super(ModeLegMove, self).__init__("leg_move", robot)

        #self._buttonsMask = (0, 0, 0, 0, 0, 0, '+', '+', 0, 0, 0, 0)
        #self._axesMask = (0, 1, 1, 1, 0, 0)

        #self._selectedLeg = 0

    #def _handleInput(self, buttons, axes):
        #"""
        #@todo: addd body Z management
        #"""
        #Logger().trace("ModeLegMove.handleInput()")

        ## Leg selection
        #if buttons[6] and not buttons[7]:

            ## Put current leg back to neutral position
            #self._move(config.LEGS_INDEX[self._selectedLeg])

            #self._selectedLeg += 1
            #if self._selectedLeg >= len(config.LEGS_INDEX):
                #self._selectedLeg = 0

        #elif buttons[7] and not buttons[6]:

            ## Put current leg back to neutral position
            #self._move(config.LEGS_INDEX[self._selectedLeg])

            #self._selectedLeg -= 1
            #if self._selectedLeg < 0:
                #self._selectedLeg = len(config.LEGS_INDEX) - 1

        ## Get leg position
        #x = axes[2] * 40.
        #y = axes[3] * 40.
        #z = -axes[1] * 40. - 10.

        #footP = numpy.matrix([[x],
                             #[y],
                             #[z]])
        #self._move(config.LEGS_INDEX[self._selectedLeg], footP)

    #def _move(self, legIndex, footP=None):
        #""" Move leg at index to given position
        #"""
        #if footP is None:
            #footP = numpy.matrix([[0.],
                                 #[0.],
                                 #[0.]])
        #footPosition = {legIndex: footP}

        #try:
            #self._robot.updateIk(0.1, footPosition)
        #except Py4botIKError:
            #Logger().warning("IK error; skipping position")

    #def enter(self):

        ## Lift up  current leg

        #footP = numpy.matrix([[0],
                             #[0],
                             #[-10.]])
        #self._move(config.LEGS_INDEX[self._selectedLeg], footP)

    #def leave(self):

        ## Put current leg back to neutral position
        #self._move(config.LEGS_INDEX[self._selectedLeg])






#class ModeBodyMove(Mode):
    #""" Move body mode

    #In this mode, we only move the body.
    #"""
    #def __init__(self, robot):
        #super(ModeBodyMove, self).__init__("body_move", robot)

        #self._buttonsMask = (0, 0, 0, 0, '+', '+', 0, 0, 0, 0, 0, 0)
        #self._axesMask = (1, 1, 1, 0, 1, 1)

    #def _handleInput(self, buttons, axes):
        #"""
        #@todo: first call parent for masks, fronts...
        #"""
        #Logger().trace("ModeBodyMove.handleInput()")

        #body = Body()
        #body.copyFrom(self._robot.body)

        ## Translations
        #body.x -= axes[4] * 10
        #body.y -= axes[5] * 10
        #if buttons[4] and not buttons[5]:
            #body.z += 10
        #elif buttons[5] and not buttons[4]:
            #body.z -= 10

        ## Rotations
        #body.yaw = axes[2] * 30.
        #body.pitch = -axes[1] * 30.
        #body.roll = axes[0] * 30.

        #if body != self._robot.body:
            #Logger().debug("ModeBodyMove.handleInput(): body target pos=%s" % repr((body.x, body.y, body.z, body.yaw, body.pitch, body.roll)))
            #try:
                #self._robot.updateIk(0.1, bodyTarget=body)
            #except Py4botIKError:
                #Logger().warning("IK error; skipping position")
