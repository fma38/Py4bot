# -*- coding: utf-8 -*-

""" Robot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Robot 3D management

Implements
==========

 - B{Body3D}

Documentation
=============

This class is for 3D simulation of a robot, based on VPython.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import math

import numpy
import visual

from py4bot.services.logger import Logger
from py4bot.common.rotations import Rx, Ry, Rz, matrixToAxisAngle


class Body3D(visual.frame):
    """
    """
    def __init__(self):
        """
        """
        super(Body3D, self).__init__()

        self._pos = {'x': 0., 'y': 0., 'z': 0., 'yaw': 0., 'pitch': 0., 'roll': 0.}
        self._yaw = 0.
        self._pitch = 0.
        self._roll = 0.
        self._angle = 0.
        self._axis = None

        self._build3D()

    def _build3D(self):
        visual.box(frame=self, axis=(0, 1, 0), length=5, width=50, height=30, color=visual.color.yellow)

    def _translation(self, x=None, y=None, z=None):
        """
        """
        if x is not None:
            self.x = x
        if y is not None:
            self.z = -y
        if z is not None:
            self.y = z

    def _rotation(self, yaw=None, pitch=None, roll=None):
        """
        """
        if yaw is not None:
            self._yaw = yaw
        if roll is not None:
            self._roll = roll
        if pitch is not None:
            self._pitch = pitch

        R = Rz(self._yaw) * Ry(self._roll) * Rx(self._pitch)
        try:
            axis, angle = matrixToAxisAngle(R)

            # Revert previous rotation
            if self._axis is not None:
                self.rotate(angle=-self._angle, axis=(self._axis[0], self._axis[2], -self._axis[1]))

            # New rotation
            self.rotate(angle=angle, axis=(axis[0], axis[2], -axis[1]))
            self._angle = angle
            self._axis = axis

        except ZeroDivisionError:
            #Logger().exception("Body3D._rotation()", debug=True)

            # Revert previous rotation
            if self._axis is not None:
                self.rotate(angle=-self._angle, axis=(self._axis[0], self._axis[2], -self._axis[1]))
                self._axis = None

    def setPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None):
        """
        """
        if x is not None:
            self._pos['x'] = x
        if y is not None:
            self._pos['y'] = y
        if z is not None:
            self._pos['z'] = z
        if yaw is not None:
            self._pos['yaw'] = yaw
        if pitch is not None:
            self._pos['pitch'] = pitch
        if roll is not None:
            self._pos['roll'] = roll

        self._translation(self._pos['x'], self._pos['y'], self._pos['z'])
        self._rotation(self._pos['yaw'], self._pos['pitch'], self._pos['roll'])

    def incPosition(self, dx=0., dy=0., dz=0., dyaw=0., dpitch=0., droll=0.):
        """
        """
        self._pos['x'] += dx
        self._pos['y'] += dy
        self._pos['z'] += dz
        self._pos['yaw'] += dyaw
        self._pos['pitch'] += dpitch
        self._pos['roll'] += droll

        self._translation(self._pos['x'], self._pos['y'], self._pos['z'])
        self._rotation(self._pos['yaw'], self._pos['pitch'], self._pos['roll'])

    def setExtraPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None):
        """
        """
        if x is not None:
            x += self._pos['x']
        if y is not None:
            y += self._pos['y']
        if z is not None:
            z += self._pos['z']
        if yaw is not None:
            yaw += self._pos['yaw']
        if pitch is not None:
            pitch += self._pos['pitch']
        if roll is not None:
            roll += self._pos['roll']

        self._translation(x, y, z)
        self._rotation(yaw, pitch, roll)
