# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Leg joints

Implements
==========

 - B{Joint}
 - B{Coxa}
 - B{Femur}
 - B{Tibia}
  -B{Tars}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""


class Joint(object):
    """
    """
    def __init__(self, name):
        """
        """
        super(Joint, self).__init__()

        self._name = name

        self._angle = None

    @property
    def name(self):
        return self._name

    @property
    def angle(self):
        return self._angle

    @angle.setter
    def angle(self, angle):
        self._angle = angle


class Coxa(Joint):
    """
    """
    def __init__(self):
        """
        """
        super(Coxa, self).__init__('coxa')

    @property
    def gamma(self):
        return self._angle

    @gamma.setter
    def gamma(self, gamma):
        self._angle = gamma


class Femur(Joint):
    """
    """
    def __init__(self):
        """
        """
        super(Femur, self).__init__('femur')

    @property
    def alpha(self):
        return self._angle

    @alpha.setter
    def alpha(self, alpha):
        self._angle = alpha


class Tibia(Joint):
    """
    """
    def __init__(self):
        """
        """
        super(Tibia, self).__init__('tibia')

    @property
    def beta(self):
        return self._angle

    @beta.setter
    def beta(self, beta):
        self._angle = beta


class Tars(Joint):
    """
    """
    def __init__(self):
        """
        """
        super(Tars, self).__init__('tars')

    @property
    def kappa(self):
        return self._angle

    @kappa.setter
    def kappa(self, kappa):
        self._angle = kappa
