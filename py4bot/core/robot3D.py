# -*- coding: utf-8 -*-

""" Robot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Robot 3D management

Implements
==========

 - B{Robot3D}

Documentation
=============

This class is for 3D simulation of a robot, based on VPython.

Usage
=====

TODO
====

The body should not move immediatly, but only when the IK is updated.

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import visual

from py4bot.core.robot import Robot
from py4bot.core.body3D import Body3D


class Robot3D(Robot):
    """ Robot 3D class
    """
    def __init__(self, *args, **kwargs):
        """

        @todo: add a _init() method for additionnal inits
        """
        self._body3D = self._createBody3D()

        super(Robot3D, self).__init__(*args, **kwargs)

    def _createBody3D(self):
        """
        """
        return Body3D()

    def setBodyPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None, staggeringDelay=0.):
        super(Robot3D, self).setBodyPosition(x=x, y=y, z=z, yaw=yaw, pitch=pitch, roll=roll, staggeringDelay=staggeringDelay)

        self._body3D.setPosition(x=x, y=y, z=z, yaw=yaw, pitch=pitch, roll=roll)

    def incBodyPosition(self, dx=0., dy=0., dz=0., dyaw=0., dpitch=0., droll=0.):
        super(Robot3D, self).incBodyPosition(dx=dx, dy=dy, dz=dz, dyaw=dyaw, dpitch=dpitch, droll=droll)

        self._body3D.incPosition(dx=dx, dy=dy, dz=dz, dyaw=dyaw, dpitch=dpitch, droll=droll)

    def setBodyExtraPosition(self, x=None, y=None, z=None, yaw=None, pitch=None, roll=None):
        super(Robot3D, self).setBodyExtraPosition(x=x, y=y, z=z, yaw=yaw, pitch=pitch, roll=roll)

        self._body3D.setExtraPosition(x=x, y=y, z=z, yaw=yaw, pitch=pitch, roll=roll)

    def mainLoopStep(self):
        """ VPython 6 stuff
        """
        visual.rate(25)
