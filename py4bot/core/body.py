# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WAANTY; without even the implied warranty of
MECHANTABILITY or FITNESS FO A PATICULA PUPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Body main computation

Implements
==========

 - B{Body}

Documentation
=============

Usage
=====

TODO
====

 - add a body offset

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import copy

import numpy

from py4bot.common import config
from py4bot.common.rotations import Rx, Ry, Rz


class BodyBase(object):
    """ Body base robot object
    """
    def __init__(self):
        """
        """
        super(BodyBase, self).__init__()

        # Init body position
        self._P = numpy.matrix([[0.],
                                [0.],
                                [0.]])
        self._yaw = 0.
        self._pitch = 0.
        self._roll = 0.
        self._R = numpy.matrix(numpy.identity(3))

    def __eq__(self, other):
        if (self._P == other._P).all() and \
           self._yaw == other._yaw and \
           self._pitch == other._pitch and \
           self._roll == other._roll:
            return True
        else:
            return False

    @property
    def P(self):
        return self._P

    @P.setter
    def P(self, P):
        self._P = P

    @property
    def x(self):
        return self._P[0]

    @x.setter
    def x(self, x):
        self._P[0] = x

    @property
    def y(self):
        return self._P[1]

    @y.setter
    def y(self, y):
        self._P[1] = y

    @property
    def z(self):
        return self._P[2]

    @z.setter
    def z(self, z):
        self._P[2] = z

    @property
    def yaw(self):
        return self._yaw

    @yaw.setter
    def yaw(self, yaw):
        self._yaw = yaw
        self._updateR()

    @property
    def pitch(self):
        return self._pitch

    @pitch.setter
    def pitch(self, pitch):
        self._pitch = pitch
        self._updateR()

    @property
    def roll(self):
        return self._roll

    @roll.setter
    def roll(self, roll):
        self._roll = roll
        self._updateR()

    @property
    def R(self):
        return self._R

    @R.setter
    def R(self, R):
        self._R = R

    def _updateR(self):
        """
        """
        self._R = Rz(self._yaw) * Ry(self._roll) * Rx(self._pitch)

    def reset(self):
        """ Reset body position
        """
        self._P = numpy.matrix([[0.],
                                [0.],
                                [0.]])
        self._yaw = 0.
        self._pitch = 0.
        self._roll = 0.
        self._updateR()


class CompositeBody(BodyBase):
    """ Body robot object
    """
    def __init__(self):
        """
        """
        super(CompositeBody, self).__init__()

        # Body extra
        self._extra = BodyBase()

    def __eq__(self, other):
        if super(CompositeBody, self).__eq__(other) and \
           self._extra == other._extra:
            return True
        else:
            return False

    @property
    def extra(self):
        return self._extra

    def reset(self):
        super(CompositeBody, self).reset()
        self._extra.reset()


class Body(CompositeBody):
    """
    """
    def __init__(self, legsOrigin):
        """
        """
        super(Body, self).__init__()

        # Init legs origin rotation matrix and translation vector
        self._legsOrigin = {}
        for legIndex, legOrigin in legsOrigin.items():
            legsOriginR = Rz(legOrigin['gamma0'])
            legsOriginP = numpy.matrix([[legOrigin['x']],
                                        [legOrigin['y']],
                                        [0.]])
            self._legsOrigin[legIndex] = (legsOriginR, legsOriginP)

    @property
    def legsOrigin(self):
        return self._legsOrigin


if __name__ == '__main__':
    import unittest
    import copy

    class BodyTestCase(unittest.TestCase):

        def setUp(self):
            self.body1 = Body({})
            self.body2 = copy.deepcopy(self.body1)

        def test_eq_0(self):
            self.assertEqual(self.body1, self.body2)

        def test_eq_1(self):
            self.body2.P[0] = 1
            self.assertNotEqual(self.body1, self.body2)

        def test_eq_2(self):
            self.body2.extra.P[0] = 1
            self.assertNotEqual(self.body1, self.body2)

    unittest.main()
