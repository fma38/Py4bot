# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{PololuSerial}

Documentation
=============

Usage
=====


@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import struct
import time
import threading

import serial

from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver

NB_SERVOS = 8
CMD_SET_PARAMETERS = 0
CMD_SET_SPEED = 1
CMD_SET_ABSOLUTE_POSITION = 4
SPEED_MIN = 50
SPEED_MAX = 6350
POS_MIN = 500
POS_MAX = 5500


class PololuSerial(Driver):
    """ Pololu Serial driver implementation

    This object generates code for the Pololu Serial servo controller.
    See U{https://www.pololu.com/file/0J38/ssc04a_guide.pdf}
    """
    def __init__(self, port="/dev/ttyUSB0", baudrate=9600):
        """ Init Pololu Serial driver
        """
        super(PololuSerial, self).__init__()

        self._serial = serial.Serial(port=port, baudrate=baudrate)
        self._serial.setRTS(0)
        self._serial.setDTR(0)

        self._servoPositions = NB_SERVOS * [1500]

        self._lock = threading.RLock()

    def _sendCmd(self, command, servo, data1, data2=None):
        """ Low-level send command
        """
        Logger().debug("PololuSerial._sendCmd(): command=%d, servo=%d, data1=%s, data2=%s" % (command, servo, data1, data2))

        self._lock.acquire()
        try:
            if data2 is not None:
                data2Str = hex(data2)
            else:
                data2Str = 'None'
            #Logger().debug("PololuSerial._sendCmd(): command=%d, servo=%d, data1=%s, data2=%s" % (command, servo, hex(data1), data2Str))
            if command in (0, 1, 2):
                if data2 is not None:
                    raise ValueError("Command %d takes only 1 data parameter" % command)
                else:
                    self._serial.write(struct.pack("BBBBB", 0x80, 0x01, command, servo, data1))
            elif command in (3, 4, 5):
                if data2 is None:
                    raise ValueError("Command %d takes 2 data parameters" % command)
                else:
                    self._serial.write(struct.pack("BBBBBB", 0x80, 0x01, command, servo, data1, data2))
            else:
                raise ValueError("command must be in [0-5]")

            # Purge buffer
            time.sleep(0.1)
            Logger().debug("PololuSeria._sendCmd(): pololu returned: ", repr(self._serial.read(self._serial.inWaiting())))

        finally:
            self._lock.release()

    def _setParameters(self, servo, on=True, direction='forward', range=15):
        """ Set parameters of module
        """
        if direction not in ('forward', 'reverse'):
            raise Py4botValueError("direction must be in ('forward', 'reverse')")

        data1 = on << 6 | (direction != 'forward') << 5 | (range & 31)
        #Logger().debug("PololuSeria.setParameters(): data1=%s" % hex(data1))
        self._sendCmd(CMD_SET_PARAMETERS, servo, data1)

    def _setSpeed(self, servo, speed):
        """ Set servo speed

        @param servo: servo num, in range [0-7]
        @type servo: int

        @param speed: servo speed, in range [50-6350] (µs/s)
                      Note that speed is rounded as multiples of 50µs/s
        @type speed: int or float
        """
        if not SPEED_MIN <= speed <= SPEED_MAX:
            raise Py4botValueError("speed must be in [%d-%d]" % (SPEED_MIN, SPEED_MAX))

        speed = int(round(speed / 50., ndigits=0))
        self._sendCmd(CMD_SET_SPEED, servo, speed)

    def _setPositionAbsolute(self, servo, position):
        """ Set servo position, in us
        """
        if not POS_MIN <= position <= POS_MAX:
            raise Py4botValueError("position must be in [%d-%d]" % (POS_MIN, POS_MAX))

        data1 = int(position*2) / 128  # !!!!
        data2 = int(position) % 128
        self._sendCmd(CMD_SET_ABSOLUTE_POSITION, servo, data1, data2)

    def move(self, servos, staggeringDelay):
        Logger().trace("PololuSerial.move()")

        for servo in servos:

            # We first set the speed of each serv (here, full speed)
            if self._servoPositions[servo.num]:
                self._setSpeed(servo.num, SPEED_MAX)

            # Then we send the new position
            self._setPositionAbsolute(servo.num, servo.pulse)

            # And save that position
            self._servoPositions[servo.num] = servo.pulse

            if staggeringDelay:
                time.sleep(staggeringDelay)

    def moveSync(self, servos, duration):
        Logger().debug("PololuSerial.moveSync(): duration=%.3f" % duration)

        for servo in servos:

            # We first need to compute the speed of each servo
            if self._servoPositions[servo.num]:
                speed = abs(servo.pulse - self._servoPositions[servo.num]) / float(duration)
                self._setSpeed(servo.num, speed)

            # Then we send the new position
            self._setPositionAbsolute(servo.num, servo.pulse)

            # And save that position
            self._servoPositions[servo.num] = servo.pulse
