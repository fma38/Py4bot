# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Driver base implementation

Implements
==========

 - B{Driver}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time

from py4bot.services.logger import Logger


class Driver(object):
    """ Driver base object
    """
    def __init__(self):
        """ Init Driver object
        """
        super(Driver, self).__init__()

        self._moveSyncDuration = 0.
        self._moveSyncStart = time.time()

    def waitEndOfSyncMove(self):
        """ Wait previous move to end
        """
        while time.time() - self._moveSyncStart < self._moveSyncDuration:
            time.sleep(0.01)

    def move(self, actuators, staggeringDelay=0.):
        """ Move actuators

        @param actuators: actuators to move
        @type actuators: dict

        @param staggeringDelay: delay between each servo move, in s
                                usefull for first move to avoid all servos moving at the same time
        @type staggeringDelay: float
        """
        raise NotImplementedError

    def moveSync(self, actuators, duration=0):
        """ Move actuators synchrone

        @param actuators: actuators to move
        @type actuators: dict

        @param duration: time for the synchrone move
        @type duration: float
        """
        Logger().trace("Driver.moveSync()")
        #Logger().debug("Driver.moveSync(): actuators=%s, duration=%.3f" % ([str(actuator) for actuator in actuators], duration))

        self.move(actuators)
        self._moveSyncStart = time.time()
        self._moveSyncDuration = duration
