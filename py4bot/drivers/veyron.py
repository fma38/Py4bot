# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{Veyron}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time
import threading

import serial

from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver


class Veyron(Driver):
    """ Veyron driver implementation

    This object generates code for the DFRobot Veyron Servo Controller.
    See U{http://www.dfrobot.com/wiki/index.php/Veyron_Servo_Driver_(24-Channel)_(SKU:DRI0029)}
    """
    def __init__(self, port="/dev/ttyACM0", baudrate=57600):
        """ Init Veyron driver
        """
        super(Veyron, self).__init__()

        self._serial = serial.Serial(port=port, baudrate=baudrate)

        self._lock = threading.RLock()

    def _sendCmd(self, command):
        """
        """
        Logger().debug("Veyron._sendCmd(): command=%s" % command)

        self._lock.acquire()
        try:
            self._serial.write(command)
            resp = self._serial.read(self._serial.inWaiting())
            return resp

        finally:
            self._lock.release()

    def waitEndOfSyncMove(self):
        while self._sendCmd("Q\r") != '.':
            time.sleep(0.01)

    def move(self, servos, staggeringDelay=0.):
        """
        @todo: handle staggeringDelay
        """
        Logger().debug("Veyron.move()")

        cmd = ""
        for servo in servos:
            cmd += "#%d P%d " % (servo.num, servo.pulse)

        cmd = cmd[:-1] + "\r"

        #Logger().debug("Veyron.move(): cmd=%s" % cmd)
        self._sendCmd(cmd)

    def moveSync(self, servos, duration):
        Logger().debug("Veyron.moveSync(): duration=%.3f" % duration)

        cmd = ""
        for servo in servos:
            cmd += "#%d P%d " % (servo.num, servo.pulse)

        cmd += "T%d\r" % (duration * 1000)  # ms

        #Logger().debug("Veyron.moveSync(): cmd=%s" % cmd)
        self._sendCmd(cmd)
