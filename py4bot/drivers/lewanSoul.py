# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators (Servos) management using LewaSoul bus servos (LX-16A)

Implements
==========

 - B{LewanSoul}

Documentation
=============

Cf U{http://www.lewansoul.com/uploads/attachment/20171204/93cd398b7aa583824a0acd5a23fb8ebb.zip}

Command packet format:

Header     ID number  Data Length  Command  Parameter      Checksum
0x55 0x55  ID         Length       Cmd      Prm 1...Prm N  Checksum

Header: two consecutive 0x55 are received indicating the arrival of data packets.

ID：Each servo has an ID number. ID number ranges from 0 ~ 253, converted to hexadecimal 0x00 ~ 0xFD.
Broadcast ID: ID N° 254 (0xfe) is the broadcast ID. All servos will receive instructions, but they all do not
return the response message, (except for reading the servo ID number, please refer to the following instructions
for detailed instructions) to prevent bus conflict.

Length(data): equal to the length of the data that is to be sent (including its own one byte). That is, the
length of the data plus 3 is equal to the length of this command packet, from the header to the checksum.

Command: control the various instructions of servo, such as position, speed control.

Parameter: in addition to commands, parameter are control informations that need to add.

Checksum: the calculation method is as follows:
    Checksum = ~(ID+ Length+Cmd+ Prm1+...PrmN)
If the numbers in the brackets are calculated and exceeded 255, then take the lowest one byte.

Command name                Command value   Length
SERVO_MOVE_TIME_WRITE        1              7
SERVO_MOVE_TIME_READ         2              3
SERVO_MOVE_TIME_WAIT_WRITE   7              7
SERVO_MOVE_TIME_WAIT_READ    8              3
SERVO_MOVE_START            11              3
SERVO_MOVE_STOP             12              3
SERVO_ID_WRITE              13              4
SERVO_ID_READ               14              3
SERVO_ANGLE_OFFSET_ADJUST   17              4
SERVO_ANGLE_OFFSET_WRITE    18              3
SERVO_ANGLE_OFFSET_READ     19              3
SERVO_ANGLE_LIMIT_WRITE     20              7
SERVO_ANGLE_LIMIT_READ      21              3
SERVO_VIN_LIMIT_WRITE       22              7
SERVO_VIN_LIMIT_READ        23              3
SERVO_TEMP_MAX_LIMIT_WRITE  24              4
SERVO_TEMP_MAX_LIMIT_READ   25              3
SERVO_TEMP_READ             26              3
SERVO_VIN_READ              27              3
SERVO_POS_READ              28              3
SERVO_OR_MOTOR_MODE_WRITE   29              7
SERVO_OR_MOTOR_MODE_READ    30              3
SERVO_LOAD_OR_UNLOAD_WRITE  31              4
SERVO_LOAD_OR_UNLOAD_READ   32              3
SERVO_LED_CTRL_WRITE        33              4
SERVO_LED_CTRL_READ         34              3
SERVO_LED_ERROR_WRITE       35              4
SERVO_LED_ERROR_READ        36              3

Command name               Command value    length
SERVO_MOVE_TIME_READ         2              7
SERVO_MOVE_TIME_WAIT_READ    8              7
SERVO_ID_READ               14              4
SERVO_ANGLE_OFFSET_READ     19              4
SERVO_ANGLE_LIMIT_READ      21              7
SERVO_VIN_LIMIT_READ        23              7
SERVO_TEMP_MAX_LIMIT_READ   25              4
SERVO_TEMP_READ             26              4
SERVO_VIN_READ              27              5
SERVO_POS_READ              28              5
SERVO_OR_MOTOR_MODE_READ    30              7
SERVO_LOAD_OR_UNLOAD_READ   32              4
SERVO_LED_CTRL_READ         34              4
SERVO_LED_ERROR_READ        36              4

Usage
=====


@author: Frédéric Mantegazza
@copyright: (C) 2017-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time
import struct
import threading

import serial

from py4bot.common.exception import Py4botValueError, Py4botHardwareError
from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver

BAUDRATE = 115200
BROADCAST_ID = 254

COMMANDS = {
    'SERVO_MOVE_TIME_WRITE':      {'value':  1, 'length': 7, 'params': 2, 'fmt': "<2BBBBHHB"},
    'SERVO_MOVE_TIME_READ':       {'value':  2, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 7, 'rparams': 2, 'rfmt': "<2BBBBHHB"},
    'SERVO_MOVE_TIME_WAIT_WRITE': {'value':  7, 'length': 7, 'params': 2, 'fmt': "<2BBBBHHB"},
    'SERVO_MOVE_TIME_WAIT_READ':  {'value':  8, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 7, 'rparams': 2, 'rfmt': "<2BBBBHHB"},
    'SERVO_MOVE_START':           {'value': 11, 'length': 3, 'params': 0, 'fmt': "<2BBBBB"},
    'SERVO_MOVE_STOP':            {'value': 12, 'length': 3, 'params': 0, 'fmt': "<2BBBBB"},
    'SERVO_ID_WRITE':             {'value': 13, 'length': 4, 'params': 1, 'fmt': "<2BBBBBB"},
    'SERVO_ID_READ':              {'value': 14, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
    'SERVO_ANGLE_OFFSET_ADJUST':  {'value': 17, 'length': 4, 'params': 1, 'fmt': "<2BBBBbB"},
    'SERVO_ANGLE_OFFSET_WRITE':   {'value': 18, 'length': 3, 'params': 0, 'fmt': "<2BBBBB"},
    'SERVO_ANGLE_OFFSET_READ':    {'value': 19, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBbB"},
    'SERVO_ANGLE_LIMIT_WRITE':    {'value': 20, 'length': 7, 'params': 2, 'fmt': "<2BBBBHHB"},
    'SERVO_ANGLE_LIMIT_READ':     {'value': 21, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 7, 'rparams': 2, 'rfmt': "<2BBBBHHB"},
    'SERVO_VIN_LIMIT_WRITE':      {'value': 22, 'length': 7, 'params': 2, 'fmt': "<2BBBBHHB"},
    'SERVO_VIN_LIMIT_READ':       {'value': 23, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 7, 'rparams': 2, 'rfmt': "<2BBBBHHB"},
    'SERVO_TEMP_MAX_LIMIT_WRITE': {'value': 24, 'length': 4, 'params': 1, 'fmt': "<2BBBBBB"},
    'SERVO_TEMP_MAX_LIMIT_READ':  {'value': 25, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
    'SERVO_TEMP_READ':            {'value': 26, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
    'SERVO_VIN_READ':             {'value': 27, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 5, 'rparams': 1, 'rfmt': "<2BBBBHB"},
    'SERVO_POS_READ':             {'value': 28, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 5, 'rparams': 1, 'rfmt': "<2BBBBhB"},
    'SERVO_OR_MOTOR_MODE_WRITE':  {'value': 29, 'length': 7, 'params': 3, 'fmt': "<2BBBBBhB"},
    'SERVO_OR_MOTOR_MODE_READ':   {'value': 30, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 7, 'rparams': 3, 'rfmt': "<2BBBBBhB"},
    'SERVO_LOAD_OR_UNLOAD_WRITE': {'value': 31, 'length': 4, 'params': 1, 'fmt': "<2BBBBBB"},
    'SERVO_LOAD_OR_UNLOAD_READ':  {'value': 32, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
    'SERVO_LED_CTRL_WRITE':       {'value': 33, 'length': 4, 'params': 1, 'fmt': "<2BBBBBB"},
    'SERVO_LED_CTRL_READ':        {'value': 34, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
    'SERVO_LED_ERROR_WRITE':      {'value': 35, 'length': 4, 'params': 1, 'fmt': "<2BBBBBB"},
    'SERVO_LED_ERROR_READ':       {'value': 36, 'length': 3, 'params': 0, 'fmt': "<2BBBBB", 'rlength': 4, 'rparams': 1, 'rfmt': "<2BBBBBB"},
}


class LewanSoul(Driver):
    """
    """
    def __init__(self, port="/dev/ttyACM0"):
        """ Init LewanSoul driver
        """
        super(LewanSoul, self).__init__()

        self._serial = serial.Serial(port=port, baudrate=BAUDRATE)

        self._lock = threading.RLock()

    def _computeChecksum(self, packet):
        """ Compute data checksum

        Checksum: the calculation method is as follows:
            Checksum = ~(ID+ Length+Cmd+ Prm1+...PrmN)
        If the numbers in the brackets are calculated and exceeded 255, then take the lowest one byte.
        """
        #Logger().debug("LewanSoul._computeChecksum(): packet=%s" % repr(packet))
        data = struct.unpack("%dB" % len(packet), packet)

        checksum = 0
        for val in data:
            checksum += val
        checksum %= 256
        checksum = 255 - checksum

        return checksum

    def _sendCmd(self, servoId, command, *params):
        """ Low-level send command

        Header     ID number  Data Length  Command  Parameter      Checksum
        0x55 0x55  ID         Length       Cmd      Prm 1...Prm N  Checksum

        @todo: implement broadcast
        """
        Logger().debug("LewanSoul._sendCmd(): servoId=%d, command=%s, params=%s" % (servoId, command, params))

        self._lock.acquire()
        try:
            value = COMMANDS[command]['value']
            length = COMMANDS[command]['length']
            fmt = COMMANDS[command]['fmt']
            packet = struct.pack(fmt[3:-1], servoId, length, value, *params)
            checksum = self._computeChecksum(packet)
            packet = struct.pack(fmt, 0x55, 0x55, servoId, length, value, *params+(checksum,))
            Logger().debug("LewanSoul._sendCmd(): %s" % repr(packet))
            self._serial.write(packet)

            # Get response if any
            if COMMANDS[command].has_key('rparams'):
                time.sleep(0.01)
                response = self._serial.read(self._serial.inWaiting())
                Logger().debug("LewanSoul._sendCmd(): response: %s " % repr(response))
                if response:
                    rfmt = COMMANDS[command]['rfmt']
                    data = struct.unpack(rfmt, response)
                    Logger().debug("LewanSoul._sendCmd(): data=%s" % repr(data))
                    rservoId = data[2]
                    if servoId != BROADCAST_ID and rservoId != servoId:
                        raise Py4botValueError("Wrong return servo Id")
                    rlength = data[3]
                    if rlength != COMMANDS[command]['rlength']:
                        raise Py4botValueError("Wrong return length")
                    rvalue = data[4]
                    if rvalue != value:
                        raise Py4botValueError("Wrong return command value")
                    for rcommand in COMMANDS.keys():
                        if COMMANDS[rcommand]['value'] == rvalue:
                            break
                    else:
                        raise Py4botHardwareError("Wrong return command")
                    rparams = data[5:-1]
                    checksum = data[-1]
                    if checksum != self._computeChecksum(response[2:-1]):
                        raise Py4botValueError("Wrong return checksum")
                    Logger().debug("LewanSoul._sendCmd(): rservoId: %d, rvalue: %d, rparams: %s " % (rservoId, rvalue, repr(rparams)))

                    return rservoId, rvalue, rparams
                else:
                    raise Py4botHardwareError("No answer from servo")

        finally:
            self._lock.release()

    def readId(self, servo):
        """ Read the servo Id
        """
        servoId, value, params = self._sendCmd(servo.num, "SERVO_ID_READ")

        return params[0]

    def readTemperature(self, servos):
        """ Read servos temperatures
        """
        for servo in servos:
            servoId, value, params = self._sendCmd(servo.num, "SERVO_TEMP_READ")
            servo.temperature = params[0]

    def readVoltage(self, servos):
        """ Read servos power voltages
        """
        for servo in servos:
            servoId, value, params = self._sendCmd(servo.num, "SERVO_VIN_READ")
            servo.voltage = params[0]

    def readPosition(self, servos):
        """ Read servos positions
        """
        for servo in servos:
            servoId, value, params = self._sendCmd(servo.num, "SERVO_POS_READ")
            servo.position = params[0]

    def writeEnable(self, servos):
        """ Enable/disable servos
        """
        for servo in servos:
            if servo.enable:
                self._sendCmd(servo.num, "SERVO_LOAD_OR_UNLOAD_WRITE", 1)
            else:
                self._sendCmd(servo.num, "SERVO_LOAD_OR_UNLOAD_WRITE", 0)

    def writeDeviation(self, servos):
        """ Write servos deviation
        """
        for servo in servos:
            self._sendCmd(servo.num, "SERVO_ANGLE_OFFSET_ADJUST", servo.deviation)

    def saveDeviation(self, servos):
        """ Save deviation to EEPROM
        """
        self._sendCmd(BROADCAST_ID, "SERVO_ANGLE_OFFSET_WRITE")

    def waitEndOfSyncMove(self):
        while False:
            time.sleep(0.01)

    def move(self, servos, staggeringDelay=0.):
        Logger().trace("LewanSoul.move()")
        #Logger().debug("LewanSoul.move(): servos=%s, " % [(servo.num, servo.point) for servo in servos])

        for servo in servos:

            # Set servo target position (will go there at full speed)
            self._sendCmd(servo.num, "SERVO_MOVE_TIME_WRITE", servo.point, 0)

            if staggeringDelay:
                time.sleep(staggeringDelay)

    def moveSync(self, servos, duration):
        Logger().trace("LewanSoul.moveSync()")
        #Logger().debug("LewanSoul.moveSync(): servos=%s, duration=%.3f" % ([(servo.num, servo.point) for servo in servos], duration))

        for servo in servos:
            self._sendCmd(servo.num, "SERVO_MOVE_TIME_WAIT_WRITE", servo.point, duration*1000)

        # Start sequence
        self._sendCmd(BROADCAST_ID, "SERVO_MOVE_START")

        # Wait end of move
