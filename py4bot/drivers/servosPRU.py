# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators (Servos) management using PRU on BeagleBone Black


Implements
==========

 - B{PRU}
 - B{PRU0}
 - B{PRU1}
 - B{ServosPRU}

Documentation
=============

Usage
=====


@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time
import os
import mmap
import struct

import prussdrv

from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver

DATA_MEMORY = 0x00000000

NB_SERVOS = 32

PULSE_WIDTH_OFFSET = 0
INCREMENT_OFFSET = 32
NB_STEPS_OFFSET = 64


class PRU(object):
    """
    """
    def __init__(self):
        """
        """
        super(PRU, self).__init__()

        self._init()

        prussdrv.init()
        prussdrv.open(self._numEvent)
        prussdrv.pruintc_init(prussdrv.getPRUSS_INTC_INITDATA())

        with open("/dev/mem", "r+b") as f:
            self._dataMemory = mmap.mmap(f.fileno(), 8*1024, offset=0x4a300000)

    def __del__(self):
        """
        """
        prussdrv.exit()

    def _init(self):
        """
        """
        raise NotImplementedError

    def loadFirmware(self, firmware="/lib/firmware/pru0.bin"):
        """ Load firmware into PRU
        """
        if not os.path.exists(firmware):
            raise Py4botValueError("file not found (%s)" % firmware)

        prussdrv.exec_program(self._numPRU, firmware)

    def readDataMemory(self, offset, nbWords):
        """ Read words from data memory
        """
        data = struct.unpack("<%di" % nbWords, self._dataMemory[DATA_MEMORY+offset*4:DATA_MEMORY+offset*4+nbWords*4])
        if nbWords == 1:
            return data[0]
        else:
            return data

    def writeDataMemory(self, offset, data):
        """

        @param data: data to write
        @type data: list or tuple
        """
        if not isinstance(data, (list, tuple)):
            data = (data,)
        nbWords = len(data)
        self._dataMemory[DATA_MEMORY+offset*4:DATA_MEMORY+offset*4+nbWords*4] = struct.pack("<%di" % nbWords, *data)

    def waitEvent(self):
        """
        """
        prussdrv.pru_wait_event(self._numEvent)
        prussdrv.pru_clear_event(self._numInterrupt, self._numEvent)


class PRU0(PRU):
    """
    """
    def _init(self):
        self._numPRU = prussdrv.PRU0
        self._numEvent = prussdrv.PRU_EVTOUT_0
        self._numInterrupt = prussdrv.PRU0_ARM_INTERRUPT


class PRU1(PRU):
    """
    """
    def _init(self):
        self._numPRU = prussdrv.PRU1
        self._numEvent = prussdrv.PRU_EVTOUT_1
        self._numInterrupt = prussdrv.PRU1_ARM_INTERRUPT


class ServosPRU(Driver):
    """
    """
    def __init__(self):
        """ Init ServoPRU driver
        """
        #super(ServosPRU, self).__init__()

        self._pru0 = PRU0()
        self._pru0.loadFirmware("/lib/firmware/servos-pru0.bin")

    def waitEndOfSyncMove(self):
        while self._pru0.readDataMemory(NB_STEPS_OFFSET, 1):
            time.sleep(0.01)

    def move(self, servos, staggeringDelay=0.):
        Logger().trace("ServosPRU.move()")
        #Logger().debug("ServosPRU.move(): servos=%s, " % [(servo.num, servo.pulse) for servo in servos])

        for servo in servos:

            # Set servo target position (will go there at full speed)
            self._pru0.writeDataMemory(PULSE_WIDTH_OFFSET+servo.num, servo.pulse << 16)

            if staggeringDelay:
                time.sleep(staggeringDelay)

    def moveSync(self, servos, duration):
        Logger().trace("ServosPRU.moveSync()")
        #Logger().debug("ServosPRU.moveSync(): servos=%s, duration=%.3f" % ([(servo.num, servo.pulse) for servo in servos], duration))

        # Read current pulses
        rawPulses = list(self._pru0.readDataMemory(PULSE_WIDTH_OFFSET, NB_SERVOS))

        # Reset all increments
        incs = NB_SERVOS * [0]

        # Compute nb steps according to duration (a step is 20ms)
        #nbSteps = int(duration * 1000) / 20  # without import division
        nbSteps = int(round(duration * 1000 / 20))

        for servo in servos:

            # Is servo disabled?
            if rawPulses[servo.num] == 0:

                # If yes, set servo target position (will go there at full speed)
                rawPulses[servo.num] = servo.pulse << 16
            else:

                # If not, compute move increment
                #incs[servo.num] = ((servo.pulse << 16) - rawPulses[servo.num]) / nbSteps + 1  # without import division
                incs[servo.num] = int(round(((servo.pulse << 16) - rawPulses[servo.num]) / nbSteps)) + 1

        # Write raw pulses and increments
        self._pru0.writeDataMemory(PULSE_WIDTH_OFFSET, rawPulses)
        self._pru0.writeDataMemory(INCREMENT_OFFSET, incs)

        # Set nb steps, which also start sequence
        self._pru0.writeDataMemory(NB_STEPS_OFFSET, nbSteps)

        # Wait end of move
        #self._pru0.waitEvent()
