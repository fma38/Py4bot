#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Low-level servos driver

Implements
==========

 - B{createResponse}
 - B{loop}
 - B{main}

Documentation
=============

PCA9685 low-level driver implementation

Usage
=====

This code runs as a separate process, to take advantage of muti-core CPU, like
on the Raspberry Pi 2/3.

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

from py4bot.common import config
from py4bot.services.logger import Logger
from py4bot.drivers.processDriver import ProcessDriver
from py4bot.hardware.i2cBus import I2cBus, I2cFakeBus
from py4bot.hardware.pca9685 import Pca9685, Pca9685Servo


class Pca9685Driver(ProcessDriver):
    """
    """
    def __init__(self, addresses=[0x40, 0x41], i2cFake=False):
        """
        """
        self._addresses = addresses
        self._i2cFake = i2cFake

        super(Pca9685Driver, self).__init__("Pca9685")

    def _createServos(self):
        if self._i2cFake:
            i2cBus = I2cFakeBus()
        else:
            i2cBus = I2cBus(config.I2C_BUS)

        servos = []  # TODO: use pool!
        for address in self._addresses:
            try:
                pca9685 = Pca9685(i2cBus, int(address))
            except ValueError:
                pca9685 = Pca9685(i2cBus, int(address, 16))
            pca9685.setFrequency(config.PCA9685_DRIVER_PWM_FREQUENCY)

            timePerClick = 1e6 / config.PCA9685_DRIVER_PWM_FREQUENCY / Pca9685.RESOLUTION  # use µs
            for channel in range(Pca9685.NB_OUTPUT):
                servo = Pca9685Servo(pca9685, channel, timePerClick)
                servos.append(servo)

        return servos

    def _setPulse(self, num, pulse):
        self._servos[num].pulse = pulse
