# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{ProcessDriver}

Documentation
=============

When using hardware controllers wich can only set current servo position, we need to write a driver able to
move all servos in a synchronized way. Such drivers should run an en independant process, to take advantage
of mutli-cores architectures.

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time
import threading
import multiprocessing
import itertools

from py4bot.common import config
from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver
from py4bot.common.response import ResponseFactory


class ProcessDriver(Driver, multiprocessing.Process):
    """ Base class for driver running as processes
    """
    def __init__(self, name):
        """ Init ProcessDriver object
        """
        Driver.__init__(self)
        multiprocessing.Process.__init__(self, name=name)

        self._servos = self._createServos()

        self._queue = multiprocessing.JoinableQueue(1)

        self.daemon = True
        self.start()

    def _createServos(self):
        """ Create the servo table

        @todo: use pool
        """
        raise NotImplementedError

    def _setPulse(self, num, pulse):
        """ Output the pulse on controller
        """
        raise NotImplementedError

    def waitEndOfSyncMove(self):
        self._queue.join()

    def move(self, servos, staggeringDelay=0.):
        #Logger().trace("ProcessDriver.move()")

        if staggeringDelay:
            for servo in servos:
                cmd = {'pulses': {servo.num: servo.pulse}}
                self._queue.put(cmd)
                time.sleep(staggeringDelay)
        else:
            pulses = {}
            for servo in servos:
                pulses[servo.num] = servo.pulse
            cmd = {'pulses': pulses}
            self._queue.put(cmd)

    def moveSync(self, servos, duration):
        #Logger().debug("ProcessDriver.moveSync(): duration=%.3f" % duration)

        pulses = {}
        for servo in servos:
            pulses[servo.num] = servo.pulse
        cmd = {'pulses': pulses, 'duration': duration}
        self._queue.put(cmd)

    def run(self):
        """ Process main loop

        Wait for incoming datas, and drive servos accordingly.
        """
        threading.current_thread().setName("ProcessDriver")

        Logger().info("ProcessDriver entering main loop...")
        while True:
            try:

                # Wait for cmd
                cmd = self._queue.get()
                #Logger().debug("ProcessDriver.run(): received cmd '%r'" % cmd)

                # Move sync
                if cmd.has_key('duration'):
                    table = {'nums': [], 'responses': []}
                    for num, pulse in cmd['pulses'].items():
                        if self._servos[int(num)].pulse:
                            start = self._servos[int(num)].pulse
                        else:
                            start = pulse
                        end = pulse
                        response = ResponseFactory().create(config.MAESTRO_DRIVER_CURVE, start, end, cmd['duration'])
                        table['nums'].append(int(num))
                        table['responses'].append(response)

                    startTime = time.time()
                    for pulses in itertools.izip(*table['responses']):
                        #Logger().debug("ProcessDriver.run(): %.3fs -> %d" % (time.time() - startTime, int(round(pulses[0]))))
                        for i, pulse in enumerate(pulses):
                            self._setPulse(table['nums'][i], int(round(pulse)))

                # Move immediate
                else:
                    for num, pulse in cmd['pulses'].items():
                        self._setPulse(int(num), pulse)

                # Notify cmd is over
                self._queue.task_done()

            except KeyboardInterrupt:
                break

            except:
                Logger().exception("ProcessDriver.loop()")

        Logger().info("ProcessDriver main loop terminated")
