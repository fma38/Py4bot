# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http:#py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http:#www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators (Servos) management using PRU on BeagleBone Black


Implements
==========

 - B{PRU}
 - B{PRU0}
 - B{PRU1}
 - B{ServoNode}

Documentation
=============

Usage
=====


@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

from __future__ import division

import time

from py4bot.common import config
from py4bot.common.exception import Py4botValueError
from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver
from py4bot.hardware.i2cBus import I2cBus, I2cFakeBus

NB_SERVOS_PER_NODE = 4

# Commands
# Must be even values, so they can be used in General Call Address
# 0x04 and 0x06 should not be used.
CMD_IDLE = 0
CMD_MOVE = 0x10  # move from current positions to targets, synchronized, using duration
CMD_STOP = 0x12  # stop current move

# I2C registers
REG_CMD         = 0x00  # R/W, uint8_t
REG_INPUTS      = 0x01  # RO,  uint8_t
REG_ADC         = 0x02  # RO,  uint8_t
REG_POSITION_0  = 0x03  # R/W, uint16_t
REG_POSITION_1  = 0x05  # R/W, uint16_t
REG_POSITION_2  = 0x07  # R/W, uint16_t
REG_POSITION_3  = 0x09  # R/W, uint16_t
REG_TARGET_0    = 0x0b  # R/W, uint16_t
REG_TARGET_1    = 0x0d  # R/W, uint16_t
REG_TARGET_2    = 0x0f  # R/W, uint16_t
REG_TARGET_3    = 0x11  # R/W, uint16_t
REG_DURATION    = 0x13  # R/W, uint16_t
REG_ADC_LOW     = 0x15  # R/W, uint8_t
REG_ADC_HIGH    = 0x16  # R/W, uint8_t
REG_STOP_CONFIG = 0x17  # R/W, uint8_t
REG_STATUS      = 0x18  # RO,  uint8_t

# Config
STOP_CONFIG_INPUT_0      = 0b00000001  # 0=disable, 1=enable
STOP_CONFIG_EDGE_INPUT_0 = 0b00000010  # 0=low-high, 1=high-low
STOP_CONFIG_INPUT_1      = 0b00000100  # 0=disable, 1=enable
STOP_CONFIG_EDGE_INPUT_1 = 0b00001000  # 0=low-high, 1=high-low
STOP_CONFIG_INPUT_2      = 0b00010000  # 0=disable, 1=enable
STOP_CONFIG_EDGE_INPUT_2 = 0b00100000  # 0=low-high, 1=high-low
STOP_CONFIG_ADC          = 0b01000000  # 0=disable, 1=enable
STOP_CONFIG_EDGE_ADC     = 0b10000000  # 0=low-high, 1=high-low

# Status
STATUS_UNKNOW_CMD = 0b00000001  # unknown command
STATUS_MOVING     = 0b00000010  # moving
STATUS_STOPPED    = 0b00000100  # current move has been stopped, either by user or by input/adc


class ServoNode(Driver):
    """
    """
    def __init__(self, addresses=[0x10, 0x11, 0x12, 0x13, 0x14, 0x15], i2cFake=False):
        """ Init ServoNode driver
        """
        super(ServoNode, self).__init__()

        self._addresses = addresses
        if i2cFake:
            self._i2cBus = I2cFakeBus()
        else:
            self._i2cBus = I2cBus(config.I2C_BUS)

        #self._i2cBus.write(0x10, REG_ADC)
        #print("REG_ADC=", hex(self._i2cBus.read(0x10)))

    def waitEndOfSyncMove(self):
        while True:
            moving = False

            for address in self._addresses:  # TODO: retreive with servos
                status = self._i2cBus.read8(address, REG_STATUS)
                if status & STATUS_MOVING:
                    moving = True
                    break

            if not moving:
                break

            time.sleep(0.01)

    def move(self, servos, staggeringDelay=0.):
        Logger().trace("ServoNode.move()")
        #Logger().debug("ServoNode.move(): servos=%s, " % [(servo.num, servo.pulse) for servo in servos])

        for servo in servos:

            # Set servo position (will go there at full speed)
            #print(hex(self._addresses[servo.num//NB_SERVOS_PER_NODE]), REG_POSITION_0+2*(servo.num%NB_SERVOS_PER_NODE), servo.pulse)
            self._i2cBus.write16(self._addresses[servo.num//NB_SERVOS_PER_NODE], REG_POSITION_0+2*(servo.num%NB_SERVOS_PER_NODE), servo.pulse)

            if staggeringDelay:
                time.sleep(staggeringDelay)

    def moveSync(self, servos, duration):
        Logger().trace("ServoNode.moveSync()")
        #Logger().debug("ServoNode.moveSync(): servos=%s, duration=%.3f" % ([(servo.num, servo.pulse) for servo in servos], duration))

        for servo in servos:

            # Set servo target position
            self._i2cBus.write16(self._addresses[servo.num//NB_SERVOS_PER_NODE], REG_TARGET_0+2*(servo.num%NB_SERVOS_PER_NODE), servo.pulse)

        # Set duration
        for address in self._addresses:
            self._i2cBus.write16(address, REG_DURATION, int(duration*1000))

        # Start sync move
        self._i2cBus.write8(0x00, REG_CMD, CMD_MOVE)  # general address call

        # Wait end of move
        #self.waitEndOfSyncMove()
