
# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{Driver3D}

Documentation
=============

Usage
=====


@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time

from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver


class Driver3D(Driver):
    """

    @todo: move 3D as backend, and communicate via pipes, like for PCA 9685?
           This will allow decomposing movements
    """
    def move(self, actuators3D, staggeringDelay=0.):
        Logger().trace("Driver3D.move()")

        for actuator3D in actuators3D:
            #Logger().debug("Driver3D.move(): actuator3D num %d, joint '%s': angle=%.1f" % (actuator3D.num, actuator3D.joint.name, actuator3D.joint.angle))
            actuator3D.angle = actuator3D.joint.angle

            if staggeringDelay:
                time.sleep(staggeringDelay)

    def moveSync(self, actuators3D, duration):
        """

        @todo: call another method as a thread, and manage duration for next call here
        """
        Logger().trace("Driver.moveSync()")

        # Smooth move
        nbSteps = 3

        # Compute increment
        increments = 32 * [0]
        for actuator3D in actuators3D:
            increments[actuator3D.num] = (actuator3D.joint.angle - actuator3D.angle) / nbSteps
            actuator3D.joint.angle = actuator3D.angle

        for i in range(nbSteps):
            for actuator3D in actuators3D:
                actuator3D.joint.angle += increments[actuator3D.num]
            startMove = time.time()
            self.move(actuators3D)
            while time.time() - startMove < duration / nbSteps:
                time.sleep(0.01)
