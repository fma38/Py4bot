# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{FakeDriver}

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time

from py4bot.services.logger import Logger
from py4bot.drivers.driver import Driver


class FakeDriver(Driver):
    """ Fake driver implementation
    """
    def move(self, actuators, staggeringDelay=0.):
        #Logger().trace("FakeDriver.move()")

        for actuator in actuators:
            #Logger().debug("FakeDriver.move(): actuator joint '%s': angle=%.1f" % (actuator.joint.name, actuator.joint.angle))

            if staggeringDelay:
                time.sleep(staggeringDelay)
