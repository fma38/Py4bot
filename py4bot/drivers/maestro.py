# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

Actuators management

Implements
==========

 - B{Maestro}

Documentation
=============

Usage
=====

TODO
====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL
"""

import time

import serial

from py4bot.services.logger import Logger
from py4bot.drivers.processDriver import ProcessDriver

SERVO_PULSE_MIN = 64  # µs
SERVO_PULSE_MAX = 3280  # µs


class MaestroServo(ProcessDriver):
    """ Maestro-bases servos class
    """
    def __init__(self):
        """ Init MaestroServo object
        """
        super(ProcessServo, self).__init__()

        self._pulse = None

    @property
    def pulse(self):
        return self._pulse

    @pulse.setter
    def pulse(self, value):
        self._pulse = value


class Maestro(ProcessDriver):
    """ Pololu Maestro driver implementation

    This object generates code for the Pololu Maestro Servo Controllers familly.
    See U{https://www.pololu.com/category/102/maestro-usb-servo-controllers}
    """
    def __init__(self, nbServos=24, port="/dev/ttyACM0", baudrate=57600):
        """ Init Maestro driver
        """
        self._nbServos = nbServos
        self._serial = serial.Serial(port=port, baudrate=baudrate)
        self._initController()

        super(Maestro, self).__init__("Maestro")

    def _createServos(self):
        servos = []  # TODO: use pool!
        for i in range(self._nbServos):
            servo = MaestroServo()
            servos.append(servo)

        return servos

    def _setPulse(self, num, pulse):
        if not SERVO_PULSE_MIN <= pulse <= SERVO_PULSE_MAX:
            raise ValueError("Pulse width must be in [%d-%d] (%d)" % (SERVO_PULSE_MIN, SERVO_PULSE_MAX, pulse))

        Logger().debug("Maestro._setPulse(): pulse=%.2fµs (%d)" % (pulse, int(pulse * 4)))

        # Save pulse
        self._servos[num].pulse = pulse

        # Move servo
        low, high = self._splitLowHigh(int(pulse * 4))
        self._sendCmd(0x04, num, low, high)

    def _splitLowHigh(self, value):
        """ Split value in low/high bytes.

        Also set the MSB to 0, as controller expects.

        @param value: value to split
        @type value: int

        @return: (low, high) value
        @rtype: tuple of int
        """
        low = value & 0x7f
        high = (value >> 7) & 0x7f

        return low, high

    def _sendCmd(self, command, *args):
        """ Send a command to the Maestro controller

        @param command: command to send
        @type command: int
        """
        #Logger().debug("Maestro._sendCmd: command=0x%x, args=%s" % (command, args))

        # Send command to controller
        size = len(args) + 1
        self._serial.write(struct.pack(size*'B', 0x80+command, *args))
        self._serial.write(command)
        time.sleep(0.01)
        resp = self._serial.read(self._serial.inWaiting())

        return resp

    def _initController(self):
        """ Init the controller.
        """

        # Reset
        self._serial.setRTS(1)
        self._serial.setDTR(1)
        self._serial.setRTS(0)
        self._serial.setDTR(0)

        # Stop any running script
        self._sendCmd(0x24)

        # Set no accel limit
        low, high = self._splitLowHigh(0)
        self._sendCmd(0x09, self._axis, low, high)

        # Set full speed
        low, high = self._splitLowHigh(255)
        self._sendCmd(0x07, num, low, high)
