#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time
import math

import visual

from py4bot.api import *

import settings


class CoordinatesSystem3D(visual.frame):
    """ Coordinates system 3D visualisation
    """
    def __init__(self, color=None, *args, **kwargs):
        """ Init CoordinatesSystem3D object
        """
        super(CoordinatesSystem3D, self).__init__(*args, **kwargs)

        if color is None:
            visual.sphere(frame=self, radius=3, color=visual.color.gray(0.5))
            visual.arrow(frame=self, axis=(1, 0,  0), length=30, color=visual.color.cyan)
            visual.arrow(frame=self, axis=(0, 0, -1), length=30, color=visual.color.magenta)
            visual.arrow(frame=self, axis=(0, 1,  0), length=30, color=visual.color.yellow)


class HexapodBody3D(Body3D):
    """
    """
    def _build3D(self):
        visual.box(frame=self, axis=(0, 1, 0), length=5, width=130, height=70, color=visual.color.yellow, opacity=0.25)
        CoordinatesSystem3D(frame=self)


class Hexapod(Robot3D):
    def __init__(self, *args, **kwargs):
        super(Hexapod, self).__init__(*args, **kwargs)

        # Add feet with trail
        if float(visual.version[0]) >= 6:
            self._feet3D = {}
            for legIndex, footNeutralP in self._feetNeutralP.items():
                self._feet3D[legIndex] = visual.sphere(pos=(footNeutralP[0], footNeutralP[2], -footNeutralP[1]), radius=0, color=visual.color.red,
                                                       make_trail=True)

    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg3Dof(legIndex, {'coxa': Coxa(), 'femur': Femur(), 'tibia': Tibia()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg3DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = Driver3D()
        pool = ActuatorPool(driver)

        num = 0
        for leg in self._legs.values():

            # Create joints actuators
            coxa3D = Coxa3D(leg.coxa, num, settings.LEGS_GEOMETRY[leg.index]['coxa'])
            pool.add(coxa3D)
            num += 1

            femur3D = Femur3D(leg.femur, num, settings.LEGS_GEOMETRY[leg.index]['femur'])
            pool.add(femur3D)
            num += 1

            tibia3D = Tibia3D(leg.tibia, num, settings.LEGS_GEOMETRY[leg.index]['tibia'])
            pool.add(tibia3D)
            num += 1

            # Init 3D view
            # TODO: create a Leg3D to handle all this?
            coxa3D.frame = self._body3D
            femur3D.frame = coxa3D
            tibia3D.frame = femur3D

            coxa3D.pos = (settings.LEGS_ORIGIN[leg.index]['x'], 0, -settings.LEGS_ORIGIN[leg.index]['y'])
            femur3D.pos = (settings.LEGS_GEOMETRY[leg.index]['coxa'], 0, 0)
            tibia3D.pos = (settings.LEGS_GEOMETRY[leg.index]['femur'], 0, 0)

            coxa3D.rotate(angle=math.radians(settings.LEGS_ORIGIN[leg.index]['gamma0']), axis=(0, 1, 0))

            if leg.index == 'RR':
                CoordinatesSystem3D(frame=coxa3D)

        return pool

    def _createBody3D(self):
        return HexapodBody3D()

    def _updateIK(self):
        super(Hexapod, self)._updateIK()

        # Move feet, in order to refresh trail
        if float(visual.version[0]) >= 6:
            for leg in self._legs.values():
                footNeutralP = self._feetNeutralP[leg.index]
                footP = leg.footP + footNeutralP
                self._feet3D[leg.index].pos = (footP[0], footP[2], -footP[1])


class Keyboard(RemoteControl):
    def _createFrontend(self):
        try:
            return KeyboardUsb(settings.DELL_KEYBOARD_PATH)
        except OSError:
            return KeyboardUsb(settings.MICROSOFT_KEYBOARD_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_059", trigger="hold")  # F1

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_002")  # ESC
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_015")  # TAB
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_008", trigger="hold")  # 7

        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", mapper=MapperSetMultiply('z', coef=20))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_008", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('roll', coef=10))

        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_105", mapper=MapperSetValue(dx=-5))  # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_106", mapper=MapperSetValue(dx=+5))  # RIGHT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_103", mapper=MapperSetValue(dy=+5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_108", mapper=MapperSetValue(dy=-5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_104", mapper=MapperSetValue(dz=+5))  # PG UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_109", mapper=MapperSetValue(dz=-5))  # PG DOWN

        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_075", modifier="digital_042", mapper=MapperSetValue(dyaw=+5))    # NUM 4
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_077", modifier="digital_042", mapper=MapperSetValue(dyaw=-5))    # NUM 6
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_103", modifier="digital_042", mapper=MapperSetValue(dpitch=-5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_108", modifier="digital_042", mapper=MapperSetValue(dpitch=+5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_105", modifier="digital_042", mapper=MapperSetValue(droll=-5))   # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_106", modifier="digital_042", mapper=MapperSetValue(droll=+5))   # RIGHT

        self._addComponent(Button, command=self.robot.setBodyPosition, key="digital_082", mapper=MapperSetValue(x=0, y=0, z=35, yaw=0, pitch=0, roll=0))   # NUM 0


class Gamepad(RemoteControl):
    def _createFrontend(self):
        return Thrustmaster(settings.THRUSTMASTER_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_003", trigger="hold")  # BTN_NORTH

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_000")  # BTN_SOUTH
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_002")  # BTN_C, BTN_EST
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_006", trigger="hold")  # BTN_TL
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectPrevGait, key="digital_007", trigger="hold")  # BTN_TR

        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_02", "analog_03", "analog_00"), mapper=MapperWalk())

        # Body inc. translation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_008", mapper=MapperSetMultiply('dx', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_008", mapper=MapperSetMultiply('dy', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_008", mapper=MapperSetValue(dz=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", modifier="digital_008", mapper=MapperSetValue(dz=-5))

        # Body inc. rotation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_009", mapper=MapperSetMultiply('droll', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_009", mapper=MapperSetMultiply('dpitch', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_009", mapper=MapperSetValue(dyaw=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_006", modifier="digital_009", mapper=MapperSetValue(dyaw=-5))

        # Body extra translation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", modifier="digital_008", mapper=MapperSetMultiply('z', coef=20))

        # Body extra rotation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_009", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_009", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_009", mapper=MapperSetMultiply('roll', coef=10))


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    # Init some 3D stuff
    scene = visual.display(width=settings.SCENE_WIDTH, height=settings.SCENE_HEIGHT)
    ground = visual.box(axis=(0, 1, 0), pos=(0, 0, 0), length=0.1, height=500, width=500, color=visual.color.gray(0.75), opacity=0.5)

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")
    #GaitManager().select("riple")

    robot = Hexapod()

    # Init some more 3D stuff (opt.)
    CoordinatesSystem3D()
    for legIndex, footNeutralP in robot.feetNeutralP.items():
        visual.sphere(pos=(footNeutralP[0], footNeutralP[2], -footNeutralP[1]), radius=5)
        visual.cylinder(pos=(footNeutralP[0], footNeutralP[2], -footNeutralP[1]), axis=(0, 0, -1), radius=1, length=50)
        visual.cylinder(pos=(footNeutralP[0], footNeutralP[2], -footNeutralP[1]), axis=(0, 0, 1), radius=1, length=50)
        if legIndex == 'RR':
            CoordinatesSystem3D(pos=(footNeutralP[0], footNeutralP[2], -footNeutralP[1]))

    GaitSequencer().start()

    try:
        gamepad = Gamepad(robot)
        gamepad.start()
    except OSError:
        Logger().exception("main()", debug=True)
        keyboard = Keyboard(robot)
        keyboard.start()

    robot.setBodyPosition(z=40)

    robot.mainLoop()

    remote.stop()
    remote.join()
    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    Logger().setLevel('debug')
    main()
