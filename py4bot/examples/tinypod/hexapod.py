#!/usr/bin/env python
# -*- coding: utf-8 -*-

import time

from py4bot.api import *

import settings


class Hexapod(Robot):
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg2Dof(legIndex, {'coxa': Coxa(), 'femur': Femur()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg2DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = Pca9685Driver(addresses=[0x40])
        #driver = Pca9685Driver(i2cFake=True)
        pool = ServoPool(driver)

        for leg in self._legs.values():

            # Create joints actuators
            num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            servo = Servo(leg.coxa, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            servo = Servo(leg.femur, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

        return pool


class Gamepad(RemoteControl):
    def _createFrontend(self):
        return Thrustmaster(settings.THRUSTMASTER_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_008", trigger="hold")
        self._addComponent(Button, command=self.selectPreviousConfig, key="digital_009", trigger="hold")

        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", mapper=MapperSetValue(dz=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", mapper=MapperSetValue(dz=-5))
        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_02", "analog_03", "analog_00"), mapper=MapperWalk())

        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", mapper=MapperSetMultiply('yaw', coef=-15))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", mapper=MapperSetMultiply('pitch', coef=-15))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", mapper=MapperSetMultiply('roll', coef=15))


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    Logger().setLevel('trace')

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")
    GaitManager().select("riple")

    robot = Hexapod()

    #remote = Gamepad(robot)

    GaitSequencer().start()
    #remote.start()

    robot.setBodyPosition(z=0, staggeringDelay=0.1)  # staggeringDelay avoid to much current drawing
    robot.setBodyPosition(z=50)

    GaitSequencer().walk(linearSpeed=0.5, direction=0, length=0.1, angularSpeed=0.)
    time.sleep(5)
    GaitSequencer().walk(linearSpeed=0., direction=0, length=0., angularSpeed=0.)

    robot.mainLoop()

    #remote.stop()
    #remote.join()
    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    main()
