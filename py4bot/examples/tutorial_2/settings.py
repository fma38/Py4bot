# -*- coding: utf-8 -*-

from py4bot.common import config


# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs geometry
LEGS_GEOMETRY = {
    'RM': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'RF': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LF': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LM': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LR': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'RR': {'coxa': 25, 'femur': 45, 'tibia': 65}
}

# Legs origin
LEGS_ORIGIN = {
    'RM': {'x':  35., 'y':   0., 'gamma0' :   0.},
    'RF': {'x':  35., 'y':  65., 'gamma0' :  30.},
    'LF': {'x': -35., 'y':  65., 'gamma0' : 150.},
    'LM': {'x': -35., 'y':   0., 'gamma0' : 180.},
    'LR': {'x': -35., 'y': -65., 'gamma0' : 210.},
    'RR': {'x':  35., 'y': -65., 'gamma0' : 330.},
}

# Legs feet neutral position
FEET_NEUTRAL = {
    'RM': LEGS_GEOMETRY['RM']['coxa'] + LEGS_GEOMETRY['RM']['femur'],
    'RF': LEGS_GEOMETRY['RF']['coxa'] + LEGS_GEOMETRY['RF']['femur'],
    'LF': LEGS_GEOMETRY['LF']['coxa'] + LEGS_GEOMETRY['LF']['femur'],
    'LM': LEGS_GEOMETRY['LM']['coxa'] + LEGS_GEOMETRY['LM']['femur'],
    'LR': LEGS_GEOMETRY['LR']['coxa'] + LEGS_GEOMETRY['LR']['femur'],
    'RR': LEGS_GEOMETRY['RR']['coxa'] + LEGS_GEOMETRY['RR']['femur'],
}

# Gaits
GAIT_LEGS_GROUPS = {
    'tripod':      (('RM', 'LF', 'LR'), ('RF', 'LM', 'RR')),
    'tetrapod':    (('RR', 'LM'), ('RF', 'LR'), ('RM', 'LF')),
    'riple':       (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'metachronal': (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'wave':        (('RR',), ('RM',), ('RF',), ('LR',), ('LM',), ('LF',))
}

# Gaits parameters
GAIT_PARAMS = {
    'default': {
        'length': 30.,
        'angle': 2.5,
        'height': {
            'min': 20.,
            'max': 40.
        },
        'speed': {
            'min': 25.,
            'max': 250.
        }
    },
    'tripod': {},
    'tetrapod': {},
    'riple': {},
    'metachronal': {},
    'wave': {}
}

# Gamepad path
THRUSTMASTER_PATH = "/dev/input/by-id/usb-Mega_World_Thrustmaster_dual_analog_3.2-event-joystick"

# VPython scene default size
SCENE_WIDTH = 640
SCENE_HEIGHT = 480
