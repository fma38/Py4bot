#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Titus 3DoF hexapod

This hexapod is my first prototype, using small servos.

Testing auto-leveling.
"""

from __future__ import division

import math
import time
import copy

from py4bot.api import *
from py4bot.hardware.i2cBus import I2cBus
from py4bot.hardware.mpu6050 import Mpu6050

import settings


class Titus(Robot):
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg3Dof(legIndex, {'coxa': Coxa(), 'femur': Femur(), 'tibia': Tibia()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg3DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = Pca9685Driver((0x40, 0x41))
        pool = ServoPool(driver)

        for leg in self._legs.values():

            # Create joints actuators
            num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            servo = Servo(leg.coxa, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            servo = Servo(leg.femur, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['tibia']
            servo = Servo(leg.tibia, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

        return pool

    def setAutoLevel(self):
        if GaitSequencer().state in ('idle', 'pause'):
            Logger().info("Engage auto-leveling...")
            self._autoLevel = True
            self._autoLevelStartTime = time.time()

    def mainLoop(self):
        dt = 0.05  # s
        tau = 0.25  # s
        a = tau / (tau + dt)
        Logger().debug("Complementary filter: dt=%.3fs, tau=%.3fs, a=%.3f" % (dt, tau, a))

        i2cBus = I2cBus(0)
        mpu6050 = Mpu6050(i2cBus)

        self._autoLevel = False
        while True:
            try:
                t = time.time()
                mpu6050.refresh(tau, dt)

                # Get orientation (with complementary filter)
                pitch = mpu6050.orientation['filtered']['pitch']
                roll = mpu6050.orientation['filtered']['roll']

                if self._autoLevel:
                    self.setBodyPosition(pitch=pitch, roll=roll, staggeringDelay=0.001)
                    try:
                        time.sleep(dt-(time.time()-t))
                    except IOError:
                        Logger().warning("Robot mainloop overrun")

                    if time.time() - self._autoLevelStartTime > 2:
                        self._autoLevel = False
                        Logger().info("Auto-leveling done")

            except KeyboardInterrupt:
                Logger().info("Exiting robot main loop")
                break

            except:
                Logger().exception("robot.mainLoop()")


class Gamepad(RemoteControl):
    def _createFrontend(self):
        return Thrustmaster(settings.THRUSTMASTER_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_003", trigger="hold")  # BTN_NORTH

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_000")  # BTN_SOUTH
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_002")  # BTN_C, BTN_EST
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_006", trigger="hold")  # BTN_TL
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectPrevGait, key="digital_007", trigger="hold")  # BTN_TR

        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_02", "analog_03", "analog_00"), mapper=MapperWalk())

        # Body inc. translation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_008", mapper=MapperSetMultiply('dx', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_008", mapper=MapperSetMultiply('dy', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_008", mapper=MapperSetValue(dz=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", modifier="digital_008", mapper=MapperSetValue(dz=-5))

        # Body inc. rotation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_009", mapper=MapperSetMultiply('droll', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_009", mapper=MapperSetMultiply('dpitch', coef=-5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_009", mapper=MapperSetValue(dyaw=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_006", modifier="digital_009", mapper=MapperSetValue(dyaw=-5))

        # Body extra translation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", modifier="digital_008", mapper=MapperSetMultiply('z', coef=20))

        # Body extra rotation
        #self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_009", mapper=MapperSetMultiply('yaw', coef=-10))
        #self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_009", mapper=MapperSetMultiply('pitch', coef=-10))
        #self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_009", mapper=MapperSetMultiply('roll', coef=10))

        # Enable/disable auto-leveling
        self._addComponent(Button, command=self.robot.setAutoLevel, key="digital_003")


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")

    GaitManager().select("tripod")

    robot = Titus()

    robot.setBodyPosition(z=65, staggeringDelay=0.01)

    GaitSequencer().start()

    gamepad = Gamepad(robot)
    gamepad.start()

    robot.mainLoop()

    gamepad.stop()
    gamepad.join()

    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    Logger().setLevel('debug')
    main()
