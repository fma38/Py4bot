

# -*- coding: utf-8 -*-

from __future__ import division

import math


# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs geometry
LEGS_GEOMETRY = {
    'RM': {'coxa': 20, 'femur': 50, 'tibia': 73},
    'RF': {'coxa': 20, 'femur': 50, 'tibia': 73},
    'LF': {'coxa': 20, 'femur': 50, 'tibia': 73},
    'LM': {'coxa': 20, 'femur': 50, 'tibia': 73},
    'LR': {'coxa': 20, 'femur': 50, 'tibia': 73},
    'RR': {'coxa': 20, 'femur': 50, 'tibia': 73}
}

# Legs origin
LEGS_ORIGIN = {
    'RM': {'x': 40*math.cos(math.radians(  0)), 'y': 40*math.sin(math.radians(  0)), 'gamma0' :   0.},
    'RF': {'x': 40*math.cos(math.radians( 60)), 'y': 40*math.sin(math.radians( 60)), 'gamma0' :  60.},
    'LF': {'x': 40*math.cos(math.radians(120)), 'y': 40*math.sin(math.radians(120)), 'gamma0' : 120.},
    'LM': {'x': 40*math.cos(math.radians(180)), 'y': 40*math.sin(math.radians(180)), 'gamma0' : 180.},
    'LR': {'x': 40*math.cos(math.radians(240)), 'y': 40*math.sin(math.radians(240)), 'gamma0' : 240.},
    'RR': {'x': 40*math.cos(math.radians(300)), 'y': 40*math.sin(math.radians(300)), 'gamma0' : 300.},
}

# Legs feet neutral position
FEET_NEUTRAL = {
    'RM': LEGS_GEOMETRY['RM']['coxa']+LEGS_GEOMETRY['RM']['femur'],
    'RF': LEGS_GEOMETRY['RF']['coxa']+LEGS_GEOMETRY['RF']['femur'],
    'LF': LEGS_GEOMETRY['LF']['coxa']+LEGS_GEOMETRY['LF']['femur'],
    'LM': LEGS_GEOMETRY['LM']['coxa']+LEGS_GEOMETRY['LM']['femur'],
    'LR': LEGS_GEOMETRY['LR']['coxa']+LEGS_GEOMETRY['LR']['femur'],
    'RR': LEGS_GEOMETRY['RR']['coxa']+LEGS_GEOMETRY['RR']['femur'],
}

# Legs / servos mapping
LEGS_SERVOS_MAPPING = {
    'RF': {'coxa': 16, 'femur': 17, 'tibia': 18},
    'RM': {'coxa': 20, 'femur': 21, 'tibia': 22},
    'RR': {'coxa': 24, 'femur': 25, 'tibia': 26},
    'LR': {'coxa':  8, 'femur':  9, 'tibia': 10},
    'LM': {'coxa':  4, 'femur':  5, 'tibia':  6},
    'LF': {'coxa':  0, 'femur':  1, 'tibia':  2}
}

# Servos calibration
SERVOS_CALIBRATION = {
     0: {'offset':   +0., 'neutral': 1500, 'ratio': -0.120},  # LF coxa
     1: {'offset': +180., 'neutral': 1610, 'ratio': +0.090},  # LF femur
     2: {'offset':  +90., 'neutral': 1740, 'ratio': -0.120},  # LF tibia
     4: {'offset':   +0., 'neutral': 1595, 'ratio': -0.120},  # LM coxa
     5: {'offset': +180., 'neutral': 1565, 'ratio': +0.109},  # LM femur
     6: {'offset':  +90., 'neutral': 1600, 'ratio': -0.120},  # LM tibia
     8: {'offset':   +0., 'neutral': 1380, 'ratio': -0.120},  # LR coxa
     9: {'offset': +180., 'neutral': 1405, 'ratio': +0.120},  # LR femur
    10: {'offset':  +90., 'neutral': 1650, 'ratio': -0.120},  # LR tibia
    16: {'offset':   +0., 'neutral': 1400, 'ratio': -0.120},  # RF coxa
    17: {'offset': +180., 'neutral': 1250, 'ratio': -0.120},  # RF femur
    18: {'offset':  +90., 'neutral': 1050, 'ratio': +0.120},  # RF tibia
    20: {'offset':   +0., 'neutral': 1200, 'ratio': -0.120},  # RM coxa
    21: {'offset': +180., 'neutral': 1150, 'ratio': -0.120},  # RM femur
    22: {'offset':  +90., 'neutral': 1000, 'ratio': +0.120},  # RM tibia
    24: {'offset':   +0., 'neutral': 1100, 'ratio': -0.120},  # RR coxa
    25: {'offset': +180., 'neutral': 1100, 'ratio': -0.120},  # RR femur
    26: {'offset':  +90., 'neutral': 1100, 'ratio': +0.120},  # RR tibia
}

# Gaits definitions
GAIT_LEGS_GROUPS = {
    'tripod':      (('RM', 'LF', 'LR'), ('RF', 'LM', 'RR')),
    'tetrapod':    (('RR', 'LM'), ('RF', 'LR'), ('RM', 'LF')),
    'riple':       (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'metachronal': (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'wave':        (('RR',), ('RM',), ('RF',), ('LR',), ('LM',), ('LF',)),
}

# Gaits parameters
GAIT_PARAMS = {
    'default': {
        'length': 25.,
        'angle': 5.,
        'height': {
            'min': 20.,
            'max': 40.
        },
        'speed': {
            'min': 25.,
            'max': 100.
        }
    },
    'tripod': {
        'length': 30.,
    },
    'tetrapod': {},
    'riple': {},
    'metachronal': {},
    'wave': {}
}

# Remote controls paths
THRUSTMASTER_PATH = "/dev/input/by-id/usb-Mega_World_Thrustmaster_dual_analog_3.2-event-joystick"
