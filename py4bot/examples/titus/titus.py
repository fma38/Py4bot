#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Titus 3DoF hexapod

This hexapod is my first prototype, using small servos.

I built it as a proof of concept, when I started to develop Py4bot,
to see if I would be able to write some working code, before starting
to build a larger hexapod (Cronos).
"""

from py4bot.api import *

import settings


class Titus(Robot):
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg3Dof(legIndex, {'coxa': Coxa(), 'femur': Femur(), 'tibia': Tibia()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg3DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = Pca9685Driver((0x40, 0x41))  #, i2cFake=True)
        pool = ServoPool(driver)

        for leg in self._legs.values():

            # Create joints actuators
            num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            servo = Servo(leg.coxa, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            servo = Servo(leg.femur, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['tibia']
            servo = Servo(leg.tibia, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

        return pool


class Gamepad(RemoteControl):
    def _createFrontend(self):
        return Thrustmaster(settings.THRUSTMASTER_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_003", trigger="hold")  # BTN_NORTH

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_000")  # BTN_SOUTH
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_002")  # BTN_C, BTN_EST
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_006", trigger="hold")  # BTN_TL
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectPrevGait, key="digital_007", trigger="hold")  # BTN_TR

        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_02", "analog_03", "analog_00"), mapper=MapperWalk(), threshold=0.2)

        # Body inc. translation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_008", mapper=MapperSetMultiply('dx', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_008", mapper=MapperSetMultiply('dy', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_008", mapper=MapperSetValue(dz=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", modifier="digital_008", mapper=MapperSetValue(dz=-5))

        # Body inc. rotation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_009", mapper=MapperSetMultiply('droll', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_009", mapper=MapperSetMultiply('dpitch', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_009", mapper=MapperSetValue(dyaw=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_006", modifier="digital_009", mapper=MapperSetValue(dyaw=-5))

        # Body extra translation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", modifier="digital_008", mapper=MapperSetMultiply('z', coef=20))

        # Body extra rotation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_009", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_009", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_009", mapper=MapperSetMultiply('roll', coef=10))


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")

    GaitManager().select("tripod")

    robot = Titus()

    robot.setBodyPosition(z=40, staggeringDelay=0.1)

    GaitSequencer().start()
    #GaitSequencer().walk(linearSpeed=1, direction=90, angularSpeed=0)

    gamepad = Gamepad(robot)
    gamepad.start()

    robot.mainLoop()

    gamepad.stop()
    GaitSequencer().stop()


if __name__ == "__main__":
    Logger().setLevel('trace')
    main()
