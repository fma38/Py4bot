#!/usr/bin/env python
# -*- coding: utf-8 -*-

from py4bot.api import *

import settings


class Hexapod(Robot):
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg2Dof(legIndex, {'coxa': Coxa(), 'femur': Femur()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg2DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = FakeDriver()
        pool = ServoPool(driver)

        for leg in self._legs.values():

            # Create joints actuators
            num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            servo = Servo(leg.coxa, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            servo = Servo(leg.femur, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

        return pool


class Keyboard(RemoteControl):
    def _createFrontend(self):
        try:
            return KeyboardUsb(settings.DELL_KEYBOARD_PATH)
        except:
            return KeyboardUsb(settings.MICROSOFT_KEYBOARD_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="button_059", trigger="hold")  # F1

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="button_002")  # ESC
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="button_015")  # TAB
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="button_008", trigger="hold")  # 7

        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", mapper=MapperSetMultiply('z', coef=20))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="button_008", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="button_008", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="button_008", mapper=MapperSetMultiply('roll', coef=10))

        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_105", mapper=MapperSetValue(dx=-5))  # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_106", mapper=MapperSetValue(dx=+5))  # RIGHT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_103", mapper=MapperSetValue(dy=+5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_108", mapper=MapperSetValue(dy=-5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_104", mapper=MapperSetValue(dz=+5))  # PG UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_109", mapper=MapperSetValue(dz=-5))  # PG DOWN

        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_075", modifier="button_042", mapper=MapperSetValue(dyaw=+5))    # NUM 4
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_077", modifier="button_042", mapper=MapperSetValue(dyaw=-5))    # NUM 6
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_103", modifier="button_042", mapper=MapperSetValue(dpitch=-5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_108", modifier="button_042", mapper=MapperSetValue(dpitch=+5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_105", modifier="button_042", mapper=MapperSetValue(droll=-5))   # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="button_106", modifier="button_042", mapper=MapperSetValue(droll=+5))   # RIGHT

        self._addComponent(Button, command=self.robot.setBodyPosition, key="button_082", mapper=MapperSetValue(x=0, y=0, z=35, yaw=0, pitch=0, roll=0))   # NUM 0


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")
    GaitManager().select("riple")

    robot = Hexapod()

    remote = Keyboard(robot)

    GaitSequencer().start()
    remote.start()

    robot.setBodyPosition(z=30)
    GaitSequencer().walk(linearSpeed=0.2, direction=90, length=1, angularSpeed=0)

    robot.mainLoop()

    remote.stop()
    remote.join()
    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    Logger().setLevel('debug')
    main()
