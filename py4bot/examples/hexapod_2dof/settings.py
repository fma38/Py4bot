# -*- coding: utf-8 -*-

from py4bot.common import config


# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs geometry
LEGS_GEOMETRY = {
    'RM': {'coxa': 25, 'femur': 45},
    'RF': {'coxa': 25, 'femur': 45},
    'LF': {'coxa': 25, 'femur': 45},
    'LM': {'coxa': 25, 'femur': 45},
    'LR': {'coxa': 25, 'femur': 45},
    'RR': {'coxa': 25, 'femur': 45}
}

# Legs origin
LEGS_ORIGIN = {
    'RM': {'x':  50., 'y':   0., 'gamma0' :   0.},
    'RF': {'x':  35., 'y':  80., 'gamma0' :  30.},
    'LF': {'x': -35., 'y':  80., 'gamma0' : 150.},
    'LM': {'x': -50., 'y':   0., 'gamma0' : 180.},
    'LR': {'x': -35., 'y': -80., 'gamma0' : 210.},
    'RR': {'x':  35., 'y': -80., 'gamma0' : 330.},
}

# Legs feet neutral position
FEET_NEUTRAL = {
    'RM': LEGS_GEOMETRY['RM']['coxa'] + LEGS_GEOMETRY['RM']['femur']/2,
    'RF': LEGS_GEOMETRY['RF']['coxa'] + LEGS_GEOMETRY['RF']['femur']/2,
    'LF': LEGS_GEOMETRY['LF']['coxa'] + LEGS_GEOMETRY['LF']['femur']/2,
    'LM': LEGS_GEOMETRY['LM']['coxa'] + LEGS_GEOMETRY['LM']['femur']/2,
    'LR': LEGS_GEOMETRY['LR']['coxa'] + LEGS_GEOMETRY['LR']['femur']/2,
    'RR': LEGS_GEOMETRY['RR']['coxa'] + LEGS_GEOMETRY['RR']['femur']/2,
}

# Legs / servos mapping
LEGS_SERVOS_MAPPING = {
    'RF': {'coxa':  0, 'femur':  1},
    'RM': {'coxa':  2, 'femur':  3},
    'RR': {'coxa':  4, 'femur':  5},
    'LR': {'coxa':  6, 'femur':  7},
    'LM': {'coxa':  8, 'femur':  9},
    'LF': {'coxa': 10, 'femur': 11}
}

# Servos calibration
SERVOS_CALIBRATION = {
    0: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RF
    1: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RF

    2: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RM
    3: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RM

    4: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RR
    5: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RR

    6: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LR
    7: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LR

    8: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LM
    9: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LM

    10: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LF
    11: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LF
}

# Gaits
GAIT_LEGS_GROUPS = {
    'tripod':      (('RM', 'LF', 'LR'), ('RF', 'LM', 'RR')),
    'tetrapod':    (('RR', 'LM'), ('RF', 'LR'), ('RM', 'LF')),
    'riple':       (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'metachronal': (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'wave':        (('RR',), ('RM',), ('RF',), ('LR',), ('LM',), ('LF',))
}

# Gaits parameters
GAIT_PARAMS = {
    'default': {
        'length': 20.,
        'angle': 2.5,
        'height': {
            'min': 20.,
            'max': 40.
        },
        'speed': {
            'min': 25.,
            'max': 250.
        }
    },
    'tripod': {
        'length': 30.,
        'angle': 5.
    },
    'tetrapod': {},
    'riple': {},
    'metachronal': {},
    'wave': {}
}

# Keyboards path
MICROSOFT_KEYBOARD_PATH = "/dev/input/by-id/usb-Microsoft_Microsoft®_Digital_Media_Keyboard_3000-event-kbd"
DELL_KEYBOARD_PATH = "/dev/input/by-id/usb-Dell_Dell_USB_Keyboard-event-kbd"
