# -*- coding: utf-8 -*-


from py4bot.api import Gait


class GaitCripple(Gait):
    """ Implement 'cripple' gait.

    This gait only uses 5 legs (when one is broken!).
    """
    def _createTables(self):
        startTable = (
            ((0.0, 0.), ( 0.0,   0.), ( 0.0,   0.), (0.0,   0.), (0.375, 1.)),  # step 0
            ((0.0, 0.), ( 0.0,   0.), ( 0.0,   0.), (0.0,   0.), (0.75,  0.)),
            ((0.0, 0.), (-0.375, 1.), ( 0.0,   0.), (0.0,   0.), (0.75,  0.)),
            ((0.0, 0.), (-0.75,  0.), ( 0.0,   0.), (0.0,   0.), (0.75,  0.)),
            ((0.0, 0.), (-0.75,  0.), ( 0.0,   0.), (0.125, 1.), (0.75,  0.)),
            ((0.0, 0.), (-0.75,  0.), ( 0.0,   0.), (0.25,  0.), (0.75,  0.)),
            ((0.0, 0.), (-0.75,  0.), (-0.125, 1.), (0.25,  0.), (0.75,  0.)),
            ((0.0, 0.), (-0.75,  0.), (-0.25,  0.), (0.25,  0.), (0.75,  0.)),
            ((0.0, 1.), (-0.75,  0.), (-0.25,  0.), (0.25,  0.), (0.75,  0.))   # step n
        )

        walkTable = (
            (( 0.0,  1.), (-0.75, 0.), (-0.25, 0.), ( 0.25, 0.), ( 0.75, 0.)),  # step 0
            (( 1.0,  0.), (-1.0,  0.), (-0.5,  0.), ( 0.0,  0.), ( 0.5,  0.)),
            (( 0.75, 0.), ( 0.0,  1.), (-0.75, 0.), (-0.25, 0.), ( 0.25, 0.)),
            (( 0.5,  0.), ( 1.0,  0.), (-1.0,  0.), (-0.5,  0.), ( 0.0,  0.)),
            (( 0.25, 0.), ( 0.75, 0.), ( 0.0,  1.), (-0.75, 0.), (-0.25, 0.)),
            (( 0.0,  0.), ( 0.5,  0.), ( 1.0,  0.), (-1.0,  0.), (-0.5,  0.)),
            ((-0.25, 0.), ( 0.25, 0.), ( 0.75, 0.), ( 0.0,  1.), (-0.75, 0.)),
            ((-0.5,  0.), ( 0.0,  0.), ( 0.5,  0.), ( 1.0,  0.), (-1.0,  0.)),
            ((-0.75, 0.), (-0.25, 0.), ( 0.25, 0.), ( 0.75, 0.), ( 0.0,  1.)),
            ((-1.0,  0.), (-0.5,  0.), ( 0.0,  0.), ( 0.5,  0.), ( 1.0,  0.))   # step n
        )

        staticTable = (
            ((0.0, 1.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),  # step 0
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 1.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 1.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 1.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 1.)),
            ((0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.), (0.0, 0.))   # step n
        )

        stopTable = (
            ((0.0, 0.), (-0.75, 0.), (-0.25,  0.), (0.25,  0.), (0.75, 0.)),  # step 0
            ((0.0, 0.), (-0.75, 0.), (-0.125, 1.), (0.25,  0.), (0.75, 0.)),
            ((0.0, 0.), (-0.75, 0.), ( 0.0,   0.), (0.25,  0.), (0.75, 0.)),
            ((0.0, 0.), (-0.75, 0.), ( 0.0,   0.), (0.125, 1.), (0.75, 0.)),
            ((0.0, 0.), (-0.75, 0.), ( 0.0,   0.), (0.0,   0.), (0.75, 0.)),
            ((0.0, 0.), ( 0.25, 1.), ( 0.0,   0.), (0.0,   0.), (0.75, 0.)),
            ((0.0, 0.), ( 0.0,  0.), ( 0.0,   0.), (0.0,   0.), (0.75, 0.)),
            ((0.0, 0.), ( 0.0,  0.), ( 0.0,   0.), (0.0,   0.), (0.25, 1.)),
            ((0.0, 0.), ( 0.0,  0.), ( 0.0,   0.), (0.0,   0.), (0.0,  0.))   # step n
        )

        return startTable, walkTable, staticTable, stopTable
