# -*- coding: utf-8 -*-

import time
import math

#import visual

from py4bot.api import *

import settings


class Robot4DoF(Robot):
    """ 4 DOF implementation Robot base classe
    """
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg4Dof(legIndex, {'coxa': Coxa(), 'femur': Femur(), 'tibia': Tibia(), 'tars': Tars()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg4DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk


class Cronos(Robot4DoF):
    """ Real implementation
    """
    def _createActuatorPool(self):
        driver = ServoNode(i2cFake=False)
        #driver = FakeDriver()

        pool = ServoPool(driver)
        for leg in self._legs.values():

            # Create joints actuators
            num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            servo = Servo(leg.coxa, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            servo = Servo(leg.femur, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['tibia']
            servo = Servo(leg.tibia, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

            num = settings.LEGS_SERVOS_MAPPING[leg.index]['tars']
            servo = Servo(leg.tars, num, **settings.SERVOS_CALIBRATION[num])
            pool.add(servo)

        return pool

    #def mainLoopStep(self):
        #""" Test of walk cycle implementation
        #"""
        #Logger().info("start in 3s")
        #time.sleep(1)
        #Logger().info("start in 2s")
        #time.sleep(1)
        #Logger().info("start in 1s")
        #time.sleep(1)

        #GaitSequencer().walkCycle()

        #Logger().info("exit in 3s")
        #time.sleep(1)
        #Logger().info("exit in 2s")
        #time.sleep(1)
        #Logger().info("exit in 1s")
        #time.sleep(1)

        #raise Py4botInterrupt  # force exiting robot main loop


#class CronosBody3D(Body3D):
    #"""
    #"""
    #def _build3D(self):
        #visual.convex(frame=self, axis=(0, 1, 0), pos=(
            #(settings.LEGS_ORIGIN['RF']['x'], 0, settings.LEGS_ORIGIN['RF']['y']),
            #(settings.LEGS_ORIGIN['RM']['x'], 0, settings.LEGS_ORIGIN['RM']['y']),
            #(settings.LEGS_ORIGIN['RR']['x'], 0, settings.LEGS_ORIGIN['RR']['y']),
            #(settings.LEGS_ORIGIN['LR']['x'], 0, settings.LEGS_ORIGIN['LR']['y']),
            #(settings.LEGS_ORIGIN['LM']['x'], 0, settings.LEGS_ORIGIN['LM']['y']),
            #(settings.LEGS_ORIGIN['LF']['x'], 0, settings.LEGS_ORIGIN['LF']['y'])
        #))


#class Cronos3D(Robot4DoF, Robot3D):
    #""" VPython simulation implementation
    #"""
    #def _createBody3D(self):
        #return CronosBody3D()

    #def _createActuatorPool(self):
        #driver = Driver3D()

        #pool = ActuatorPool(driver)
        #for leg in self._legs.values():

            ## Create joints actuators
            #num = settings.LEGS_SERVOS_MAPPING[leg.index]['coxa']
            #length = settings.LEGS_GEOMETRY[leg.index]['coxa']
            #coxa3D = Coxa3D(leg.coxa, num, length)
            #pool.add(coxa3D)

            #num = settings.LEGS_SERVOS_MAPPING[leg.index]['femur']
            #length = settings.LEGS_GEOMETRY[leg.index]['femur']
            #femur3D = Femur3D(leg.femur, num, length)
            #pool.add(femur3D)

            #num = settings.LEGS_SERVOS_MAPPING[leg.index]['tibia']
            #length = settings.LEGS_GEOMETRY[leg.index]['tibia']
            #tibia3D = Tibia3D(leg.tibia, num, length)
            #pool.add(tibia3D)

            #num = settings.LEGS_SERVOS_MAPPING[leg.index]['tars']
            #length = settings.LEGS_GEOMETRY[leg.index]['tars']
            #tars3D = Tars3D(leg.tars, num, length)
            #pool.add(tars3D)

            ## Init 3D view
            #coxa3D.frame = self._body3D
            #femur3D.frame = coxa3D
            #tibia3D.frame = femur3D
            #tars3D.frame = tibia3D

            #coxa3D.pos = (settings.LEGS_ORIGIN[leg.index]['x'], 0, -settings.LEGS_ORIGIN[leg.index]['y'])
            #femur3D.pos = (settings.LEGS_GEOMETRY[leg.index]['coxa'], 0, 0)
            #tibia3D.pos = (settings.LEGS_GEOMETRY[leg.index]['femur'], 0, 0)
            #tars3D.pos = (settings.LEGS_GEOMETRY[leg.index]['tibia'], 0, 0)

            #coxa3D.rotate(angle=math.radians(settings.LEGS_ORIGIN[leg.index]['gamma0']), axis=(0, 1, 0))

        #return pool
