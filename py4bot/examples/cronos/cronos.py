#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" Cronos 4DoF Hexapod.

This large hexapod uses TowerPro MG996R servos.
"""

import time
import argparse

#import visual

from py4bot.api import *

import settings
from robots import Cronos  #, Cronos3D
from controllers import Gamepad, SmartRemoteCtrl2
#from accessories import ArmGrip
from gaits import GaitCripple


def addGait(gaitClass, gaitName):
    """ Helper
    """
    group = settings.GAIT_LEGS_GROUPS[gaitName]
    params = settings.GAIT_PARAMS['default']
    params.update(settings.GAIT_PARAMS[gaitName])
    gait = gaitClass(gaitName, group, params)
    GaitManager().add(gait)


def main():
    """
    """

    # Options
    parser = argparse.ArgumentParser(description="4DoF strong Hexapod.")
    parser.add_argument("-s", "--simule", action="store_true", default=False,
                        help="run robot in simulation mode.")
    parser.add_argument("-l", "--logger",
                        choices=["trace", "debug", "info", "warning", "error", "exception", "critical"],
                        action="store", dest="loggerLevel", default="debug", metavar="LEVEL",
                        help="logger level")

    # Parse arguments
    args = parser.parse_args()
    Logger().setLevel(args.loggerLevel)

    # Create robot
    if args.simule:
        display = visual.display.get_selected()
        display.width = 640
        display.height = 480
        ground = visual.box(axis=(0, 1, 0), pos=(0, 0, 0), length=0.1, height=500, width=500, color=visual.color.gray(0.25), opacity=0.5)
        robot = Cronos3D()
    else:
        robot = Cronos()

    # Create accessories
    #accessory = ArmGrip()
    #robot.addAccessory(accessory)

    # Add standard and custom gaits
    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")
    #addGait(GaitCripple, "cripple")

    GaitManager().select("tripod")

    # Create our custom remote (can use several)
    #remote = Gamepad(robot)
    #remote = SmartJoystick(robot)
    #remote = Keyboard(robot)
    remote = SmartRemoteCtrl2(robot)

    GaitSequencer().start()

    robot.setBodyPosition(z=0, staggeringDelay=0.1)  # staggeringDelay avoid to much current drawing
    #time.sleep(1)
    robot.setBodyPosition(z=65)

    remote.start()

    robot.mainLoop()

    robot.setBodyPosition(z=0)

    remote.stop()
    remote.join()
    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    main()
