# -*- coding: utf-8 -*-

from py4bot.api import *

import settings

# Input frontends
THRUSTMASTER_PATH = "/dev/input/by-id/usb-Mega_World_Thrustmaster_dual_analog_3.2-event-joystick"


class SmartRemoteCtrl2(RemoteControl):
    def _createFrontend(self):
        return JsonUDP()

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")
#        self._addConfig("leg")

        # Config selection
        self._addComponent(Button, command=self.selectPrevConfig, key="digital_JOY_L", trigger="double-hold")
        self._addComponent(Button, command=self.selectNextConfig, key="digital_JOY_R", trigger="double-hold")

        # Gait selection
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectPrevGait, key="digital_JOY_L", trigger="double-click")
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_JOY_R", trigger="double-click")

        # Walk
        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_JOY_X", "analog_JOY_Y", "analog_JOY_Z"), mapper=MapperWalk())

        # Body extra rotation
        self._addComponent(Axis, configs="walk", command=self.robot.setBodyExtraPosition, key="analog_MPU_ORIENT_Y",  mapper=MapperSetMultiply('roll', coef=10))
        self._addComponent(Axis, configs="walk", command=self.robot.setBodyExtraPosition, key="analog_MPU_ORIENT_X",  mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_X", mapper=MapperSetMultiply('roll', coef=10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_Y", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_Z", mapper=MapperSetMultiply('yaw', coef=-10))

        # Body extra translation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_U", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_V", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_JOY_W", mapper=MapperSetMultiply('z', coef=20))

        # Body inc. translation
        #self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_X", modifier="digital_008", mapper=MapperSetMultiply('dx', coef=5))
        #self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_Y", modifier="digital_008", mapper=MapperSetMultiply('dy', coef=5))
        #self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_008", mapper=MapperSetValue(dz=+10))
        #self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", modifier="digital_008", mapper=MapperSetValue(dz=-10))

        # Body inc. rotation
        #self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_X", modifier="digital_009", mapper=MapperSetMultiply('droll', coef=5))
        #self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_Y", modifier="digital_009", mapper=MapperSetMultiply('dpitch', coef=-5))
        #self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_009", mapper=MapperSetValue(dyaw=+5))
        #self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_006", modifier="digital_009", mapper=MapperSetValue(dyaw=-5))

        # Inc feet neutral position
        #self._addComponent(Button, command=self.robot.incFeetNeutral, key="digital_005", mapper=MapperSetValue(dneutral=-10))
        #self._addComponent(Button, command=self.robot.incFeetNeutral, key="digital_007", mapper=MapperSetValue(dneutral=+10))


class Gamepad(RemoteControl):
    def _createFrontend(self):
        return Thrustmaster(THRUSTMASTER_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")
        #self._addConfig("leg")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_003", trigger="hold")  # BTN_NORTH

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_000")  # BTN_SOUTH
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_002")  # BTN_C, BTN_EST
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_006", trigger="hold")  # BTN_TL
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectPrevGait, key="digital_007", trigger="hold")  # BTN_TR

        self._addComponent(Joystick, configs="walk", command=GaitSequencer().walk, keys=("analog_02", "analog_03", "analog_00"), mapper=MapperWalk())

        # Body inc. translation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_008", mapper=MapperSetMultiply('dx', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_008", mapper=MapperSetMultiply('dy', coef=5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_008", mapper=MapperSetValue(dz=+10))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_005", modifier="digital_008", mapper=MapperSetValue(dz=-10))

        # Body inc. rotation
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_04", modifier="digital_009", mapper=MapperSetMultiply('droll', coef=5))
        self._addComponent(Axis, command=self.robot.incBodyPosition, key="analog_05", modifier="digital_009", mapper=MapperSetMultiply('dpitch', coef=-5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_004", modifier="digital_009", mapper=MapperSetValue(dyaw=+5))
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_006", modifier="digital_009", mapper=MapperSetValue(dyaw=-5))

        # Body extra translation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", modifier="digital_008", mapper=MapperSetMultiply('z', coef=20))

        # Body extra rotation
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_009", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_009", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_009", mapper=MapperSetMultiply('roll', coef=10))

        # Inc feet neutral position
        self._addComponent(Button, command=self.robot.incFeetNeutral, key="digital_005", mapper=MapperSetValue(dneutral=-10))
        self._addComponent(Button, command=self.robot.incFeetNeutral, key="digital_007", mapper=MapperSetValue(dneutral=+10))

## Body
## Rotations
#body.yaw = axes[2] * 30.
#body.pitch = -axes[1] * 30.
#body.roll = axes[0] * 30.
