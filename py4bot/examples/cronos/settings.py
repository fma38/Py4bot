# -*- coding: utf-8 -*-

from py4bot.common import config

# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs geometry
LEGS_GEOMETRY = {
    'RF': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74},
    'RM': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74},
    'RR': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74},
    'LR': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74},
    'LM': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74},
    'LF': {'coxa': 30, 'femur': 96.5, 'tibia': 70.2, 'tars': 74}
}

# Legs origin
LEGS_ORIGIN = {
    'RF': {'x':  62.625, 'y':  146.85, 'gamma0' :  45.},
    'RM': {'x':  85.6,   'y':    0.,   'gamma0' :   0.},
    'RR': {'x':  62.625, 'y': -147.85, 'gamma0' : 315.},
    'LR': {'x': -62.625, 'y': -147.85, 'gamma0' : 225.},
    'LM': {'x': -85.6 ,  'y':    0.,   'gamma0' : 180.},
    'LF': {'x': -62.625, 'y':  147.85, 'gamma0' : 135.}
}

# Legs feet neutral position
FEET_NEUTRAL = {
    'RF': LEGS_GEOMETRY['RF']['coxa'] + LEGS_GEOMETRY['RF']['femur'],
    'RM': LEGS_GEOMETRY['RM']['coxa'] + LEGS_GEOMETRY['RM']['femur'],
    'RR': LEGS_GEOMETRY['RR']['coxa'] + LEGS_GEOMETRY['RR']['femur'],
    'LR': LEGS_GEOMETRY['LR']['coxa'] + LEGS_GEOMETRY['LR']['femur'],
    'LM': LEGS_GEOMETRY['LM']['coxa'] + LEGS_GEOMETRY['LM']['femur'],
    'LF': LEGS_GEOMETRY['LF']['coxa'] + LEGS_GEOMETRY['LF']['femur']
}

# Legs / servos mapping
LEGS_SERVOS_MAPPING = {
    'RF': {'coxa':  0, 'femur':  1, 'tibia':  2, 'tars':  3},  # Servo_Node I²C slave 0x10
    'RM': {'coxa':  4, 'femur':  5, 'tibia':  6, 'tars':  7},  # Servo_Node I²C slave 0x11
    'RR': {'coxa':  8, 'femur':  9, 'tibia': 10, 'tars': 11},  # Servo_Node I²C slave 0x12
    'LR': {'coxa': 12, 'femur': 13, 'tibia': 14, 'tars': 15},  # Servo_Node I²C slave 0x13
    'LM': {'coxa': 16, 'femur': 17, 'tibia': 18, 'tars': 19},  # Servo_Node I²C slave 0x14
    'LF': {'coxa': 20, 'femur': 21, 'tibia': 22, 'tars': 23}   # Servo_Node I²C slave 0x15
}

# Servos calibration (MG996R)
SERVOS_CALIBRATION = {
     0: {'offset':   +0., 'neutral': 1415, 'ratio': +0.090},  # RF coxa
     1: {'offset': +180., 'neutral': 1555, 'ratio': -0.090},  # RF femur
     2: {'offset':  +90., 'neutral': 1325, 'ratio': +0.090},  # RF tibia
     3: {'offset': +180., 'neutral': 1450, 'ratio': -0.090},  # RF tars
     4: {'offset':   +0., 'neutral': 1500, 'ratio': +0.090},  # RM coxa
     5: {'offset': +180., 'neutral': 1720, 'ratio': -0.090},  # RM femur
     6: {'offset':  +90., 'neutral': 1175, 'ratio': +0.090},  # RM tibia
     7: {'offset': +180., 'neutral': 1325, 'ratio': -0.090},  # RM tars
     8: {'offset':   +0., 'neutral': 1500, 'ratio': +0.090},  # RR coxa
     9: {'offset': +180., 'neutral': 1690, 'ratio': -0.090},  # RR femur
    10: {'offset':  +90., 'neutral': 1340, 'ratio': +0.090},  # RR tibia
    11: {'offset': +180., 'neutral': 1295, 'ratio': -0.090},  # RR tars
    12: {'offset':   +0., 'neutral': 1390, 'ratio': +0.090},  # LR coxa
    13: {'offset': +180., 'neutral': 1505, 'ratio': +0.090},  # LR femur
    14: {'offset':  +90., 'neutral': 1855, 'ratio': -0.090},  # LR tibia
    15: {'offset': +180., 'neutral': 1685, 'ratio': +0.090},  # LR tars
    16: {'offset':   +0., 'neutral': 1585, 'ratio': +0.090},  # LM coxa
    17: {'offset': +180., 'neutral': 1445, 'ratio': +0.090},  # LM femur
    18: {'offset':  +90., 'neutral': 1830, 'ratio': -0.090},  # LM tibia
    19: {'offset': +180., 'neutral': 1630, 'ratio': +0.090},  # LM tars
    20: {'offset':   +0., 'neutral': 1500, 'ratio': +0.090},  # LF coxa
    21: {'offset': +180., 'neutral': 1255, 'ratio': +0.090},  # LF femur
    22: {'offset':  +90., 'neutral': 1705, 'ratio': -0.090},  # LF tibia
    23: {'offset': +180., 'neutral': 1735, 'ratio': +0.090}   # LF tars
}

# Servos calibration (LX-16A)
#SERVOS_CALIBRATION = {
#     0: {'offset':   0, 'ratio': +0.240},  # coxa  leg LF
#     1: {'offset': 180, 'ratio': +0.240},  # femur leg LF
#     2: {'offset':  90, 'ratio': -0.240},  # tars  leg LF
#     3: {'offset': 180, 'ratio': -0.240},  # tibia leg LF
#
#     4: {'offset':   0, 'ratio': +0.240},  # coxa  leg LM
#     5: {'offset': 180, 'ratio': +0.240},  # femur leg LM
#     6: {'offset':  90, 'ratio': -0.240},  # tibia leg LM
#     7: {'offset': 180, 'ratio': -0.240},  # tars  leg LM
#
#     8: {'offset':   0, 'ratio': +0.240},  # coxa  leg LR
#     9: {'offset': 180, 'ratio': +0.240},  # femur leg LR
#    10: {'offset':  90, 'ratio': -0.240},  # tibia leg LR
#    11: {'offset': 180, 'ratio': -0.240},  # tars  leg LR
#
#    16: {'offset':   0, 'ratio': +0.240},  # coxa  leg RF
#    17: {'offset': 180, 'ratio': -0.240},  # femur leg RF
#    18: {'offset':  90, 'ratio': +0.240},  # tibia leg RF
#    19: {'offset': 180, 'ratio': +0.240},  # tars  leg RF
#
#    20: {'offset':   0, 'ratio': +0.240},  # coxa  leg RM
#    21: {'offset': 180, 'ratio': -0.240},  # femur leg RM
#    22: {'offset':  90, 'ratio': +0.240},  # tibia leg RM
#    23: {'offset': 180, 'ratio': +0.240},  # tars  leg RM
#
#    24: {'offset':   0, 'ratio': +0.240},  # coxa  leg RR
#    25: {'offset': 180, 'ratio': -0.240},  # femur leg RR
#    26: {'offset':  90, 'ratio': +0.240},  # tibia leg RR
#    27: {'offset': 180, 'ratio': +0.240},  # tars  leg RR
#}

# Gaits definitions
GAIT_LEGS_GROUPS = {
    'tripod':      (('RM', 'LF', 'LR'), ('RF', 'LM', 'RR')),
    'tetrapod':    (('RR', 'LM'), ('RF', 'LR'), ('RM', 'LF')),
    'riple':       (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'metachronal': (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'wave':        (('RR',), ('RM',), ('RF',), ('LR',), ('LM',), ('LF',)),

    'cripple':     (('RR',), ('RM',), ('RF',), ('LM',), ('LF',))
}

GAIT_PARAMS = {
    'default': {
        'length': 20.,
        'angle': 5.,
        'height': {
            'min': 40.,
            'max': 65.
        },
        'speed': {
            'min': 25.,
            'max': 250.
        }
    },
    'tripod': {
        'speed': {
            'min': 50.,
            'max': 150.
        }
    },
    'tetrapod': {},
    'riple': {},
    'metachronal': {},
    'wave': {},
    'cripple': {
        'length': 20.,
        'angle': 2.5,
        'height': {
            'min': 30.,
            'max': 30.
        },
        'speed': {
            'min': 25.,
            'max': 100.
        }
    }
}

# Joysticks/gamepads calibration
# Use py4bot-joycal.py script to generate calibration
TEENSY_JOYSTICK_CALIBRATION = {
    0: (0x2a+0x10, 0x82-0x10, 0x88+0x10, 0xe2-0x10),  #  X right
    1: (0x21+0x10, 0x6e-0x10, 0x74+0x10, 0xc5-0x10),  #  Y right
    2: (0x19+0x10, 0x82-0x10, 0x88+0x10, 0xe4-0x10),  # RZ right
    #3: (0x  +0x10, 0x  -0x10, 0x  +0x10, 0x  -0x10),  #  X left
    #4: (0x  +0x10, 0x  -0x10, 0x  +0x10, 0x  -0x10),  #  Y left
    #5: (0x  +0x10, 0x  -0x10, 0x  +0x10, 0x  -0x10)   # RZ left
}

#config.I2C_BUS = 2  # I²C VGA bus on my PC

