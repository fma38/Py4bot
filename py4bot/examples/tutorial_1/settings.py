# -*- coding: utf-8 -*-

from py4bot.common import config


# Legs index
LEGS_INDEX = ('RF', 'RM', 'RR', 'LR', 'LM', 'LF')

# Legs geometry
LEGS_GEOMETRY = {
    'RM': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'RF': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LF': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LM': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'LR': {'coxa': 25, 'femur': 45, 'tibia': 65},
    'RR': {'coxa': 25, 'femur': 45, 'tibia': 65}
}

# Legs origin
LEGS_ORIGIN = {
    'RM': {'x':  50., 'y':   0., 'gamma0' :   0.},
    'RF': {'x':  35., 'y':  80., 'gamma0' :  30.},
    'LF': {'x': -35., 'y':  80., 'gamma0' : 150.},
    'LM': {'x': -50., 'y':   0., 'gamma0' : 180.},
    'LR': {'x': -35., 'y': -80., 'gamma0' : 210.},
    'RR': {'x':  35., 'y': -80., 'gamma0' : 330.},
}

# Legs feet neutral position
FEET_NEUTRAL = {
    'RM': LEGS_GEOMETRY['RM']['coxa'] + LEGS_GEOMETRY['RM']['femur'],
    'RF': LEGS_GEOMETRY['RF']['coxa'] + LEGS_GEOMETRY['RF']['femur'],
    'LF': LEGS_GEOMETRY['LF']['coxa'] + LEGS_GEOMETRY['LF']['femur'],
    'LM': LEGS_GEOMETRY['LM']['coxa'] + LEGS_GEOMETRY['LM']['femur'],
    'LR': LEGS_GEOMETRY['LR']['coxa'] + LEGS_GEOMETRY['LR']['femur'],
    'RR': LEGS_GEOMETRY['RR']['coxa'] + LEGS_GEOMETRY['RR']['femur'],
}

# Legs / servos mapping
LEGS_SERVOS_MAPPING = {
    'RF': {'coxa':  0, 'femur':  1, 'tibia':  2},
    'RM': {'coxa':  4, 'femur':  5, 'tibia':  6},
    'RR': {'coxa':  8, 'femur':  9, 'tibia': 10},
    'LR': {'coxa': 15, 'femur': 14, 'tibia': 13},
    'LM': {'coxa': 19, 'femur': 18, 'tibia': 17},
    'LF': {'coxa': 23, 'femur': 22, 'tibia': 21}
}

# Servos calibration
SERVOS_CALIBRATION = {
     0: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RF
     1: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RF
     2: {'offset':  90, 'neutral': 1500, 'ratio':  0.090},  # tibia leg RF

     4: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RM
     5: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RM
     6: {'offset':  90, 'neutral': 1500, 'ratio':  0.090},  # tibia leg RM

     8: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg RR
     9: {'offset': 180, 'neutral': 1500, 'ratio':  0.090},  # femur leg RR
    10: {'offset':  90, 'neutral': 1500, 'ratio':  0.090},  # tibia leg RR

    15: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LR
    14: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LR
    13: {'offset':  90, 'neutral': 1500, 'ratio': -0.090},  # tibia leg LR

    19: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LM
    18: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LM
    17: {'offset':  90, 'neutral': 1500, 'ratio': -0.090},  # tibia leg LM

    23: {'offset':   0, 'neutral': 1500, 'ratio':  0.090},  # coxa  leg LF
    22: {'offset': 180, 'neutral': 1500, 'ratio': -0.090},  # femur leg LF
    21: {'offset':  90, 'neutral': 1500, 'ratio': -0.090},  # tibia leg LF
}

# Gaits
GAIT_LEGS_GROUPS = {
    'tripod':      (('RM', 'LF', 'LR'), ('RF', 'LM', 'RR')),
    'tetrapod':    (('RR', 'LM'), ('RF', 'LR'), ('RM', 'LF')),
    'riple':       (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'metachronal': (('RR',), ('LM',), ('RF',), ('LR',), ('RM',), ('LF',)),
    'wave':        (('RR',), ('RM',), ('RF',), ('LR',), ('LM',), ('LF',))
}

# Gaits parameters
GAIT_PARAMS = {
    'default': {
        'length': 20.,
        'angle': 2.5,
        'height': {
            'min': 20.,
            'max': 40.
        },
        'speed': {
            'min': 25.,
            'max': 250.
        }
    },
    'tripod': {
        'length': 30.,
        'angle': 5.
    },
    'tetrapod': {},
    'riple': {},
    'metachronal': {},
    'wave': {}
}

# Gamepad path
THRUSTMASTER_PATH = "/dev/input/by-id/usb-Mega_World_Thrustmaster_dual_analog_3.2-event-joystick"
