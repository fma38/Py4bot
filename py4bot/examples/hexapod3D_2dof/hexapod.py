#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math

import visual

from py4bot.api import *

import settings


class Hexapod3D(Robot3D):
    def _createBody(self):
        return Body(settings.LEGS_ORIGIN)

    def _createLegs(self):
        legs = {}
        legIk = {}
        for legIndex in settings.LEGS_INDEX:
            legs[legIndex] = Leg2Dof(legIndex, {'coxa': Coxa(), 'femur': Femur()}, settings.FEET_NEUTRAL[legIndex])
            legIk[legIndex] = Leg2DofIk(settings.LEGS_GEOMETRY[legIndex])

        return legs, legIk

    def _createActuatorPool(self):
        driver = Driver3D()
        pool = ActuatorPool(driver)

        num = 0
        for leg in self._legs.values():

            # Create joints actuators
            coxa3D = Coxa3D(leg.coxa, num, settings.LEGS_GEOMETRY[leg.index]['coxa'])
            pool.add(coxa3D)
            num += 1

            femur3D = Femur3D(leg.femur, num, settings.LEGS_GEOMETRY[leg.index]['femur'])
            pool.add(femur3D)
            num += 1

            # Init 3D view
            coxa3D.frame = self._body3D
            femur3D.frame = coxa3D

            coxa3D.pos = (settings.LEGS_ORIGIN[leg.index]['x'], 0, -settings.LEGS_ORIGIN[leg.index]['y'])
            femur3D.pos = (settings.LEGS_GEOMETRY[leg.index]['coxa'], 0, 0)

            coxa3D.rotate(angle=math.radians(settings.LEGS_ORIGIN[leg.index]['gamma0']), axis=(0, 1, 0))

        return pool


class Keyboard(RemoteControl):
    def _createFrontend(self):
        try:
            return KeyboardUsb(settings.DELL_KEYBOARD_PATH)
        except:
            return KeyboardUsb(settings.MICROSOFT_KEYBOARD_PATH)

    def _buildComponents(self):
        self._addConfig("walk")
        self._addConfig("body")

        self._addComponent(Button, command=self.selectNextConfig, key="digital_059", trigger="hold")  # F1

        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStop, key="digital_002")  # 1
        self._addComponent(Button, configs="walk", command=GaitSequencer().walkStep, key="digital_015")  # TAB
        self._addComponent(Button, configs="walk", command=GaitSequencer().selectNextGait, key="digital_008", trigger="hold")  # 7

        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", mapper=MapperSetMultiply('x', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", mapper=MapperSetMultiply('y', coef=30))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_01", mapper=MapperSetMultiply('z', coef=20))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_00", modifier="digital_008", mapper=MapperSetMultiply('yaw', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_03", modifier="digital_008", mapper=MapperSetMultiply('pitch', coef=-10))
        self._addComponent(Axis, configs="body", command=self.robot.setBodyExtraPosition, key="analog_02", modifier="digital_008", mapper=MapperSetMultiply('roll', coef=10))

        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_105", mapper=MapperSetValue(dx=-5))  # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_106", mapper=MapperSetValue(dx=+5))  # RIGHT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_103", mapper=MapperSetValue(dy=+5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_108", mapper=MapperSetValue(dy=-5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_104", mapper=MapperSetValue(dz=+5))  # PG UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_109", mapper=MapperSetValue(dz=-5))  # PG DOWN

        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_075", modifier="digital_042", mapper=MapperSetValue(dyaw=+5))    # NUM 4
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_077", modifier="digital_042", mapper=MapperSetValue(dyaw=-5))    # NUM 6
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_103", modifier="digital_042", mapper=MapperSetValue(dpitch=-5))  # UP
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_108", modifier="digital_042", mapper=MapperSetValue(dpitch=+5))  # DOWN
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_105", modifier="digital_042", mapper=MapperSetValue(droll=-5))   # LEFT
        self._addComponent(Button, command=self.robot.incBodyPosition, key="digital_106", modifier="digital_042", mapper=MapperSetValue(droll=+5))   # RIGHT

        self._addComponent(Button, command=self.robot.setBodyPosition, key="digital_082", mapper=MapperSetValue(x=0, y=0, z=35, yaw=0, pitch=0, roll=0))   # NUM 0

        self._addComponent(Button, command=self.test, key="digital_003", mapper=MapperToggle())  # 2

    def test(self, flag):
        print flag


def main():
    def addGait(gaitClass, gaitName):
        """ Helper
        """
        group = settings.GAIT_LEGS_GROUPS[gaitName]
        params = settings.GAIT_PARAMS['default']
        params.update(settings.GAIT_PARAMS[gaitName])
        gait = gaitClass(gaitName, group, params)
        GaitManager().add(gait)

    scene = visual.display(width=settings.SCENE_WIDTH, height=settings.SCENE_HEIGHT)
    ground = visual.box(axis=(0, 1, 0), pos=(0, 0, 0), length=0.1, height=500, width=500, color=visual.color.gray(0.75), opacity=0.5)

    addGait(GaitTripod, "tripod")
    addGait(GaitTetrapod, "tetrapod")
    addGait(GaitRiple, "riple")
    addGait(GaitWave, "metachronal")
    addGait(GaitWave, "wave")
    GaitManager().select("riple")

    robot = Hexapod3D()

    remote = Keyboard(robot)

    GaitSequencer().start()
    remote.start()

    robot.setBodyPosition(z=30)
    #GaitSequencer().walk(linearSpeed=0.2, direction=90, length=1, angularSpeed=0)

    robot.mainLoop()

    remote.stop()
    remote.join()
    GaitSequencer().stop()
    GaitSequencer().join()


if __name__ == "__main__":
    Logger().setLevel('trace')
    main()
