# -*- coding: utf-8 -*-

""" Py4bot framework.

License
=======

 - B{Py4bot} (U{http://py4bot.gbiloba.org}) is Copyright:
  - (C) 2014-2021 Frédéric Mantegazza

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
or see:

 - U{http://www.gnu.org/licenses/gpl.html}

Module purpose
==============

API imports.

Implements
==========

Documentation
=============

Usage
=====

@author: Frédéric Mantegazza
@copyright: (C) 2014-2021 Frédéric Mantegazza
@license: GPL

@todo: split in subpackages?
"""

from py4bot.common import config

# Services
from py4bot.services.logger import Logger
#Logger().setLevel("debug")  # uncomment to debug imports

# Actuators
from py4bot.actuators.actuator import Actuator
from py4bot.actuators.actuatorPool import ActuatorPool
from py4bot.actuators.servo import Servo
from py4bot.actuators.servoPool import ServoPool
try:
    from py4bot.actuators.actuators3D import Actuator3D, Coxa3D, Femur3D, Tibia3D, Tars3D
except ImportError:
    Logger().warning("VPython unavailable on this platform")
    Logger().exception("API import", debug=True)

# Common
from py4bot.common.exception import Py4botError, Py4botValueError, Py4botIKError, Py4botAttributeError, Py4botInterrupt
from py4bot.common.utils import cartesian2polar

# Core
from py4bot.core.body import Body
try:
    from py4bot.core.body3D import Body3D
except ImportError:
    Logger().warning("VPython unavailable on this platform")
    Logger().exception("API import", debug=True)
from py4bot.core.joints import Joint, Coxa, Femur, Tibia, Tars
from py4bot.core.leg import Leg
from py4bot.core.leg2Dof import Leg2Dof
from py4bot.core.leg3Dof import Leg3Dof
from py4bot.core.leg4Dof import Leg4Dof
from py4bot.core.robot import Robot
try:
    from py4bot.core.robot3D import Robot3D
except ImportError:
    Logger().warning("VPython unavailable on this platform")
    Logger().exception("API import", debug=True)

# Drivers
from py4bot.drivers.driver import Driver
from py4bot.drivers.driver3D import Driver3D
from py4bot.drivers.fakeDriver import FakeDriver
from py4bot.drivers.lewanSoul import LewanSoul
from py4bot.drivers.maestro import Maestro
from py4bot.drivers.pca9685Driver import Pca9685Driver
from py4bot.drivers.pololuSerial import PololuSerial
from py4bot.drivers.servoNode import ServoNode
try:
    from py4bot.drivers.servosPRU import ServosPRU
except ImportError:
    Logger().warning("ServosPRU unavailable on this platform")
    Logger().exception("API import", debug=True)
from py4bot.drivers.veyron import Veyron
from py4bot.drivers.usc32 import USC32

# Gaits
from py4bot.gaits.gait import Gait
from py4bot.gaits.gaitManager import GaitManager
from py4bot.gaits.gaitRiple import GaitRiple
from py4bot.gaits.gaitSequencer import GaitSequencer
from py4bot.gaits.gaitTetrapod import GaitTetrapod
from py4bot.gaits.gaitTripod import GaitTripod
from py4bot.gaits.gaitWave import GaitWave

# Ik
from py4bot.ik.legIk import LegIk
from py4bot.ik.leg2DofIk import Leg2DofIk
from py4bot.ik.leg3DofIk import Leg3DofIk
from py4bot.ik.leg4DofIk import Leg4DofIk

# Inputs
from py4bot.inputs.axis import Axis
from py4bot.inputs.button import Button
from py4bot.inputs.joystick import Joystick
from py4bot.inputs.remoteControl import RemoteControl
from py4bot.inputs.curves.curve import Curve, CurveVerySlow, CurveSlow, CurveStd, CurveFast, CurveVeryFast
from py4bot.inputs.frontends.frontend import Frontend
from py4bot.inputs.frontends.frontendUsb import FrontendUsb
from py4bot.inputs.frontends.ipegaPG9023 import IpegaPG9023
from py4bot.inputs.frontends.jsonUDP import JsonUDP
from py4bot.inputs.frontends.keyboardUsb import KeyboardUsb
from py4bot.inputs.frontends.teensyUsb import TeensyUsb
from py4bot.inputs.frontends.thrustmaster import Thrustmaster
from py4bot.inputs.mappers.mapper import Mapper, MapperStraight, MapperSet, MapperSetMultiply, MapperSetValue, MapperToggle, MapperWalk
